package br.com.mario.reservalugar.utils

/**
 * Arquivo que contém os enums que auxiliam nos tipos/caminhos dos dados
 *
 * Created on 20/04/2022
 */

/** Categoriza os tipos de carteira do sistema. <br>Created on 27/01/2022</br> */
enum class WALLET(val str: String) {
	BANK("banco"), MONEY("dinheiro");

	companion object {
		val sto: (String) -> WALLET
			get() = { when(it.lowercase()) {
				"banco" -> BANK
				else -> MONEY
			} }
		val opIdx: (String) -> Int
			get() = { sto(it).ordinal }
	}
}

/** Created on 17/04/2022 */
enum class PATHS(val str: String) {
	EXTRA("extra"), TABLE("mesa"), PARK("estacionamento");

	companion object {
		val sto: (String) -> PATHS
			get() = { when(it.lowercase()){
				"extra" -> EXTRA
				"mesa" -> TABLE
				else -> PARK
			} }
	}
}