/**
 * Created on 27/06/2022
 * @since 1.5.6.11
 * @see https://stackoverflow.com/a/71902319/3443949
 */
package br.com.mario.reservalugar.utils

import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.boundsInRoot
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalView
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import java.io.File
import java.io.IOException

/**
 * Create a State of screenshot of composable that is used with that is kept on each recomposition.
 * @param delayInMillis delay before each screenshot if [liveScreenshotFlow] is collected.
 */
@Composable
fun rememberScreenshotState(delayInMillis: Long = 20) = remember {
	ScreenshotState(delayInMillis)
}

/**
 * State of screenshot of composable that is used with.
 * @param timeInMillis delay before each screenshot if [liveScreenshotFlow] is collected.
 */
class ScreenshotState internal constructor(private val timeInMillis: Long = 20) {
	internal var callback: (() -> Bitmap?)? = null

	private val bitmapState = mutableStateOf(callback?.invoke())

	/** Captures current state of Composables inside [ScreenshotBox] */
	fun capture() {
		bitmapState.value = callback?.invoke()
	}

	val liveScreenshotFlow = flow {
		while (true) {
			val bmp = callback?.invoke()
			bmp?.let {
				emit(it)
			}
			delay(timeInMillis)
		}
	}

	val bitmap: Bitmap?
		get() = bitmapState.value

	val imageBitmap: ImageBitmap?
		get() = bitmap?.asImageBitmap()
}

/**
 * A composable that gets screenshot of Composable that is in [content].
 * @param screenshotState state of screenshot that contains [Bitmap].
 * @param content Composable that will be captured to bitmap on action or periodically.
 */
@Composable
fun ScreenshotBox(
	modifier: Modifier = Modifier,
	screenshotState: ScreenshotState,
	content: @Composable () -> Unit,
) {
	val view = LocalView.current

	var composableBounds by remember {
		mutableStateOf<Rect?>(null)
	}

	DisposableEffect(Unit) {
		var bitmap: Bitmap? = null

		screenshotState.callback = {
			composableBounds?.let { bounds ->

				if (bounds.width == 0f || bounds.height == 0f) return@let

				bitmap = Bitmap.createBitmap(
					bounds.width.toInt(),
					bounds.height.toInt(),
					Bitmap.Config.ARGB_8888
				)

				bitmap?.let { bmp ->
					val canvas = Canvas(bmp)
						.apply {
							translate(-bounds.left, -bounds.top)
						}
					view.draw(canvas)
				}
			}
			bitmap
		}

		onDispose {
			bitmap?.apply {
				if (!isRecycled) {
					recycle()
					bitmap = null
				}
			}
			screenshotState.callback = null
		}
	}

	Box(modifier = modifier
		.onGloballyPositioned {
			composableBounds = it.boundsInRoot()
		}
	) {
		content()
	}
}

fun File.writeBitmap(bitmap: Bitmap, format: Bitmap.CompressFormat, quality: Int) {
	try {
		outputStream().use { out ->
			bitmap.compress(format, quality, out)
			out.close()
		}

	} catch (e: IOException) {
		e.printStackTrace()
	}
}