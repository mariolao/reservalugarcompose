package br.com.mario.reservalugar.ui

import android.os.Build
import android.os.Bundle

import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi

import androidx.compose.animation.ExperimentalAnimationApi

import br.com.mario.reservalugar.ReservaLugarApplication
import br.com.mario.reservalugar.TEST
import br.com.mario.reservalugar.ui.settings.SettingDataStore

import br.mariodeveloper.menucollapsable.utils.rememberWindowInfo

import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking

/**
 * Gerencia a aplicação, chama o carregamento das informações
 *
 * @since 11/10/2021
 * Lst mod on 11/05/2022
 */
class CoreActivity : ComponentActivity() {
	@OptIn(ExperimentalAnimationApi::class) //@ExperimentalAnimationApi
	@RequiresApi(Build.VERSION_CODES.O)
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
//		WindowCompat.setDecorFitsSystemWindows(window, false)
		TEST = runBlocking { SettingDataStore(applicationContext).testBDFlagFlow.first() } // resgate síncrono

		val appContainer = (application as ReservaLugarApplication).container
		setContent {
			ReservaLugarComposeEntry(appContainer, rememberWindowInfo())
		}
	}
}