package br.com.mario.reservalugar.ui

import androidx.compose.animation.core.AnimationVector1D
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.TwoWayConverter
import androidx.compose.animation.core.animateOffset
import androidx.compose.animation.core.animateSize
import androidx.compose.animation.core.animateValue
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.animation.core.updateTransition

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

import br.com.mario.reservalugar.ui.BoxPaths.COMPAT
import br.com.mario.reservalugar.ui.BoxPaths.HORIZONTAL
import br.com.mario.reservalugar.ui.BoxPaths.QUADRANT1
import br.com.mario.reservalugar.ui.BoxPaths.QUADRANT2
import br.com.mario.reservalugar.ui.BoxPaths.QUADRANT3
import br.com.mario.reservalugar.ui.BoxPaths.QUADRANT4
import br.com.mario.reservalugar.ui.BoxPaths.VERTICAL
import br.com.mario.reservalugar.ui.BoxPaths.stroke
import br.com.mario.reservalugar.ui.theme.MyColors


/**
 * @author Mário Henrique
 * Created on 09/01/2022
 * Lst mod on 11/01/2022
 */
@Composable
fun SplashScreen() {
	Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
		val timelapse = 500
		// TODO: https://github.com/mxalbert1996/compose-shared-elements
		val repeatable by rememberInfiniteTransition().animateValue(
			initialValue = Steps.S1,
			targetValue = Steps.S25,
			typeConverter = TwoWayConverter(
				{ AnimationVector1D(it.ordinal.toFloat()) },
				{ Steps.values()[it.value.toInt()] }),
			animationSpec = infiniteRepeatable(animation = tween(25 * timelapse, easing = LinearEasing))
		)
		/*val infiniteBox2 by infinite.animateValue(
			initialValue = BoxState.S1,
			targetValue = BoxState.S9,
			typeConverter = TwoWayConverter(
				{ AnimationVector1D(it.ordinal.toFloat()) },
				{ BoxState.values()[it.value.toInt()] }),
			animationSpec = infiniteRepeatable(animation = tween(9000, easing = LinearEasing))
		)*/
		/*val infiniteBox2: BoxState by animateValueAsState(
			targetValue = if (infiniteBox1 == BoxState.Q1) BoxState.Q4
			else if (infiniteBox1 == BoxState.Q2) BoxState.Q4
			else if (infiniteBox1 == BoxState.Q3) BoxState.Q5
			else if (infiniteBox1 == BoxState.Q4) BoxState.Q7
			else if (infiniteBox1 == BoxState.Q5) BoxState.Q5
			else if (infiniteBox1 == BoxState.Q6) BoxState.Q5
			else if (infiniteBox1 == BoxState.Q7) BoxState.Q5
			else if (infiniteBox1 == BoxState.Q8) BoxState.Q5
			else BoxState.Q8,
			typeConverter =
			TwoWayConverter({ AnimationVector1D(it.ordinal.toFloat()) }, { BoxState.values()[it.value.toInt()] }),
			animationSpec = infiniteRepeatable(animation = tween(9000, easing = LinearEasing))
		)*/

		val sizeOuterBox = 180.dp
		BoxPaths.lgt = LocalDensity.current.run { sizeOuterBox.toPx() }
		stroke = BoxPaths.lgt / 20f

		val update = updateTransition(targetState = repeatable, label = "")
		val box1Size by update.animateSize({ tween(timelapse, easing = LinearEasing) }, label = "") { state ->
			when (state) {
				Steps.S1 -> HORIZONTAL
				Steps.S2 -> COMPAT
				Steps.S3 -> COMPAT
				Steps.S4 -> COMPAT
				Steps.S5 -> COMPAT
				Steps.S6 -> COMPAT
				Steps.S7 -> VERTICAL
				Steps.S8 -> COMPAT
				Steps.S9 -> COMPAT
				Steps.S10 -> COMPAT
				Steps.S11 -> COMPAT
				Steps.S12 -> COMPAT
				Steps.S13 -> HORIZONTAL
				Steps.S14 -> COMPAT
				Steps.S15 -> COMPAT
				Steps.S16 -> COMPAT
				Steps.S17 -> COMPAT
				Steps.S18 -> COMPAT
				Steps.S19 -> VERTICAL
				Steps.S20 -> COMPAT
				Steps.S21 -> COMPAT
				Steps.S22 -> COMPAT
				Steps.S23 -> COMPAT
				Steps.S24 -> COMPAT
				else -> HORIZONTAL
			}
		}
		val box1Offset by update.animateOffset({ tween(timelapse, easing = LinearEasing) }, label = "") { state ->
			when (state) {
				Steps.S1 -> QUADRANT1
				Steps.S2 -> QUADRANT2
				Steps.S3 -> QUADRANT2
				Steps.S4 -> QUADRANT2
				Steps.S5 -> QUADRANT2
				Steps.S6 -> QUADRANT2
				Steps.S7 -> QUADRANT2
				Steps.S8 -> QUADRANT3
				Steps.S9 -> QUADRANT3
				Steps.S10 -> QUADRANT3
				Steps.S11 -> QUADRANT3
				Steps.S12 -> QUADRANT3
				Steps.S13 -> QUADRANT4
				Steps.S14 -> QUADRANT4
				Steps.S15 -> QUADRANT4
				Steps.S16 -> QUADRANT4
				Steps.S17 -> QUADRANT4
				Steps.S18 -> QUADRANT4
				Steps.S19 -> QUADRANT1
				Steps.S20 -> QUADRANT1
				Steps.S21 -> QUADRANT1
				Steps.S22 -> QUADRANT1
				Steps.S23 -> QUADRANT1
				Steps.S24 -> QUADRANT1
				else -> QUADRANT1
			}
		}
		val box2Size by update.animateSize({ tween(timelapse, easing = LinearEasing) }, label = "") { state ->
			when (state) {
				Steps.S1 -> COMPAT
				Steps.S2 -> COMPAT
				Steps.S3 -> COMPAT
				Steps.S4 -> COMPAT
				Steps.S5 -> HORIZONTAL
				Steps.S6 -> COMPAT
				Steps.S7 -> COMPAT
				Steps.S8 -> COMPAT
				Steps.S9 -> COMPAT
				Steps.S10 -> COMPAT
				Steps.S11 -> VERTICAL
				Steps.S12 -> COMPAT
				Steps.S13 -> COMPAT
				Steps.S14 -> COMPAT
				Steps.S15 -> COMPAT
				Steps.S16 -> COMPAT
				Steps.S17 -> HORIZONTAL
				Steps.S18 -> COMPAT
				Steps.S19 -> COMPAT
				Steps.S20 -> COMPAT
				Steps.S21 -> COMPAT
				Steps.S22 -> COMPAT
				Steps.S23 -> VERTICAL
				Steps.S24 -> COMPAT
				else -> COMPAT
			}
		}
		val box2Offset by update.animateOffset({ tween(timelapse, easing = LinearEasing) }, label = "") { state ->
			when (state) {
				Steps.S1 -> QUADRANT3
				Steps.S2 -> QUADRANT3
				Steps.S3 -> QUADRANT3
				Steps.S4 -> QUADRANT3
				Steps.S5 -> QUADRANT4
				Steps.S6 -> QUADRANT4
				Steps.S7 -> QUADRANT4
				Steps.S8 -> QUADRANT4
				Steps.S9 -> QUADRANT4
				Steps.S10 -> QUADRANT4
				Steps.S11 -> QUADRANT1
				Steps.S12 -> QUADRANT1
				Steps.S13 -> QUADRANT1
				Steps.S14 -> QUADRANT1
				Steps.S15 -> QUADRANT1
				Steps.S16 -> QUADRANT1
				Steps.S17 -> QUADRANT1
				Steps.S18 -> QUADRANT2
				Steps.S19 -> QUADRANT2
				Steps.S20 -> QUADRANT2
				Steps.S21 -> QUADRANT2
				Steps.S22 -> QUADRANT2
				Steps.S23 -> QUADRANT2
				Steps.S24 -> QUADRANT3
				else -> QUADRANT3
			}
		}
		val box3Size by update.animateSize({ tween(timelapse, easing = LinearEasing) }, label = "") { state ->
			when (state) {
				Steps.S1 -> COMPAT
				Steps.S2 -> COMPAT
				Steps.S3 -> VERTICAL
				Steps.S4 -> COMPAT
				Steps.S5 -> COMPAT
				Steps.S6 -> COMPAT
				Steps.S7 -> COMPAT
				Steps.S8 -> COMPAT
				Steps.S9 -> HORIZONTAL
				Steps.S10 -> COMPAT
				Steps.S11 -> COMPAT
				Steps.S12 -> COMPAT
				Steps.S13 -> COMPAT
				Steps.S14 -> COMPAT
				Steps.S15 -> VERTICAL
				Steps.S16 -> COMPAT
				Steps.S17 -> COMPAT
				Steps.S18 -> COMPAT
				Steps.S19 -> COMPAT
				Steps.S20 -> COMPAT
				Steps.S21 -> HORIZONTAL
				Steps.S22 -> COMPAT
				Steps.S23 -> COMPAT
				Steps.S24 -> COMPAT
				else -> COMPAT
			}
		}
		val box3Offset by update.animateOffset({ tween(timelapse, easing = LinearEasing) }, label = "") { state ->
			when (state) {
				Steps.S1 -> QUADRANT4
				Steps.S2 -> QUADRANT4
				Steps.S3 -> QUADRANT1
				Steps.S4 -> QUADRANT1
				Steps.S5 -> QUADRANT1
				Steps.S6 -> QUADRANT1
				Steps.S7 -> QUADRANT1
				Steps.S8 -> QUADRANT1
				Steps.S9 -> QUADRANT1
				Steps.S10 -> QUADRANT2
				Steps.S11 -> QUADRANT2
				Steps.S12 -> QUADRANT2
				Steps.S13 -> QUADRANT2
				Steps.S14 -> QUADRANT2
				Steps.S15 -> QUADRANT2
				Steps.S16 -> QUADRANT3
				Steps.S17 -> QUADRANT3
				Steps.S18 -> QUADRANT3
				Steps.S19 -> QUADRANT3
				Steps.S20 -> QUADRANT3
				Steps.S21 -> QUADRANT4
				Steps.S22 -> QUADRANT4
				Steps.S23 -> QUADRANT4
				Steps.S24 -> QUADRANT4
				else -> QUADRANT4
			}
		}

		Canvas(Modifier.size(sizeOuterBox).background(Color.DarkGray), onDraw = {
			drawRect(
				Color(0xFFf5f5f5),
				box1Offset,
				box1Size,
				style = Stroke(stroke)
			)
			drawRect(
				MyColors.Holo_GreenGrass,
				box2Offset,
				box2Size,
				style = Stroke(stroke)
			)
			drawRect(
				MyColors.Holo_RedDragon,
				box3Offset,
				box3Size,
				style = Stroke(stroke)
			)
		})


	}
}

enum class Steps {
	S1, S2, S3, S4, S5, S6, S7, S8, S9, S10,
	S11, S12, S13, S14, S15, S16, S17, S18, S19, S20,
	S21, S22, S23, S24, S25
}

object BoxPaths {
	var stroke = 20f
	var lgt = 0f

	val QUADRANT1 by lazy { Offset(stroke, stroke) }
	val QUADRANT2 by lazy { Offset((lgt * 2) / 5f + ( 3 * stroke), stroke) }
	val QUADRANT3 by lazy { Offset((lgt * 2) / 5f + ( 3 * stroke), ((lgt * 2) / 5f) + (3 * stroke)) }
	val QUADRANT4 by lazy { Offset(stroke, ((lgt * 2) / 5f) + (3 * stroke)) }

	val COMPAT by lazy { Size((lgt * 2) / 5f, (lgt * 2) / 5f) }
	val HORIZONTAL by lazy { Size(lgt - (2 * stroke), (lgt * 2) / 5f) }
	val VERTICAL by lazy { Size((lgt * 2) / 5f, lgt - (2 * stroke)) }

	/*val states by lazy { listOf(
		BOX(QUADRANT1, HORIZONTAL),
		BOX(QUADRANT2, COMPAT),
		BOX(QUADRANT2, VERTICAL),
		BOX(QUADRANT3, COMPAT),
		BOX(QUADRANT4, HORIZONTAL),
		BOX(QUADRANT4, COMPAT),
		BOX(QUADRANT1, VERTICAL),
		BOX(QUADRANT1, COMPAT),
		BOX(QUADRANT1, COMPAT)
	)}*/
}

data class BOX(val coord: Offset, val size: Size)

@Preview
@Composable
fun PreviewSplash() {
	SplashScreen()
}