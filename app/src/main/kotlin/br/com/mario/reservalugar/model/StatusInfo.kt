package br.com.mario.reservalugar.model

import com.google.firebase.database.Exclude

/**
 * Encapsula os vários dados, agrupados pela categoria, para exibir no quadrante correspondente em StatusSector
 *
 * Created on 21/12/2021
 * Lst mod on 02/05/2022
 */
data class StatusInfo(val price: Double, val capacity: Long, val remain: Long, val paydValue: Double) {
	val rented: Long
		get() = capacity - remain

	@get:Exclude
	val paid: Double
		get() = (paydValue / price)

	val prev: Double
		get() = (rented * price)

	override fun equals(other: Any?) = when (other) {
		null -> false
		is StatusInfo -> (other.hashCode() == this.hashCode())
		else -> true
	}

	override fun hashCode(): Int =
		31 *
			(31 *
					(31 *
							price.hashCode() + capacity.hashCode()) +
					remain.hashCode()) +
			paydValue.hashCode()
}