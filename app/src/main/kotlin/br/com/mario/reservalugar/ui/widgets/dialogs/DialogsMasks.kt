package br.com.mario.reservalugar.ui.widgets.dialogs

import androidx.annotation.DrawableRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties

import br.com.mario.reservalugar.model.BuyerObject
import br.com.mario.reservalugar.model.CostObject
import br.com.mario.reservalugar.model.TableObject
import br.com.mario.reservalugar.utils.WALLET

/** Encapsula as chamadas dos Caixas de Diálogo
 *
 * Created on 17/12/2021
 * Lst mod on 30/06/2022
 */
object DialogBuilder {

	@Composable
	fun DialogReserveOne(seat: Int, onDismiss: () -> Unit, onPositiveClick: (String) -> Unit) {
		Dialog(onDismiss, DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false),
			content = { DialogReserveOneContent(seat, onDismiss, onPositiveClick) }
		)
	}

	/** Created on 14/12/2021 */
	@Composable
	fun DialogReserveVarious(onDismiss: OnDismiss, onPositiveClick: (seats: String, owner: String) -> Unit) {
		Dialog(onDismiss, DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false),
			content = { DialogReserveVariousContent(onDismiss, onPositiveClick) })
	}

	@Composable
	fun DialogTableDetails(
		result: Pair<TableObject, BuyerObject>,
		onBookParkLot: (BuyerObject, Boolean) -> Unit,
		onBookExtra: (TableObject, Boolean) -> Unit,
		onChangeOwner: () -> Unit,
		onDismiss: () -> Unit,
		onUnbookSeat: (TableObject, BuyerObject) -> Unit
	) {
		Dialog(onDismiss, DialogProperties(dismissOnBackPress = false),
			{ DialogDetailsContent(result, onBookParkLot, onBookExtra, onUnbookSeat, onChangeOwner, onDismiss) })
	}

	@Composable
	fun DialogCosts(cost: CostObject, onDismiss: OnDismiss, onPositiveClick: (CostObject, init: Double) -> Unit) {
		Dialog(onDismiss, DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false),
			{ DialogCostContent(cost, onDismiss, onPositiveClick = onPositiveClick) })
	}
	/**  */
	@Composable
	fun DialogChooseWallet(onDismiss: OnDismiss, onPayClick: (WALLET) -> Unit) {
		Dialog(onDismissRequest = onDismiss, DialogProperties(dismissOnClickOutside = false)) {
			DialogChooseWalletContent(onDismiss, onPayClick)
		}
	}
	/** Created on 31/01/2022 */
	@Composable
	fun DialogRefundWallet(@DrawableRes img: Int, onDismiss: OnDismiss, onConfirm: () -> Unit) {
		Dialog(onDismissRequest = onDismiss, DialogProperties(dismissOnClickOutside = false)) {
			DialogRefundWalletContent(img, onDismiss = onDismiss, onConfirmClick = onConfirm)
		}
	}

	/** Created on 30/06/2022. @since Beta 1.5.6.12 */
	@Composable
	fun DialogChangeOwnership(
		pair: Pair<TableObject, BuyerObject>,
		onDismiss: () -> Unit,
		onChangeOwner: (TableObject, BuyerObject, String) -> Unit,
	) {
		Dialog(onDismiss, DialogProperties(dismissOnBackPress = false))
		{ DialogChangeOwnershipContent(pair, onChangeOwner, onDismiss) }
	}
}

typealias OnDismiss = () -> Unit