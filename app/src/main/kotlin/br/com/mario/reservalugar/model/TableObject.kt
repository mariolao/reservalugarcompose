/**
 * @author Mário Henrique
 * Created on 11/04/2022
 * Lst mod on 22/04/2022
 */
package br.com.mario.reservalugar.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.IgnoreExtraProperties
import com.google.firebase.database.PropertyName
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

private const val BUYER_TAG = "comprador"
private const val DATE_TAG = "data"
private const val PAY_TAG = "pagamento"
private const val WALLET_TAG = "carteira"
private const val EXTRA_TAG = "extra"

/** Encapsula o itens do nó "reserva_lugar", que representam a reserva de uma mesa */
@IgnoreExtraProperties
data class TableObject(
	@get:PropertyName(BUYER_TAG) val buyer: String = "", @get:PropertyName(DATE_TAG) val dateStr: String = "",
	@get:PropertyName(PAY_TAG) var paid: Boolean = false, @get:PropertyName(WALLET_TAG) val walletStr: String = "",
	@get:PropertyName(EXTRA_TAG) var extra: ExtraModel? = null,
	@get:Exclude val id: String = "-1"
) {
	private val timeFormatter = DateTimeFormat.forPattern("dd/MM/yy")

	/** Transforma a data escrita em formato de objeto para melhor tratamento */
	@get:Exclude
	val dateStamp: DateTime
		get() = timeFormatter.parseDateTime(dateStr)

	@get:Exclude
	val hasOwner: Boolean
		get() = buyer.isNotBlank()

	companion object {
		fun fromMap(item: Any?, key: String) =
			if (item == null) TableObject()
			else {
				with(item as HashMap<*, *>) {
					TableObject(this[BUYER_TAG].toString(), this[DATE_TAG].toString(),
						this[PAY_TAG] as Boolean, this[WALLET_TAG].toString(), ExtraModel.fromMap(this[EXTRA_TAG] as HashMap<*, *>?), key)
				}
			}

		fun toPayMap(walletStr: String, paid: Boolean): HashMap<String, Any> =
			hashMapOf(WALLET_TAG to walletStr, PAY_TAG to paid)
	}
}
/** Created  on 11/04/2022 */
data class ExtraModel(
	@get:PropertyName(DATE_TAG) val date: String,
	@get:PropertyName(PAY_TAG) val paid: Boolean = false,
	@get:PropertyName(WALLET_TAG) val wallet: String = "",
) {
	companion object {
		fun fromMap(item: HashMap<*, *>?) =
			item?.let { ExtraModel(it[DATE_TAG].toString(), it[PAY_TAG] as Boolean, it[WALLET_TAG].toString()) }

		fun toPayMap(walletStr: String, paid: Boolean): HashMap<String, Any> =
			hashMapOf(WALLET_TAG to walletStr, PAY_TAG to paid)
	}
}