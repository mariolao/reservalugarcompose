package br.com.mario.reservalugar

import android.content.Context
import android.graphics.Color
import android.widget.Toast

import br.com.mario.reservalugar.model.BuyerObject
import br.com.mario.reservalugar.model.TableObject

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference

import com.pdfjet.Align
import com.pdfjet.Cell
import com.pdfjet.CoreFont
import com.pdfjet.Font
import com.pdfjet.Letter
import com.pdfjet.PDF
import com.pdfjet.Page

import java.io.File
import java.io.FileOutputStream

import kotlin.math.absoluteValue

/**
 * @author Mário Henrique
 * Created on 09/06/2018
 * Modified  on 27/06/2020
 * Lst mod on 30/06/2022
 */

abstract class GeneratePDF<T>(private val ctx: Context, protected val theList: List<T>) {
	abstract val titles: Array<String>
	abstract val pdfFile: String
	abstract var sorted: List<T>

	/**
	 * PDFjet http://pdfjet.com/java/examples/example-08.html
	 *
	 * @param pos1 posição da coluna 'nome' na primeira parte da tabela
	 * @param pos2 posição da coluna 'nome' na  segunda parte da tabela
	 */
	fun generate(pos1: Int = titles.size, pos2: Int = titles.size + 2): File? {
		val fileResult = File(File(ctx.cacheDir, "docs").apply { mkdirs() }, pdfFile)
		val fOut = FileOutputStream(fileResult) // overwrites this image every time

		val pdf = PDF(fOut)
		val page = Page(pdf, Letter.PORTRAIT)

		val f1 = Font(pdf, CoreFont.HELVETICA_BOLD).apply { size = 7f }
		val f2 = Font(pdf, CoreFont.HELVETICA).apply { size = 6f }

		Table(getData(f1, f2)).apply {
			setTextColorInRow(0, Color.BLUE) // linha cabeçalho
			setCellBordersWidth(.8f)
			autoAdjustColumnWidths()
			setColumnWidth(1, getColumnWidth(1) + 3) // 1ª linha de nome
			setColumnWidth(pos2, getColumnWidth(1) + 3) // 2ª linha de nome
			setColumnWidth(pos1, 50f) // espaço do meio

			setPosition((page.width - width) / 2, 30f)
			getNumberOfPages(page)
			drawOn(page)
		}

		try {
			pdf.flush()
			fOut.close()

		} catch (e: Exception) {
			Toast.makeText(ctx, "Erro ao criar arquivo", Toast.LENGTH_SHORT).show()
			return (null)
		} finally {
			Toast.makeText(ctx, "Arquivo $pdfFile criado", Toast.LENGTH_SHORT).show()
		}

		return (fileResult)
	}

	protected abstract fun makeRow(f: Font, elem: T, row: ArrayList<Cell> = ArrayList()): ArrayList<Cell>

	protected fun cCell(f: Font, text: String, align: Int = Align.CENTER) = Cell(f, text).apply { textAlignment = align }
	protected fun eCell(f: Font, boo: Boolean?) = (
			if (boo != null) (cCell(f, "P").apply { if (!boo) setFgColor(Color.RED) })
			else cCell(f, "X").apply { setFgColor(Color.RED) })

	private fun getData(f1: Font, f2: Font) = ArrayList<List<Cell>>().apply {
		sorted.apply {
			if (size <= MAX_ROWS) {
				add(titles.map { Cell(f1, it) }) // cabeçalho

				for (i in 0 until size)
					add(makeRow(f2, get(i)))
			} else {
				add(titles.map { Cell(f1, it) }.plus(Cell(f1).apply { setNoBorders() }).plus(titles.map { Cell(f1, it) }))

				for (i in 0 until (size - MAX_ROWS).absoluteValue)
					add(makeRow(f2, get(i + MAX_ROWS), // coluna direita
						emptyRow(f2,            // coluna do meio (espaço vazio)
							makeRow(f2, get(i))) // coluna esquerda
					))
				for (i in (size - MAX_ROWS) until MAX_ROWS) // resto da primeira coluna
					add(makeRow(f2, get(i)))
			}
		}
	}
	private fun emptyRow(f2: Font, row: ArrayList<Cell>) = row.apply { add(Cell(f2).apply { setNoBorders() }) }
}

class GenerateTablePDF(context: Context, list: List<TableObject>) : GeneratePDF<TableObject>(context, list) {
	override val titles = arrayOf("DATA", "NOME", "MESA", "EXTRA")

	override val pdfFile = PDF_FILE_01
	override var sorted: List<TableObject> = theList
		get() = field.sortedWith(compare({ it.dateStamp }, { it.buyer }))

	override fun makeRow(f: Font, elem: TableObject, row: ArrayList<Cell>) = row.apply {
		addAll(arrayListOf(
			Cell(f, elem.dateStr), Cell(f, elem.buyer),
			cCell(f, elem.id).apply { if (!elem.paid) setFgColor(Color.RED) },
			eCell(f, elem.extra?.run { paid })
		))
	}
}

class GenerateParkingPDF(context: Context, list: List<BuyerObject>) : GeneratePDF<BuyerObject>(context, list) {
	override val titles = arrayOf("NOME", "VAGA")

	override val pdfFile = PDF_FILE_02
	override var sorted: List<BuyerObject> = theList
		get() = field.sortedWith(compare({ it.name }))

//	override fun generate(pos1: Int, pos2: Int) = super.generate(2, 4)

	override fun makeRow(f: Font, elem: BuyerObject, row: ArrayList<Cell>) = row.apply {
		addAll( arrayListOf(Cell(f, elem.name), eCell(f, elem.parking?.run { paid })) )
	}
}

/**
 * Add a listener for a single change in the data at this location.
 * This listener will be triggered once with the value of the data at the location
 */
fun DatabaseReference.onSingleDataChange(f: (DataSnapshot) -> Unit) {
	singleEventListener { onDataChange { f(it) } }
}

fun DatabaseReference.onValueDataChange(f: (DataSnapshot) -> Unit) = valueDataListener { onDataChange { f(it) } }

private fun DatabaseReference.valueDataListener(init: __ValueEventListener.() -> Unit) {
	val listener = __ValueEventListener()
	listener.init()
	addValueEventListener(listener)
}

private fun DatabaseReference.singleEventListener(init: __ValueEventListener.() -> Unit) {
	val listener = __ValueEventListener()
	listener.init()
	addListenerForSingleValueEvent(listener)
}

@Suppress("unused", "ClassName")
class __ValueEventListener: com.google.firebase.database.ValueEventListener {
	private var _onDataChange: ((DataSnapshot) -> Unit)? = null
	private var _onCancelled: ((DatabaseError) -> Unit)? = null

	override fun onCancelled(error: DatabaseError) {
		_onCancelled?.invoke(error)
	}

	fun onCancelled(listener: ((DatabaseError) -> Unit)?) {
		_onCancelled = listener
	}

	override fun onDataChange(dataSnapshot: DataSnapshot) {
		_onDataChange?.invoke(dataSnapshot)
	}

	fun onDataChange(listener: ((DataSnapshot) -> Unit)?) {
		_onDataChange = listener
	}
}

fun DatabaseReference.childEventListener(init: __ChildEventListener.() -> Unit) {
	val listener = __ChildEventListener()
	listener.init()
	addChildEventListener(listener)
}

@Suppress("unused", "ClassName")
class __ChildEventListener : com.google.firebase.database.ChildEventListener {
	private var _onCancelled: ((DatabaseError?) -> Unit)? = null
	private var _onChildMoved: ((DataSnapshot?, String?) -> Unit)? = null
	private var _onChildChanged: ((DataSnapshot, String?) -> Unit)? = null
	private var _onChildAdded: ((DataSnapshot, String?) -> Unit)? = null
	private var _onChildRemoved: ((DataSnapshot?) -> Unit)? = null

	override fun onCancelled(dError: DatabaseError) {
		_onCancelled?.invoke(dError)
	}

	fun onCancelled(listener: ((data: DatabaseError?) -> Unit)?) {
		_onCancelled = listener
	}

	override fun onChildMoved(dSnap: DataSnapshot, elem: String?) {
		_onChildMoved?.invoke(dSnap, elem)
	}

	fun onChildMoved(listener: ((data: DataSnapshot?, oldName:String?) -> Unit)?) {
		_onChildMoved = listener
	}

	override fun onChildChanged(dSnap: DataSnapshot, elem: String?) {
		_onChildChanged?.invoke(dSnap, elem)
	}

	fun onChildChanged(listener: ((data: DataSnapshot, oldName:String?) -> Unit)?) {
		_onChildChanged = listener
	}

	override fun onChildAdded(dSnap: DataSnapshot, oldName: String?) {
		_onChildAdded?.invoke(dSnap, oldName)
	}

	fun onChildAdded(listener: ((data: DataSnapshot, oldName:String?) -> Unit)?) {
		_onChildAdded = listener
	}

	override fun onChildRemoved(dSnap: DataSnapshot) {
		_onChildRemoved?.invoke(dSnap)
	}

	fun onChildRemoved(listener: ((data: DataSnapshot?) -> Unit)?) {
		_onChildRemoved = listener
	}
}