package br.com.mario.reservalugar.ui.widgets

import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue

import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.OffsetMapping
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.TransformedText
import androidx.compose.ui.unit.TextUnit

import br.com.mario.reservalugar.ui.widgets.textfields.TextFieldErrorBasic
import br.com.mario.reservalugar.ui.widgets.textfields.InputValidator
import br.com.mario.reservalugar.ui.widgets.textfields.InputWrapper

import java.lang.IllegalArgumentException
import java.lang.NullPointerException
import java.lang.NumberFormatException
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.Currency
import java.util.Locale
import kotlin.math.pow

/** Encapsula o campos de TextField para tratar de valores monetários
 *
 * Created on 15/12/2021
 * Lst mod on 16/03/2022
 */
class CurrencyTextBuilder {
	private var mEraseWhenZero = false
	private var mDecimalPoints = 0
	private var mShowSymbol = false
	private var groupDivider = 0.toChar()
	private var monetaryDivider = 0.toChar()
	private var numberFormat: DecimalFormat? = null
	private var currencySymbol: String? = null

	val currencyDouble: String.() -> Double = {
		val text = this.replace(groupDivider, '\u0020').replace(monetaryDivider, '\u0020')
			.replace(".", "").replace(" ", "")
			.replace(currencySymbol!!, "").replace("\\s".toRegex(), "")

		if (mShowSymbol)
			text.replace(currencySymbol!!, "").toDouble() / 10.0.pow(fractionDigits.toDouble())
		else
			text.toDouble() / 10.0.pow(fractionDigits.toDouble())
	}
	val cleanValue: (Double) -> String = { ((it * 100).toInt()).toString() }

	private var fractionDigits = 0

	@Composable
	/** Build a component that shows a number in monetary notation, but don't allow editing */
	fun TextCurrencyLabel(fColor: Color, fSize: TextUnit, fontWeight: FontWeight, text: TextFieldValue ) =
		CurrencyText(TextStyle(fColor, fSize, fontWeight),
			mGroupDivider = '.', mMonetaryDivider = ',', init = text, showSymbol = true, mEnable = false)

	@Composable
	/** Build a component that shows a number in monetary notation, allow editing */
	fun TextCurrencyEditable(modifier: Modifier = Modifier, textStyle: TextStyle, txt: String, callback: (String, Boolean) -> Unit) {
		CurrencyTextCallback(modifier, fontStyle = textStyle, mGroupDivider = '.', mMonetaryDivider = ',', init = txt, callback, true)
	}

	@Composable
	private fun CurrencyText(fontStyle: TextStyle, mGroupDivider: Char, mMonetaryDivider: Char, init: TextFieldValue, locale: Locale = Locale("pt", "BR"), showSymbol: Boolean = false, mEnable: Boolean = true) {
		mShowSymbol = showSymbol
		configure(locale, mGroupDivider, mMonetaryDivider)

		BasicTextField( modifier = Modifier.width(IntrinsicSize.Min), // força largura se adequar ao texto
			value = init, onValueChange = {/* somente mostra valores */},
			enabled = mEnable,
			textStyle = fontStyle,
			keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
			visualTransformation = { formatterVisual(it, OffsetMapping.Identity) }
		)
	}

	@Composable
	private fun CurrencyTextCallback(modifier: Modifier, fontStyle: TextStyle, mGroupDivider: Char, mMonetaryDivider: Char, init: String, callback: (String, Boolean) -> Unit, showSymbol: Boolean = false, mEnable: Boolean = true) {
		mShowSymbol = showSymbol
		configure(Locale("pt", "BR"), mGroupDivider, mMonetaryDivider)

		val translator = object : OffsetMapping {
			override fun originalToTransformed(offset: Int): Int {
				if (offset == 0) return offset
				if (offset == 1) return offset + 6
				if (offset == 2) return offset + 5
				if (offset == 3) return offset + 4
				if (offset <= 6) return offset + 3 // três dígitos pós vírgula
				if (offset <= 9) return offset + 4 // ponto aparece em 7
				return offset + 5 // segundo ponto a partir de offset 10
			}
			override fun transformedToOriginal(offset: Int): Int {
				if (offset == 0) return offset
				if (offset == 1) return offset - 6
				if (offset == 2) return offset - 5
				if (offset == 3) return offset - 4
				if (offset == 4) return offset - 3
				if (offset == 5) return offset - 3
				if (offset == 6) return offset - 3
				if (offset <= 9) return offset - 4 // ponto aparece em 7
				return offset - 5 // segundo ponto a partir de offset 10
			}
		}

		var value by remember { mutableStateOf(InputWrapper(init)) }

		TextFieldErrorBasic(
			modifier = modifier,
			input = value,
			placeholder = "R$ 0,00",
			bgColor = Color.LightGray,
			type = KeyboardType.Number,
			visual = { formatterVisual(it, translator) },
			limit = 8,
			onValueChange = {
				value = InputWrapper(it, InputValidator.getValueErrorOrNull(it))
				callback(it, it.isNotBlank() && (value.errorId == null))
			}
		)
	}

	private fun configure(locale: Locale, mGroupDivider: Char, mMonetaryDivider: Char) {
		var success = false
		while (!success) {
			try {
				if (fractionDigits == 0) fractionDigits = Currency.getInstance(locale).defaultFractionDigits
				val symbols = DecimalFormatSymbols.getInstance(locale)
				if (mGroupDivider.code > 0) symbols.groupingSeparator = mGroupDivider
				groupDivider = symbols.groupingSeparator
				if (mMonetaryDivider.code > 0) symbols.monetaryDecimalSeparator = mMonetaryDivider
				monetaryDivider = symbols.monetaryDecimalSeparator
				currencySymbol = symbols.currencySymbol

				val df = DecimalFormat.getCurrencyInstance(locale) as DecimalFormat
				numberFormat = DecimalFormat(df.toPattern(), symbols)
				if (mDecimalPoints > 0)
					numberFormat!!.minimumFractionDigits = mDecimalPoints
				success = true

			} catch (e: IllegalArgumentException) { }
		}
	}

	private fun formatterVisual(message: AnnotatedString, translator: OffsetMapping = OffsetMapping.Identity) : TransformedText {
		message.ifEmpty { return TransformedText(AnnotatedString(""), translator) }

		var temp = message.text//.replace("[^\\d]".toRegex(), "")
		if (checkErasable(temp))
			return TransformedText(AnnotatedString(""), translator) // clear
		else {
			try { temp = format(temp) }
			catch (e: NumberFormatException) {}
			catch (e: NullPointerException) {}

			return TransformedText(AnnotatedString(temp), translator)
		}
	}

	private fun checkErasable(text: String) = if (mEraseWhenZero) text.toInt() == 0 else false

	private fun format(text: String) = when {
		mShowSymbol -> numberFormat!!.format(text.toDouble() / 10.0.pow(fractionDigits.toDouble()))
		else -> numberFormat!!.format(text.toDouble() / 10.0.pow(fractionDigits.toDouble()))
			.replace(currencySymbol!!, "")
	}
}

object DateTextBuilder {
	@Composable
	fun TextFieldDate(text: String, callback: NotBlankCallback) {
		var dateInput by remember { mutableStateOf(InputWrapper(text, InputValidator::getDateErrorIdOrNull)) }

		TextFieldErrorBasic(
			modifier = Modifier,
			input = dateInput,
			placeholder = "01/01/2022",
			bgColor = Color.LightGray,
			type = KeyboardType.Number,
			visual = { formatterDate(it) },
			limit = 8,
			onValueChange = {
				val errorId = InputValidator.getDateErrorIdOrNull(it)
				dateInput = InputWrapper(it, errorId)
				callback(formatStr(it), it.isNotBlank() && (errorId == null))
			}
		)
	}

	private fun formatterDate(message: AnnotatedString): TransformedText {
		val translator = object : OffsetMapping {
			override fun originalToTransformed(offset: Int): Int {
				if (offset < 2) return offset
				if (offset < 4) return offset + 1
				if (offset < 8) return offset + 2
				return 10
			}
			override fun transformedToOriginal(offset: Int): Int {
				if (offset <= 1) return offset
				if (offset <= 4) return offset - 1
				if (offset <= 9) return offset - 2
				return 8
			}
		}
		return (TransformedText(AnnotatedString(formatStr(message.text)), translator))
	}

	private fun formatStr(text: String) = text.foldIndexed(StringBuilder(""), { i, out, c ->
		out.apply {
			append(c)
			if (i == 1 || i == 3) append('/')
		}
	}).toString()
}

typealias NotBlankCallback = (String, Boolean) -> Unit