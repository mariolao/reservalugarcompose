package br.com.mario.reservalugar.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.PropertyName

private const val NAME_TAG = "nome"
private const val TABLE_TAG = "mesas"
private const val PARK_TAG = "vaga"

/**
 * Created on 13/04/2022
 * Lst mod on 30/06/2022
 */
data class BuyerObject(
	@get:PropertyName(NAME_TAG) val name: String = "", @get:PropertyName(TABLE_TAG) var tables: String = "",
	@get:PropertyName(PARK_TAG) var parking: ParkingModel? = null
) {
	@get:Exclude
	val hasParkLot: Boolean
		get() = parking != null

	@get:Exclude
	private val tableList: ArrayList<String>
		get() = arrayListOf(*tables.split(',').toTypedArray())
	private val tempList: ArrayList<String> = arrayListOf()

	@get:Exclude
	internal val hasMoreThanOne
		get() = tableList.size > 1

	init {
		tempList.addAll(tableList)
		updateTableString()
	}

	fun addTable(table: String) = this.apply { tempList.add(table); updateTableString() }
	fun addTables(list: List<String>) { tempList.addAll(list); updateTableString() }

	fun unlinkTable(table: String) {
		tables = tempList.apply { remove(table) }.joinToString(",")
	}
	/** Atualiza string de mesas em ordem alfabética. @since 1.5.6.007 */
	private fun updateTableString() {
		tables = tempList.apply { sortBy { it.toInt() } }.joinToString(",")
	}

	companion object {
		fun fromMap(item: Any) = with(item as HashMap<*, *>) {
			BuyerObject(this[NAME_TAG].toString(), this[TABLE_TAG].toString(), ParkingModel.fromMap(this[PARK_TAG] as HashMap<*, *>?))
		}
	}
}

/** Lst mod on 21/04/2022 */
data class ParkingModel(
	@get:PropertyName("data") val date: String,
	@get:PropertyName("pagamento") val paid: Boolean = false,
	@get:PropertyName("carteira") val wallet: String = "",
) {
	companion object {
		fun fromMap(item: HashMap<*, *>?) =
			item?.let { ParkingModel(it["data"].toString(), it["pagamento"] as Boolean, it["carteira"].toString()) }

		fun toPayMap(walletStr: String, paid: Boolean): HashMap<String, Any> =
			hashMapOf("carteira" to walletStr, "pagamento" to paid)
	}
}