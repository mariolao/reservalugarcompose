package br.com.mario.reservalugar.ui.settings

import android.content.Context
import android.content.Intent

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape

import androidx.compose.material.Card
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.material.TextButton

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

import br.com.mario.reservalugar.GenerateParkingPDF
import br.com.mario.reservalugar.GenerateTablePDF
import br.com.mario.reservalugar.model.BuyerObject
import br.com.mario.reservalugar.model.TableObject
import br.com.mario.reservalugar.relaunchApp

import br.com.mario.reservalugar.ui.theme.MyColors.Holo_Bluedark
import br.com.mario.reservalugar.ui.theme.MyColors.Holo_RedDragon
import br.com.mario.reservalugar.ui.widgets.dialogs.AlertResetApplication

import com.google.firebase.auth.FirebaseAuth
import java.io.File

/**
 * Created on 07/01/2022
 * Lst mod on 30/06/2022
 */

@Composable
fun SettingRoute(settingVM: SettingViewModel) {
	val uiState by settingVM.uiState.collectAsState()
	SettingScreen(uiState, settingVM::confirmFlagChange, settingVM::shareFile, settingVM::switchBDFlag)
}

@Composable
private fun SettingScreen(
	uiState: SettingViewModelState,
	confirmSwitcher: (block: () -> Unit) -> Unit,
	onShareFile: (file: File, context: Context) -> Unit,
	switchDialog: (switch: Boolean, open: Boolean) -> Unit,
) {
	val tableList = uiState.tableReservationList
	val parkList = uiState.parkingList

	val current = LocalContext.current
	Box {
		Column(Modifier.wrapContentHeight().padding(10.dp)) {
			SectorProfile(uiState)
			Spacer(Modifier.height(15.dp))

			SectorReports(title = "Relatórios", tableList, parkList, onShareFile)
			Spacer(Modifier.height(10.dp))

			Row(Modifier.fillMaxWidth(), Arrangement.SpaceBetween, Alignment.CenterVertically) {
				Text("Banco de Dados em modo teste: ")
				Switch(checked = uiState.testDBFlag, onCheckedChange = {
					switchDialog(it, true)
				})
			}
			//todo adicionar área de mudança de valores dos produtos, caso seja ADM
		}

		if (uiState.openAlertChangeFlag) {
			AlertResetApplication(
				onDismiss = { switchDialog(!uiState.testDBFlag, false) }, // desfaz mudança de estado e fecha o Alerta
				onPositive = { // confirma mudança de estado, salva com delay e reinicia aplicação
					confirmSwitcher {
						current.relaunchApp()
					}
				})
		}
	}
}

/**
 * Gerencia a área em que o vendedor pode verificar seu perfil
 * Created on 09/05/2022
 */
@Composable
private fun SectorProfile(uiState: SettingViewModelState, modifier: Modifier = Modifier, corner: Int = 20, elevation: Dp = 10.dp) {
	// todo acesso ao relatório de operações
	// alterar senha
	Card(modifier.fillMaxWidth().wrapContentHeight(), shape = RoundedCornerShape(corner), elevation = elevation) {
		Column(Modifier.padding(start = 15.dp, end = 15.dp, top = 20.dp)) {
			Row(Modifier.fillMaxWidth(), Arrangement.SpaceBetween) {
				Column(verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.Start) {
					Text(uiState.localUser.name, fontWeight = FontWeight.Bold)
					Spacer(Modifier.height(10.dp))
					Text(uiState.localUser.email, fontSize = 15.sp)
				}

				Box(Modifier.background(
					color = if (uiState.localUser.adm) Holo_Bluedark else Holo_RedDragon,
					shape = RoundedCornerShape(10.dp)
				).padding(5.dp),
					content = { Text("ADM") }
				)
			}
			//todo adicionar linha tênue divisória

			Row(Modifier.fillMaxWidth(), Arrangement.SpaceBetween, Alignment.CenterVertically) {
				Text("Notificações") // adicionar subtexto explicativo
				Switch(checked = uiState.testDBFlag, onCheckedChange = {
				})
			}
		}
	}
}

/** Created on 07/01/2022 */
@Composable
private fun SectorReports(
	title: String,
	tableList: List<TableObject>,
	parkList: List<BuyerObject>,
	onShareFile: (File, Context) -> Unit,
	corner: Int = 20,
	elevation: Dp = 10.dp,
) {
	val ctx = LocalContext.current
	Card(Modifier.fillMaxWidth().wrapContentHeight(), shape = RoundedCornerShape(corner), elevation = elevation) {
		Column(Modifier.padding(15.dp)) {
			Text(title, Modifier.padding(start = 0.dp).background(Color.White))
			Spacer(Modifier.height(5.dp))
			Row(Modifier.padding(vertical = 8.dp).border(1.dp, Color.Black).fillMaxWidth(), Arrangement.SpaceEvenly) {
				TextButton(
					onClick = {
						GenerateTablePDF(ctx, tableList).generate()?.let { onShareFile(it,  ctx) }
						if (parkList.isNotEmpty())
							GenerateParkingPDF(ctx, parkList).generate()?.let { onShareFile(it,  ctx) }
					},
					content = { Text("Ganhos") }
				)
				TextButton(
					onClick = { /*TODO gerar relatório de gastos*/ },
					content = { Text("Gastos") }
				)
				TextButton(
					onClick = { /*TODO gerar relatório de extrato*/ },
					content = { Text("Histórico") }
				)
			}

		}
	}
}

@Preview
@Composable
private fun PreviewSetting() {
	SettingScreen(uiState = SettingViewModelState(testDBFlag = true, currentUser = FirebaseAuth.getInstance().currentUser), switchDialog = { _, _ ->}, confirmSwitcher = {}, onShareFile = {_,_ -> Intent()})
}

@Preview
@Composable
fun PreviewSession() {
	SectorReports("Referências", listOf(TableObject()), listOf(BuyerObject()), onShareFile = {_,_ -> Intent()})
}