package br.com.mario.reservalugar

import android.app.Application
import br.com.mario.reservalugar.data.AppContainer
import br.com.mario.reservalugar.data.AppConteinerImpl
import net.danlew.android.joda.JodaTimeAndroid

/** Created on 11/04/2022 */
class ReservaLugarApplication : Application() {
	lateinit var container: AppContainer

	override fun onCreate() {
		super.onCreate()
		container = AppConteinerImpl(this)
		JodaTimeAndroid.init(this) // inicializa API de celendário
	}
}