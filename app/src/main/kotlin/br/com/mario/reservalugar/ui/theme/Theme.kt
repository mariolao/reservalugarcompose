package br.com.mario.reservalugar.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

import br.com.mario.reservalugar.ui.theme.MyColors.Purple200
import br.com.mario.reservalugar.ui.theme.MyColors.Purple500
import br.com.mario.reservalugar.ui.theme.MyColors.Purple700
import br.com.mario.reservalugar.ui.theme.MyColors.Teal200

private val DarkColorPalette = darkColors(
	primary = Purple200,
	primaryVariant = Purple700,
	secondary = Teal200
)

private val LightColorPalette = lightColors(
	primary = Purple500,
	primaryVariant = Purple700,
	secondary = Teal200,
	surface = Color.White

	/* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onSecondary = Color.Black,
    onBackground = Color.Black,
    onSurface = Color.Black,
    */
)

@Composable
fun ReservaLugarTheme(/*darkTheme: Boolean = false, */content: @Composable () -> Unit) {
	val colors = LightColorPalette

	MaterialTheme(
		colors = colors,
		typography = Typography,
		shapes = Shapes,
		content = content,
	)
}