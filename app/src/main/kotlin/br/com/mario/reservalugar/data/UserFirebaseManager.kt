package br.com.mario.reservalugar.data

import br.com.mario.reservalugar.UserDB
import br.com.mario.reservalugar.model.UserObject
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * Gerencia os dados relacionados com usuário, tanto para autenticação quanto dados de vendedor
 *
 * Created on 12/05/2022
 * Lst mod 19/05/2022
 */
class UserFirebaseManager {
	private val _userMap = MutableStateFlow<Map<String, UserObject>>(hashMapOf())
	val userMapFlow = _userMap

	private val _currentUser = MutableStateFlow(FirebaseAuth.getInstance().currentUser)
	val currentUser get() = _currentUser.value
	val currentUserFlow = _currentUser

	private val _localCurrentUser = MutableStateFlow(UserObject())
	val localCurrentUserFlow = _localCurrentUser
	val localCurrentUser: ResultWrapper<UserObject>
		get() = if (_localCurrentUser.value.hash == null) ResultWrapper.Error("usuário local ainda não carregado")
		else ResultWrapper.Success(_localCurrentUser.value)

	val isCurrentFirstAccess
		get() = _userMap.value[_currentUser.value!!.uid]!!.firstAccess

	init {
		UserDB.loadUsers { users ->
			val transformed = users.mapValues { (key, item) -> UserObject.fromMap(item, key) }
			_userMap.value = transformed
		}
		UserDB.changeUsers {
			onChildChanged { data, oldName ->
				val mtMap = (_userMap.value as MutableMap)
				mtMap[data.key!!] = UserObject.fromMap(data.value, data.key!!)
				_userMap.value = mtMap
			}
		}

		FirebaseAuth.getInstance().addAuthStateListener { auth ->
			auth.currentUser?.let { firebaseUser ->
				_currentUser.value = firebaseUser

				UserDB.loadCurrentUser(firebaseUser.uid) {
					_localCurrentUser.value = UserObject.fromMap(it, firebaseUser.uid)
				}
			}
		}
		if (currentUser != null) {
			UserDB.loadCurrentUser(currentUser!!.uid) {
				_localCurrentUser.value = UserObject.fromMap(it, currentUser!!.uid)
			}
		}
	}

	/*fun getUserMap(): ResultWrapper<Map<String, UserObject>> {
	}*/

	fun authenticate(usermail: String, password: String, result: (Boolean) -> Unit = {}) {
		FirebaseAuth.getInstance().signInWithEmailAndPassword(usermail, password).addOnCompleteListener { task ->
			result(task.isSuccessful)
		}
	}

	fun setSecondAccess() {
		currentUser?.let {
			UserDB.setAccess(it.uid, false)
		}
	}
}