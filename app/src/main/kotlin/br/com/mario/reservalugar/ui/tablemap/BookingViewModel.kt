package br.com.mario.reservalugar.ui.tablemap

import android.app.Application
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.widget.Toast

import androidx.core.content.FileProvider

import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope

import br.com.mario.reservalugar.IMG_FILE
import br.com.mario.reservalugar.IMG_FOLDER
import br.com.mario.reservalugar.R
import br.com.mario.reservalugar.TITLE_SHARE_IMG
import br.com.mario.reservalugar.TODAY
import br.com.mario.reservalugar.data.BuyerRepository
import br.com.mario.reservalugar.data.ReportRepository
import br.com.mario.reservalugar.data.ResultWrapper
import br.com.mario.reservalugar.data.TableRepository
import br.com.mario.reservalugar.data.ValuesRepository

import br.com.mario.reservalugar.model.BuyerObject
import br.com.mario.reservalugar.model.TableObject
import br.com.mario.reservalugar.utils.PATHS
import br.com.mario.reservalugar.utils.ScreenshotState
import br.com.mario.reservalugar.utils.writeBitmap

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

import java.io.File
import kotlin.NullPointerException

/**
 * Created on 10/04/2022
 * Lst mod on 01/07/2022
 */
class BookingViewModel(
	private val buyerRepository: BuyerRepository,
	private val reportRepository: ReportRepository,
	private val tableRepository: TableRepository,
	private val valuesRepository: ValuesRepository,
	ctx: Application
) : AndroidViewModel(ctx) {
	private val viewModelState = MutableStateFlow(TableViewModelState(isLoading = true))
	private val longToast = { res: Int ->
		Toast.makeText(getApplication(), res, Toast.LENGTH_LONG).show()
	}

	val uiState = viewModelState.map { it.toUIState() }.stateIn(viewModelScope, SharingStarted.Eagerly, viewModelState.value.toUIState())

	init {
		initialTableState()
		initialBuyerState()

		viewModelScope.launch {
			tableRepository.observeTableReserves().collect { mappingTables ->
				viewModelState.update { it.copy(tableList = mappingTables) }
			}
		}
		viewModelScope.launch {
			buyerRepository.observeBuyerReserves().collect { buyerMapping ->
				viewModelState.update { it.copy(buyerMap = buyerMapping) }
			}
		}
	}

	private fun initialTableState() {
		viewModelState.update { it.copy(isLoading = true) }

		viewModelScope.launch {
			val result = tableRepository.getTableReserves()

			viewModelState.update {
				when(result) {
					is ResultWrapper.Success -> it.copy(tableList = result.data, isLoading = false)
					is ResultWrapper.Error -> {
						val errorMessages = it.errorMessages + "Some new error"
						it.copy(errorMessages = errorMessages, isLoading = false)
					}
				}
			}
		}
	}
	private fun initialBuyerState() {
		viewModelState.update { it.copy(isLoading = true) }

		viewModelScope.launch {
			val result = buyerRepository.getBuyerReserves()

			viewModelState.update {
				when(result) {
					is ResultWrapper.Success -> it.copy(buyerMap = result.data, isLoading = false)
					is ResultWrapper.Error -> it.copy(errorMessages = it.errorMessages + result.message!!, isLoading = false)
				}
			}
		}
	}

	/* GERENCIAMENTO DE DIÁLOGOS -> */
	/** @since Beta 1.5.6.12 */
	fun showChangeOwner() = viewModelState.update { it.copy(openDetails = false, openOwner = true) }
	fun showMultipleReservation(open: Boolean = false) = viewModelState.update { it.copy(openVarious = open) }
	fun selectTable(hasOwner: Boolean, tableNumb: Int) =
		viewModelState.update { it.copy(selectedTableId = tableNumb, openDetails = hasOwner, openOne = !hasOwner) }
	fun closeAllDialogs() = viewModelState.update { it.copy(openOwner = false, openDetails = false, openOne = false, openVarious = false) }
	fun openAlert(isAlertExclusion: Boolean) {
		viewModelState.update { it.copy(openExclusion = isAlertExclusion, openWarning = !isAlertExclusion) }
	}
	fun closeAllAlerts() = viewModelState.update { it.copy(openExclusion = false, openWarning = false, message = "") }
	/* <- DIÁLOGOS | MESAS -> */
	fun addTableReserve(buyerName: String, table: String, date: String) {
		val buyerReserve = buyerRepository.getBuyerReserve(buyerName)

		if (buyerReserve != null) {
			val addReserve = buyerReserve.addTable(table) // instância com mesas atualizadas
			buyerRepository.setOfflineBuyer(buyerName, addReserve)
			buyerRepository.includeOwner(addReserve)
		}
		else
			buyerRepository.includeOwner(BuyerObject(buyerName, table))

		tableRepository.reserveTable(table, buyerName, date)

		valuesRepository.updateRemaining(PATHS.TABLE, -1)
		reportRepository.addReserve("reserva", buyerName, "mesa $table", "atual")
	}

	fun addTablesReserve(tableStr: String, buyerName: String, date: String) {
		val split = tableStr.split(",")
		val ocp = ArrayList<String>(0)

		/* TESTES PRÉVIOS */
		try { split.forEach {
			if (uiState.value.tableList[it.toInt()].hasOwner) ocp.add(it)
		} }
		catch (e: Exception) { return }

		if (ocp.isNotEmpty()) { // atualizar chamada para abrir alerta de ocupação
			viewModelState.update {
				it.copy(openWarning = true,
					openVarious = false,
					message = "As seguintes mesas estão ocupadas:\n$ocp")
			}
			return
		} // se há mesas ocupadas
		/* TESTES PRÉVIOS */
		addTablesReserve(tableStr, split, buyerName, date)
	}
	private fun addTablesReserve(tableStr: String, tableList: List<String>, buyerName: String, date: String) {
		val buyerReserve = buyerRepository.getBuyerReserve(buyerName)

		if (buyerReserve != null)
			buyerRepository.includeOwner(buyerReserve.apply { addTables(tableList) })
		else
			buyerRepository.includeOwner(BuyerObject(buyerName, tableStr))

		val children = tableList.associateWith { TableObject(buyerName, date) }
		tableRepository.reserveTable(children)

		valuesRepository.updateRemaining(PATHS.TABLE, -tableList.size)
		reportRepository.addReserve("reservas", buyerName, "mesas $tableStr", "atual")
	}

	fun prepareToRemoveTableReservation(table: TableObject, buyer: BuyerObject) =
		viewModelState.update {
			it.copy(openExclusion = true,
				selectedTableId = table.id.toInt(),
				message = "Confirmar exclusão da mesa ${table.id} no nome de ${buyer.name}?\n" +
						"Verificar pagamento de extra e estacionamento")
		}
	fun removeTableReserve(table: TableObject, buyer: BuyerObject) {
		if (buyer.hasMoreThanOne) {
			buyer.unlinkTable(table.id)
			//todo atualizar apenas o nó "mesas" em comprador no BD
		} else
			buyerRepository.excludeOwner(buyer)

		//todo fazer operações de estorno:
		// testar se mesa está paga
		// testar se extra está paga
		// testar se vaga está paga
		tableRepository.wipeTable(table.id)

		longToast(R.string.alert_reserve_off)
		valuesRepository.updateRemaining(PATHS.TABLE, 1)
		reportRepository.addReserve("remoção", buyer.name, "mesa ${table.id}", "atual")
	}

	/**
	 * Update a table's reserve ownership.
	 * @since 1.5.6.11 - 29/06/2022
	 */
	fun changeOwner(table: TableObject, buyer: BuyerObject, newOwner: String) {
		closeAllDialogs()

		val changedBuyer = buyerRepository.getBuyerReserve(newOwner)

		if (changedBuyer != null) { // comprador alvo já existe
			val addReserve = changedBuyer.addTable(table.id)

			if (buyer.hasMoreThanOne) {
				buyer.unlinkTable(table.id) // desvincula mesa atual
				buyerRepository.includeOwner(buyer) // atualiza BD com o comprador modificado
				buyerRepository.includeOwner(addReserve) // adiciona novo comprador com a mesa
			} else { // só uma mesa - remove comprador anterior e adiciona novo
				buyerRepository.excludeOwner(buyer)
				buyerRepository.includeOwner(addReserve)
			}
		} else {
			if (buyer.hasMoreThanOne) {
				buyer.unlinkTable(table.id) // desvincula mesa atual
				buyerRepository.includeOwner(buyer) // atualiza BD com o comprador modificado
				buyerRepository.includeOwner(buyer.copy(name = newOwner, tables = table.id)) // adiciona novo comprador com a mesa
			} else { // só uma mesa - remove comprador anterior e adiciona novo
				buyerRepository.excludeOwner(buyer)
				buyerRepository.includeOwner(buyer.copy(name = newOwner))
			}
		}
		tableRepository.reserveTable(mapOf(table.id to table.copy(buyer = newOwner))) // atualiza comprador

		longToast(R.string.alert_ownership_changed)
		//		reportRepository todo criar relatório de troca de titularidade ?
	}

	/* <- GERENCIAMENTO DE MESAS */
	fun manageExtraReserve(table: TableObject, setExtraReservation: Boolean) {
		if (setExtraReservation) {
			if (valuesRepository.isExtraLeft) {
				tableRepository.attachExtraToTable(table.id, TODAY)
				valuesRepository.updateRemaining(PATHS.EXTRA, -1)
				reportRepository.addReserve("reserva", table.buyer, "extra de ${table.id}", "atual")
			}
			else longToast(R.string.alert_extra_unavailable)
		} else {
			try {
				if (!table.extra!!.paid) {
					tableRepository.detachExtraToTable(table.id)
					valuesRepository.updateRemaining(PATHS.EXTRA, 1)
					reportRepository.addReserve("remoção", table.buyer, "extra ${table.id}", "atual")
				}
				else longToast(R.string.alert_deletion_not_alowed)

			} catch (n: NullPointerException) {
				//todo atualização manual de Extra
				closeAllDialogs()
				longToast(R.string.alert_out_of_sync)
			}
		}
	}

	fun manageParkReserve(buyer: BuyerObject, setParkReservation: Boolean) {
		if (setParkReservation) {
			 if (valuesRepository.isParkLotLeft) {
				buyerRepository.attachParkingToBuyer(buyer.name, TODAY)
				 valuesRepository.updateRemaining(PATHS.PARK, -1)
				 reportRepository.addReserve("reserva", buyer.name, "vaga", "atual")
			} else longToast(R.string.alert_lots_unavailable)
		} else {
			try {
				if (!buyer.parking!!.paid) {
					buyerRepository.detachParkingToBuyer(buyer.name)
					valuesRepository.updateRemaining(PATHS.PARK, 1)
					reportRepository.addReserve("remoção", buyer.name, "vaga", "atual")
				}
				else longToast(R.string.alert_deletion_not_alowed)

			} catch (n: NullPointerException) {

			}
		}
	}
	/** @since Beta 1.5.6.12 */
	fun makeScreenshot(ctx: Context, screenshotState: ScreenshotState) {
		screenshotState.capture()
		val shareFile = File(File(ctx.cacheDir, IMG_FOLDER).apply { mkdirs() }, IMG_FILE)
		shareFile.writeBitmap(screenshotState.bitmap!!, Bitmap.CompressFormat.PNG, 100) // overwrites this image every time
		shareIntent(ctx, shareFile)
	}
	/** @since Beta 1.5.6.12 */
	private fun shareIntent(ctx: Context, shareFile: File) {
		val contentUri = FileProvider.getUriForFile(ctx, ctx.getString(R.string.authority), shareFile)
		Intent().apply {
			action = Intent.ACTION_SEND
			addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
			setDataAndType(contentUri, ctx.contentResolver.getType(contentUri))
			putExtra(Intent.EXTRA_STREAM, contentUri)
			type = "image/*"

			ctx.startActivity(Intent.createChooser(this, TITLE_SHARE_IMG))
		}
	}

	companion object {
		fun provideFactory(
			context: Application,
			buyerRepository: BuyerRepository,
			report: ReportRepository,
			tableRepository: TableRepository,
			valuesRepository: ValuesRepository
		): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
			override fun <T : ViewModel> create(modelClass: Class<T>): T {
				return BookingViewModel(buyerRepository, report, tableRepository, valuesRepository, context) as T
			}
		}
	}
}

/** Created on 10/04/2022 */
private data class TableViewModelState(
	val tableList: List<TableObject>? = null,
	val buyerMap: Map<String, BuyerObject>? = null,
	val selectedTableId: Int = 0,
//	val selectedBuyerId: String = "",
	val openDetails: Boolean = false,
	val openOne: Boolean = false,
	val openOwner: Boolean = false,
	val openVarious: Boolean = false,
	val openExclusion: Boolean = false,
	val openWarning: Boolean = false,
	val message: String = "",
	val hasOwner: Boolean = false,
	val isLoading: Boolean = false,
	val errorMessages: List<String> = emptyList()
) {
	fun toUIState() : TableUiState =
		if (tableList == null || buyerMap == null) {
			TableUiState.NotLoaded(
				isLoading = isLoading,
				tableList = emptyTableList(),
				errorMessages = errorMessages
			)
		} else {
			TableUiState.Loaded(
				selectedTable = if (selectedTableId == 0) null else tableList[selectedTableId],
				openDetail = openDetails,
				hasOwner = hasOwner,
				openDialogOne = openOne,
				openDialogVarious = openVarious,
				openDialogChangeOwner = openOwner,
				isLoading = isLoading,
				openTableReservationExclusion = openExclusion,
				openTablesReservationWarning = openWarning,
				msgBody = message,
				tableList = tableList,
				buyerMap = buyerMap,
				errorMessages = errorMessages
			)
		}

	private fun emptyTableList() = ArrayList<TableObject>(121).also {
		for (i in 0..120)
			it.add(TableObject())
	}
}
/** Created on 10/04/2022 */
sealed interface TableUiState {
	val isLoading: Boolean
	val tableList: List<TableObject>
	val buyerMap: Map<String, BuyerObject>
	val errorMessages: List<String>

	data class NotLoaded(
		override val isLoading: Boolean,
		override val tableList: List<TableObject> = emptyList(),
		override val buyerMap: Map<String, BuyerObject> = emptyMap(),
		override val errorMessages: List<String>
	) : TableUiState

	data class Loaded(
		val hasOwner: Boolean = false,
		val openDetail: Boolean = false,
		val openDialogOne: Boolean,
		val openDialogVarious: Boolean,
		val openDialogChangeOwner: Boolean,
		val openTableReservationExclusion: Boolean,
		val openTablesReservationWarning: Boolean,
		val msgBody: String = "",
		override val isLoading: Boolean,
		override val tableList: List<TableObject>,
		override val buyerMap: Map<String, BuyerObject>,
		override val errorMessages: List<String>,
		val selectedTable: TableObject?,
	) : TableUiState {
		val selectedBuyer: BuyerObject?
			get() = if (selectedTable != null) buyerMap[selectedTable.buyer] else null
	}
}