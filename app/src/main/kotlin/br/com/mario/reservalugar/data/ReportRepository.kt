package br.com.mario.reservalugar.data

import br.com.mario.reservalugar.ReportDB
import br.com.mario.reservalugar.TODAY
import br.com.mario.reservalugar.model.CostReport
import br.com.mario.reservalugar.model.GainReport
import br.com.mario.reservalugar.model.ReportObject
import br.com.mario.reservalugar.model.TableReport

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * Repositório do relatório de operações da aplicação
 *
 * @author Mário Henrique
 * Created on 03/05/2022
 */
class ReportRepository {
	private val reportList = MutableStateFlow<List<ReportObject>>(emptyList())
	private var reportMap : HashMap<String, ReportObject>? = null

	init {
		ReportDB.loadGeneral { data ->
			reportMap = HashMap()
			data.forEach { (key, item) -> reportMap!![key] = ReportObject.fromMap(item) }
			reportList.value = reportMap!!.map { it.value }
		}
		ReportDB.changeGeneral {
			onChildAdded { data, _ ->
				val item = ReportObject.fromMap(data.value!!)
				reportMap?.set(data.key!!, item)
				reportList.value = reportMap?.map { it.value } ?: emptyList()
			}
		}
	}

	fun observeReportList(): Flow<List<ReportObject>> = reportList

	fun addCost(op: String, desc: String, seller: String, value: Double, walletStr: String) =
		ReportDB.report(CostReport(TODAY, "gasto $op", desc, seller, value, walletStr))
	fun addPayment(buyer: String, desc: String, seller: String, price: Double, walletStr: String) =
		ReportDB.report(GainReport(TODAY, "pagamento", desc, buyer, seller, price, walletStr))
	fun addReserve(op: String, buyer: String, tg: String, seller: String) =
		ReportDB.report(TableReport(TODAY, op, tg, buyer, seller))

	fun getList() : ResultWrapper<List<ReportObject>> =
		if (reportMap == null) ResultWrapper.Error("Itens não carregados")
	else ResultWrapper.Success(reportList.value)
}