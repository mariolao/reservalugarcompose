package br.com.mario.reservalugar.model

import com.google.firebase.database.Exclude
import com.google.firebase.database.PropertyName

private const val NAME_TAG = "nome"
private const val FIRST_TAG = "primeiro_acesso"
private const val ADM_TAG = "administrador"

/**
 * @author Mário Henrique
 * Created on 12/05/2022
 * Lst mod on 19/05/2022
 */
data class UserObject(
	@get:PropertyName(NAME_TAG) val name: String = "", val email: String = "",
	@get:PropertyName(FIRST_TAG) val firstAccess: Boolean = false,
	@get:PropertyName(ADM_TAG) val adm: Boolean = false,
	@get:Exclude val hash: String? = null
) {
	companion object {
		fun fromMap(map: Any?, key: String) =
			if (map == null) UserObject()
			else {
				with(map as HashMap<*, *>) {
					UserObject(this[NAME_TAG].toString(), this["email"].toString(), this[FIRST_TAG] as Boolean, this[ADM_TAG] as Boolean, key)
				}
			}
	}
}