package br.com.mario.reservalugar.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras
import androidx.lifecycle.viewmodel.compose.viewModel

import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

import br.com.mario.reservalugar.R
import br.com.mario.reservalugar.data.AppContainer
import br.com.mario.reservalugar.nav.Routes

import br.com.mario.reservalugar.ui.cost.CostRoute
import br.com.mario.reservalugar.ui.cost.CostViewModel
import br.com.mario.reservalugar.ui.gains.GainRoute
import br.com.mario.reservalugar.ui.gains.GainViewModel
import br.com.mario.reservalugar.ui.login.LogonViewModel
import br.com.mario.reservalugar.ui.login.LogonRoute
import br.com.mario.reservalugar.ui.resume.ResumeScreen
import br.com.mario.reservalugar.ui.resume.ResumeTabViewModel
import br.com.mario.reservalugar.ui.resume.StatementViewModel
import br.com.mario.reservalugar.ui.settings.SettingRoute
import br.com.mario.reservalugar.ui.settings.SettingDataStore
import br.com.mario.reservalugar.ui.settings.SettingViewModel
import br.com.mario.reservalugar.ui.tablemap.TableScreen
import br.com.mario.reservalugar.ui.tablemap.BookingViewModel
import br.com.mario.reservalugar.ui.theme.MyColors
import br.com.mario.reservalugar.ui.theme.ReservaLugarTheme
import br.com.mario.reservalugar.userManager

import br.mariodeveloper.menucollapsable.MenuCollapsable
import br.mariodeveloper.menucollapsable.MenuItemModel
import br.mariodeveloper.menucollapsable.utils.CONDITIONAL_TAG
import br.mariodeveloper.menucollapsable.utils.WindowInfo

/**
 * @author Mário Henrique
 * Created on 09/04/2022
 * Lst mod on 27/06/2022
 */
@Composable
fun ReservaLugarComposeEntry(appContainer: AppContainer, windowInfo: WindowInfo) {
//	val splash = "splashscreen"
	val core = "menusession"

	ReservaLugarTheme {
		val nav = rememberNavController()
		val isExpanded = windowInfo.screenWidthInfo is WindowInfo.WindowType.Medium
		val loginVM = viewModel<LogonViewModel>(factory = object : ViewModelProvider.Factory {
			override fun <T : ViewModel> create(modelClass: Class<T>): T = LogonViewModel() as T
		})

		/* SOLUÇÃO TEMPORÁRIA PARA DEIXAR USUÁRIO PADRÃO */
		userManager.authenticate("duxmario@gmail.com", "123456")
		/* SOLUÇÃO TEMPORÁRIA PARA DEIXAR USUÁRIO PADRÃO */

		NavHost(navController = nav, startDestination = core) {
			composable("splashscreen") {
				SplashScreen()
			}
			composable(Routes.LogonRoute.route) {
				LaunchedEffect(Unit) {
					nav.previousBackStackEntry?.savedStateHandle?.set(CONDITIONAL_TAG, false)
				}
				LogonRoute(
					loginVM = loginVM,
					onSuccessful = {
						nav.previousBackStackEntry!!.savedStateHandle.set(CONDITIONAL_TAG, true)
						nav.popBackStack()
					}
				)
			}
			composable(core) { backStackEntry ->
				val bookingVM: BookingViewModel = viewModel(factory = BookingViewModel.provideFactory(appContainer.app,
					appContainer.buyerRepository, appContainer.reportRepository,
					appContainer.tableRepository, appContainer.valuesRepository))
				val gainVM: GainViewModel = viewModel(factory = GainViewModel.provideFactory(
					appContainer.buyerRepository, appContainer.reportRepository,
					appContainer.tableRepository, appContainer.valuesRepository))
				val costVM: CostViewModel = viewModel(factory = CostViewModel.provideFactory(
					appContainer.costRepository, appContainer.reportRepository, appContainer.valuesRepository))
				val resmVM: ResumeTabViewModel = viewModel(factory = ResumeTabViewModel.provideFactory(appContainer.valuesRepository))
				val reportVM = viewModel<StatementViewModel>(factory = StatementViewModel.provideFactory(
					appContainer.reportRepository, appContainer.valuesRepository))
				val settingDS = SettingDataStore(appContainer.app)
				val configVM = viewModel<SettingViewModel>(factory = object : ViewModelProvider.Factory {
					override fun <T : ViewModel> create(modelClass: Class<T>, extras: CreationExtras): T =
						SettingViewModel(appContainer.buyerRepository, appContainer.tableRepository, settingDS) as T
				})

				MenuCollapsable(
					controller = rememberNavController(),
					windowInfo = windowInfo,
					hasSettings = true,
					settingRoute = "setting",
					menusOptions = listOf(
						MenuItemModel(
							MyColors.Holo_YellowShine,
							R.drawable.img_balanco,
							R.string.menu_report_title,
							Routes.ResumeRoute.route
						),
						MenuItemModel(
							MyColors.Holo_RedDragon,
							R.drawable.img_despesas,
							R.string.menu_costs_title,
							Routes.CostsRoute.route
						),
						MenuItemModel(
							MyColors.Holo_GreenGrass,
							R.drawable.img_money_bag,
							R.string.menu_payments_title,
							Routes.GainsRoute.route
						),
						MenuItemModel(
							MyColors.Holo_Bluedark,
							R.drawable.img_table3,
							R.string.menu_seats_title,
							Routes.TablesRoute.route
						)
					)
				) {
					composable(Routes.MenuRoute.route) {
						Box(Modifier.fillMaxSize())
					} // tela de fundo (vazia inicialmente)
					composable(Routes.SettingRoute.route) {
						SettingRoute(configVM)
					}
					composable(Routes.ResumeRoute.route) {
						ResumeScreen(reportVM, resmVM, isExpanded)
					}
					composable(Routes.GainsRoute.route) {
						GainRoute(gainVM, isExpanded)
					}
					composable(Routes.TablesRoute.route) {
						TableScreen(bookingVM)
					}
					composable(Routes.CostsRoute.route) {
						CostRoute(costVM, isExpanded)
					}
				}
			}
		}
	}
}