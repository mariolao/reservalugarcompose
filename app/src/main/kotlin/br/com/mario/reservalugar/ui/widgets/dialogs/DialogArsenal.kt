package br.com.mario.reservalugar.ui.widgets.dialogs

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape

import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Checkbox
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextButton

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue

import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

import br.com.mario.reservalugar.R
import br.com.mario.reservalugar.model.BuyerObject
import br.com.mario.reservalugar.model.CostObject
import br.com.mario.reservalugar.model.ParkingModel
import br.com.mario.reservalugar.model.TableObject

import br.com.mario.reservalugar.ui.theme.MyColors.Holo_Bluedark
import br.com.mario.reservalugar.ui.theme.MyColors.Holo_OrangeAutumm
import br.com.mario.reservalugar.ui.theme.MyColors.Holo_YellowShine
import br.com.mario.reservalugar.ui.theme.MyColors.IceWhite
import br.com.mario.reservalugar.ui.theme.MyColors.SeatPayNO
import br.com.mario.reservalugar.ui.theme.MyColors.SeatPayYES
import br.com.mario.reservalugar.ui.theme.Typography
import br.com.mario.reservalugar.ui.widgets.CurrencyTextBuilder
import br.com.mario.reservalugar.ui.widgets.DateTextBuilder.TextFieldDate
import br.com.mario.reservalugar.ui.widgets.textfields.MyTextField
import br.com.mario.reservalugar.utils.WALLET

import br.mariodeveloper.superspinner.Spinner

/**
 * Contém a construção das Caixas de Diálogo utilizadas na tela Mesas
 *
 * Created on 26/11/2021
 * Lst mod on 30/06/2022
 */

@Composable
internal fun DialogReserveOneContent(seat: Int, onDismiss: () -> Unit, onPositiveClick: (String) -> Unit) {
	DialogSkeleton(color = Color.Gray, head = seat) { wdtBt, hgtBt ->
		Text("Reservar:", color = Color.Gray, fontSize = 13.sp)

		var nameTxt by remember { mutableStateOf("") }
		var isNameNotBlank by remember { mutableStateOf(false) }
		MyTextField("nome do comprador", KeyboardType.Text, Color.LightGray) { str, b ->
			nameTxt = str
			isNameNotBlank = b
		}
		Spacer(Modifier.height(15.dp))

		Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
			Button(
				onClick = onDismiss,
				Modifier.size(wdtBt, hgtBt),
				colors = ButtonDefaults.buttonColors(backgroundColor = Holo_OrangeAutumm),
				shape = RoundedCornerShape(8.dp),
				content = { Text("Cancelar") }
			)
			Button(
				onClick = { onPositiveClick(nameTxt) },
				Modifier.size(wdtBt, hgtBt),
				enabled = isNameNotBlank,
				colors = ButtonDefaults.buttonColors(backgroundColor = Holo_Bluedark),
				shape = RoundedCornerShape(8.dp),
				content = { Text("Confirmar") }
			)
		}
	}
}

@Composable
internal fun DialogReserveVariousContent(onDismiss: () -> Unit, onPositiveClick: (tables: String, name: String) -> Unit) {
	DialogSkeleton(color = Color.DarkGray, head = -1) { w, h ->
		Text("Reserva múltipla", color = Color.Gray, fontSize = 23.sp)
		Spacer(Modifier.height(10.dp))

		var nameTxt by remember { mutableStateOf("") }
		var isNameNotBlank by remember { mutableStateOf(false) }
		MyTextField("nome do comprador", KeyboardType.Text, Color.LightGray) { str, b ->
			nameTxt = str
			isNameNotBlank = b
		}
		Spacer(Modifier.height(10.dp))

		Text("Mesas:", color = Color.Gray, fontSize = 13.sp)

		var tablesTxt by remember { mutableStateOf("") }
		var isTableNotBlank by remember { mutableStateOf(false) }
		MyTextField("1, 2, 3", KeyboardType.Number, Color.LightGray) { str, b ->
			tablesTxt = str
			isTableNotBlank = b
		}
		Spacer(Modifier.height(15.dp))

		Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
			Button(
				onClick = onDismiss,
				Modifier.size(w, h),
				colors = ButtonDefaults.buttonColors(backgroundColor = Holo_OrangeAutumm),
				shape = RoundedCornerShape(8.dp),
				content = { Text("Cancelar") }
			)
			Button(
				onClick = { onPositiveClick(tablesTxt, nameTxt) },
				Modifier.size(w, h),
				enabled = (isNameNotBlank && isTableNotBlank),
				colors = ButtonDefaults.buttonColors(backgroundColor = Holo_Bluedark),
				shape = RoundedCornerShape(8.dp),
				content = { Text("Confirmar") }
			)
		}
	}
}

@Composable
internal fun DialogDetailsContent(result: Pair<TableObject, BuyerObject>,
	onBookParkLot: (BuyerObject, Boolean) -> Unit, onBookExtra: (TableObject, Boolean) -> Unit, onUnbookClick: (TableObject, BuyerObject) -> Unit,
	onChangeOwner: () -> Unit, onDismiss: () -> Unit
) {
	val (_, _, tablePaid, _, extra, tableStr) = result.first
	val (owner, tables, parking) = result.second
	DialogSkeleton(if (tablePaid) SeatPayYES else SeatPayNO, head = tableStr.toInt()) { w, h ->
		Text("Reservado para", color = Color.Gray, fontSize = 13.sp)
		TextButton(onClick = onChangeOwner) {
			Text(
				owner, Modifier.padding(top = 2.dp),
				color = Color.Black,
				fontWeight = FontWeight.Bold,
				fontSize = 25.sp, letterSpacing = 2.sp
			)
		}
		Spacer(Modifier.height(12.dp))

		Text("Mesas reservadas", color = Color.Gray, fontSize = 13.sp)
		Text(
			tables, Modifier.padding(top = 2.dp),
			color = Color.Black,
			fontSize = 23.sp, letterSpacing = 2.sp
		)
		Spacer(Modifier.height(15.dp))

		var checkPark by remember { mutableStateOf(parking != null) }
		var checkExtra by remember { mutableStateOf(extra != null) }
		Row(Modifier.fillMaxWidth(), Arrangement.Center, Alignment.CenterVertically) {
			Checkbox(checked = checkPark, { checkPark = it; onBookParkLot(result.second, it) })
			Text("Vaga", Modifier.padding(start = 2.dp), fontSize = 15.sp)
			Spacer(Modifier.width(15.dp))
			Checkbox(checked = checkExtra, { checkExtra = it; onBookExtra(result.first, it) })
			Text("Mesa extra", Modifier.padding(start = 2.dp), fontSize = 15.sp)
		}
		Spacer(Modifier.height(25.dp))

		Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceEvenly) {
			Button(
				onClick = { onDismiss() },
				modifier = Modifier.size(w, h),
				colors = ButtonDefaults.buttonColors(backgroundColor = Holo_Bluedark),
				shape = RoundedCornerShape(8.dp),
				content = { Text("FECHAR", color = Color.White, fontWeight = FontWeight.Bold) }
			)
			Button(
				onClick = { onUnbookClick(result.first, result.second) },
				modifier = Modifier.size(w, h),
				colors = ButtonDefaults.buttonColors(backgroundColor = Holo_OrangeAutumm),
				shape = RoundedCornerShape(8.dp),
				content = { Text("REMOVER", color = Color.White, fontWeight = FontWeight.Bold) }
			)
		}
	}
}

@Composable
internal fun DialogCostContent(cost: CostObject, onDismiss: () -> Unit, onPositiveClick: (CostObject, Double) -> Unit) {
	val captionSize = 13.sp
	val isNewItem = cost.desc.isBlank()
	DialogSkeleton(color = Holo_OrangeAutumm, head = -2) { wdt, hgt ->
		val initialValue = cost.value
		Text("Data", Modifier.fillMaxWidth(), Color.Gray, captionSize, textAlign = TextAlign.Start)
		var txtDate by remember { mutableStateOf(cost.date) }
		var isDateNotBlank by remember { mutableStateOf(txtDate.isNotBlank()) }
		TextFieldDate(cost.cleanDate) { txt, b ->
			txtDate = txt
			isDateNotBlank = b
		}
		Spacer(Modifier.height(10.dp))

		Text("Descrição", Modifier.fillMaxWidth(), Color.Gray, captionSize, textAlign = TextAlign.Start)
		var txtDesc by remember { mutableStateOf(cost.desc) }
		var isDescNotBlank by remember { mutableStateOf(false) }
		MyTextField(placeholder = "", type = KeyboardType.Text, Color.LightGray, textInit = txtDesc) { txt, b ->
			txtDesc = txt
			isDescNotBlank = b
		}
		Spacer(Modifier.height(10.dp))

		Text("Valor", Modifier.fillMaxWidth(), Color.Gray, captionSize, textAlign = TextAlign.Start)
		val currency = CurrencyTextBuilder()
		var txtValue by remember { mutableStateOf( (currency.cleanValue(cost.value)) ) }
		var isValueNotBlank by remember { mutableStateOf(false) }

		val walletList = listOf(painterResource(R.drawable.icon_bb), painterResource(R.drawable.icon_wallet))
		val (selectedItem, setSelection) = remember { mutableStateOf(WALLET.opIdx(cost.wallet)) }
		Row(Modifier.wrapContentWidth()) {
			currency.TextCurrencyEditable(Modifier.weight(.75f), TextStyle(Color.Black), txtValue) { txt, b ->
				txtValue = txt
				isValueNotBlank = b
			}
			Spinner(Modifier.weight(.25f),
				current = selectedItem,
				onSelected = setSelection,
				elements = listOf(walletList[0], walletList[1]),
				elementsTitle = listOf("BB", "DIN")
			)
		}
		Spacer(Modifier.height(20.dp))

		Row(Modifier.fillMaxWidth(), Arrangement.SpaceEvenly) {
			Button(
				onClick = onDismiss,
				Modifier.size(wdt, hgt),
				colors =  ButtonDefaults.buttonColors(backgroundColor = Holo_OrangeAutumm),
				shape = RoundedCornerShape(8.dp),
				content = { Text("CANCELAR", color = Color.White) }
			)
			Button(
				onClick = { onPositiveClick(CostObject(txtDate, txtDesc.trim(), currency.currencyDouble(txtValue), WALLET.values()[selectedItem].str, cost.hash), initialValue) },
				Modifier.size(wdt, hgt),
				enabled = isNewItem && isDateNotBlank && isDescNotBlank && isValueNotBlank,
				colors =  ButtonDefaults.buttonColors(backgroundColor = Holo_Bluedark),
				shape = RoundedCornerShape(8.dp),
				content = { Text("SALVAR", color = Color.White) }
			)
		}
	}
}

@Composable
internal fun DialogChooseWalletContent(onDismiss: OnDismiss, onPlayClick: (WALLET) -> Unit) {
	DialogSkeleton(color = Holo_YellowShine, head = -1) { wBt, hBt ->
		var checkedBank by remember { mutableStateOf(false) }
//		var checkedCEF by remember { mutableStateOf(false) }
		var checkedMNY by remember { mutableStateOf(false) }
		var wallet by remember { mutableStateOf(WALLET.BANK) }

		Text("Selecione a carteira a ser usada:")
		Spacer(Modifier.height(10.dp))

		Row(Modifier.fillMaxWidth().padding(5.dp), Arrangement.SpaceEvenly) {
			val width = 70.dp
			Row(Modifier.width(width), verticalAlignment = Alignment.CenterVertically) {
				Checkbox(checked = checkedBank,
					onCheckedChange = { checkedBank = it; checkedMNY = false; wallet = WALLET.BANK })
				Image(painterResource(R.drawable.icon_bb), contentDescription = "", Modifier.size(40.dp))
			}
			/*Row(Modifier.width(width), verticalAlignment = Alignment.CenterVertically) {
				Checkbox(checked = checkedCEF,
					onCheckedChange = { checkedCEF = it; checkedBank = false; checkedMNY = false; wallet = WALLET.CEF })
				Image(painterResource(R.drawable.icon_cef), contentDescription = "", Modifier.size(40.dp))
			}*/
			Row(Modifier.width(width), verticalAlignment = Alignment.CenterVertically) {
				Checkbox(checked = checkedMNY,
					onCheckedChange = { checkedMNY = it; checkedBank = false; wallet = WALLET.MONEY })
				Image(painterResource(R.drawable.icon_wallet), contentDescription = "", Modifier.size(40.dp))
			}
		}

		Row(Modifier.fillMaxWidth(), Arrangement.SpaceEvenly) {
			Button(
				onClick = onDismiss,
				Modifier.size(wBt, hBt),
				colors =  ButtonDefaults.buttonColors(backgroundColor = Holo_OrangeAutumm),
				shape = RoundedCornerShape(8.dp),
				content = { Text(stringResource(R.string.alert_bt_nok), color = Color.White) }
			)
			Button(
				onClick = { onPlayClick(wallet) },
				Modifier.size(wBt, hBt),
				enabled = (checkedBank || checkedMNY),
				colors =  ButtonDefaults.buttonColors(backgroundColor = Holo_Bluedark),
				shape = RoundedCornerShape(8.dp),
				content = { Text(stringResource(R.string.alert_bt_ok), color = Color.White) }
			)
		}
	}
}

@Composable
internal fun DialogRefundWalletContent(@DrawableRes resImg: Int, onDismiss: OnDismiss, onConfirmClick: () -> Unit) {
	DialogSkeleton(color = Color.LightGray, head = -1) { wBt, hBt ->
		Row(Modifier.fillMaxWidth(), Arrangement.Center) {
			Row(Modifier.wrapContentWidth(), verticalAlignment = Alignment.CenterVertically) {
				Text("Desfazer o pagamento feito em: ")
				Image(painterResource(resImg), contentDescription = "", Modifier.size(40.dp))
			}
		}
		Spacer(Modifier.height(5.dp))

		Row(Modifier.fillMaxWidth(), Arrangement.SpaceEvenly) {
			Button(
				onClick = onDismiss,
				modifier = Modifier.size(wBt, hBt),
				colors =  ButtonDefaults.buttonColors(backgroundColor = Holo_OrangeAutumm),
				shape = RoundedCornerShape(8.dp),
				content = { Text("CANCELAR", color = Color.White) }
			)
			Button(
				onClick = onConfirmClick ,
				modifier = Modifier.size(wBt, hBt),
				colors =  ButtonDefaults.buttonColors(backgroundColor = Holo_Bluedark),
				shape = RoundedCornerShape(8.dp),
				content = { Text("DESFAZER", color = Color.White) }
			)
		}

	}
}
/** Created on 30/06/2022. @since Beta 1.5.6.12 */
@Composable
internal fun DialogChangeOwnershipContent(
	pair: Pair<TableObject, BuyerObject>,
	onChangeOwner: (TableObject, BuyerObject, String) -> Unit,
	onDismiss: () -> Unit
) {
	val (owner, _, _, _, _, tableStr) = pair.first
	DialogSkeleton(color = Color.LightGray, head = tableStr.toInt()) { w, h ->
		Text("Reservado para", color = Color.Gray, fontSize = 13.sp)
		Text(
			owner, Modifier.padding(top = 2.dp),
			color = Color.Black,
			fontWeight = FontWeight.Bold,
			fontSize = 25.sp, letterSpacing = 2.sp
		)
		Spacer(Modifier.height(12.dp))

		var nameTxt by remember { mutableStateOf("") }
		var isNameNotBlank by remember { mutableStateOf(false) }
		MyTextField("novo comprador", KeyboardType.Text, Color.LightGray) { str, b ->
			nameTxt = str
			isNameNotBlank = b
		}
		Spacer(Modifier.height(15.dp))

		Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceEvenly) {
			Button(
				onClick = { onDismiss() },
				modifier = Modifier.size(w, h),
				colors = ButtonDefaults.buttonColors(backgroundColor = Holo_Bluedark),
				shape = RoundedCornerShape(8.dp),
				content = { Text("FECHAR", color = Color.White, fontWeight = FontWeight.Bold) }
			)
			Button(
				onClick = { onChangeOwner(pair.first, pair.second, nameTxt) },
				modifier = Modifier.size(w, h),
				enabled = isNameNotBlank,
				colors = ButtonDefaults.buttonColors(backgroundColor = Holo_OrangeAutumm),
				shape = RoundedCornerShape(8.dp),
				content = { Text("TROCAR", color = Color.White, fontWeight = FontWeight.Bold) }
			)
		}
	}
}

@Composable
private fun DialogSkeleton(color: Color, head: Int, body: @Composable ((width: Dp, height: Dp) -> Unit)) {
	val wdt = 300 // Tongue = 260
	Box(Modifier.width(wdt.dp)) {
		Spacer(Modifier.size((wdt-40).dp, 50.dp).align(Companion.BottomCenter)
				.background(color, RoundedCornerShape(20.dp)))// ligueta que fica na parte de baixo

		Column(Modifier.width(wdt.dp)) { // espaço visível do Box
			Spacer(Modifier.height(30.dp)) // espaço vazio superior
			Column(
				Modifier.width(wdt.dp).background(IceWhite, RoundedCornerShape(10.dp))
					.padding(start = 30.dp, end = 30.dp, top = 30.dp, bottom = 20.dp),
				horizontalAlignment = Alignment.CenterHorizontally, content = { body(110.dp, 40.dp) }// corpo efetivamente
			)
			Spacer(Modifier.height(13.dp))
		}

		if (head != -1) {
			Surface(
				Modifier.size(140.dp, 50.dp).align(Alignment.TopCenter),
				color = color,
				shape = RoundedCornerShape(15.dp)
			) {
				Text(
					if (head == -2) String.format("GASTOS") else String.format("%03d", head),
					Modifier.padding(vertical = 5.dp).align(Companion.Center),
					Color.White,
					fontWeight = FontWeight.Bold,
					textAlign = TextAlign.Center,
					style = Typography.h4
				)
			}
		}
	}
}

@Preview(name = "Dialog Costs")
@Composable
private fun PreviewCosts() {
	DialogCostContent(
		CostObject("12022020", "fitas crepes",25.65),
		onDismiss = { /*TODO*/ }, onPositiveClick = {_,_ ->})
}

@Preview(name = "Dialog Reserve One Seat")
@Composable
private fun PreviewReserveOne() =
	DialogReserveOneContent(0, onDismiss = { /*TODO*/ }, onPositiveClick = {})

@Preview(name = "Dialog Reserve Various")
@Composable
private fun PreviewReserveVarious() =
	DialogReserveVariousContent(onDismiss = { /*TODO*/ }, onPositiveClick = {_,_ ->})

@Preview(name = "Dialog Details")
@Composable
private fun PreviewDialog() {
	DialogDetailsContent(
		TableObject("Fernanda") to BuyerObject(tables = "3,4", parking = ParkingModel("10/04/2022")),
		onBookParkLot = { s, b -> },
		onBookExtra = {_, _ ->},
		onChangeOwner = {},
		onDismiss = {},
		onUnbookClick = {_,_ -> }
	)
}

@Preview(name = "Dialog Payment")
@Composable
private fun PreviewPayDialog() {
	DialogChooseWalletContent({}, {})
}

/*class SampleCostProvider: PreviewParameterProvider<((String, String, String) -> Unit)> {
	override val values = sequenceOf({ _:String, _:String, _:String -> })
	override val count: Int = values.count()
}*/