@file:Suppress("UNCHECKED_CAST")

package br.com.mario.reservalugar.ui.resume

import androidx.compose.foundation.layout.Column

import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.Text

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue

import androidx.compose.ui.res.stringResource

import androidx.lifecycle.viewmodel.compose.viewModel

import br.com.mario.reservalugar.R
import br.com.mario.reservalugar.ui.theme.MyColors.Holo_YellowShine

import kotlinx.coroutines.flow.StateFlow

/**
 * Exibe, através de abas, o resumo de reservas, saldos nas carteiras e extratos
 *
 * Created on 20/12/2021
 * Lst mod on 07/05/2022
 */

@Composable
fun ResumeScreen(statementVM: StatementViewModel, resumeTabVM: ResumeTabViewModel, isExpanded: Boolean) {
	val tabVM: ResumeViewModel = viewModel(factory = ResumeViewModel.provideFactory())

	val uiBalState by (resumeTabVM.uiBalanceState as StateFlow<BalanceUiState>).collectAsState()
	val uiCatState by resumeTabVM.uiCategoryState.collectAsState()

	val tabIndex by tabVM.tabIndex.collectAsState() // somente assim salva o estado ao girar tela
	val tabTitles = listOf(
		stringResource(R.string.resume_tab_status),
		stringResource(R.string.resume_tab_balance),
		stringResource(R.string.resume_tab_statements)
	)

	Column {
		TabRow(selectedTabIndex = tabIndex, backgroundColor = Holo_YellowShine) {
			tabTitles.forEachIndexed { idx, tt ->
				Tab(selected = (tabIndex == idx), text = { Text(tt) }, onClick = { tabVM.setIndex(idx) })
			}
		}

		when (tabIndex) {
			0 -> ResumeTabStatusScreen(uiCatState, isExpanded, resumeTabVM::refreshInfos)
			1 -> ResumeTabBalanceScreen(uiBalState, isExpanded)
			2 -> ResumeTabStatementRoute(statementVM, isExpanded, statementVM::refreshReportList)
		}
	}
}

/*
@Preview(name = "Tela de Resumo")
@Composable
fun PreviewResume() {
	ResumeScreen()
}*/