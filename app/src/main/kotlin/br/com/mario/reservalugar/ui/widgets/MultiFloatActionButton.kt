package br.com.mario.reservalugar.ui.widgets

import androidx.compose.animation.core.Transition
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.updateTransition

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize

import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.unit.dp

import br.com.mario.reservalugar.ui.widgets.MultiFabBuilder.FabState.COLLAPSED
import br.com.mario.reservalugar.ui.widgets.MultiFabBuilder.FabState.EXPANDED

/**
 * Created on 13/12/2021
 * Lst mod on 30/06/2022
 */

object MultiFabBuilder {
	enum class FabState {
		COLLAPSED, EXPANDED
	}
	enum class FabOptions {
		MULTIBOOK, SCREENSHOT
	}

	class MultiFabItem(val identifier: MultiFabBuilder.FabOptions, val icon: ImageVector, val label: String)

	@Composable
	fun MultiFAB(
		fabIcon: ImageVector = Icons.Filled.Add,
		items: List<MultiFabBuilder.MultiFabItem>,
		modifier: Modifier,
		toState: MultiFabBuilder.FabState,
		stateChanged: (fabState: MultiFabBuilder.FabState) -> Unit,
		onFabItemClicked: (item: MultiFabBuilder.MultiFabItem) -> Unit
	) {
		val transition: Transition<MultiFabBuilder.FabState> = updateTransition(targetState = toState, label = "appear")
		val rotation: Float by transition.animateFloat(label = "rodando") { state ->
			if (state == EXPANDED) 135f else 0f
		}

		Column(modifier.wrapContentSize().padding(end = 5.dp, bottom = 5.dp), horizontalAlignment = Alignment.CenterHorizontally) {
			if (transition.currentState == EXPANDED) {
				items.forEach {
					FloatingActionButton(onClick = { onFabItemClicked(it) }, Modifier.size(35.dp)) {
						Icon(it.icon, "")
					}
					Spacer(Modifier.height(20.dp))
				}
			}
			FloatingActionButton(
				onClick = {
					stateChanged(if (transition.currentState == EXPANDED) COLLAPSED else EXPANDED)
				}, content = { Icon(fabIcon, null, Modifier.rotate(rotation)) })
		}
	}
}

/*
@Composable
fun MiniFAB(item: MultiFabItem, onFabItemClicked: (item: MultiFabItem) -> Unit) {
	val fabColor = MaterialTheme.colors.secondary
	Canvas(modifier = Modifier.size(32.dp).clickable(MutableInteractionSource(),
		onClick = { onFabItemClicked(item) },
		indication =  rememberRipple(false, 20.dp, MaterialTheme.colors.onSecondary)
	)) {
		drawCircle(fabColor, 56f)
		drawImage(item.icon,
		topLeft = Offset(
			(this.center.x) - (item.icon.width / 2),
			(this.center.y) - (item.icon.width / 2)
		))
	}
}*/