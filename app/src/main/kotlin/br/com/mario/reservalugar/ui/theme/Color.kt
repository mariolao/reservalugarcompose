package br.com.mario.reservalugar.ui.theme

import androidx.compose.ui.graphics.Color
import br.com.mario.reservalugar.color

object MyColors {
	val Purple200 = Color(0xFFBB86FC)
	val Purple500 = Color(0xFF6200EE)
	val Purple700 = Color(0xFF3700B3)
	val Teal200 = Color(0xFF03DAC5)

	val BackgroundMenu = Color(0xFFDCDCDC)
	val TextNumber = Color(0xFF0083CA)

	val Holo_Bluedark = Color(0xff0099cc)
	val Holo_RedDragon = Color(0xffF44336)
	val Holo_OrangeAutumm = Color(0xFFEF6C00)
	val Holo_YellowShine = Color(0xffECB205)
	val Holo_GreenGrass = Color(0xFF558B2F)

	val Back_CEF = Color(0xFF0039BA)

	val ButtonStatusOFF = Color(0xFFc6c6c6)
	val ButtonStatusON = Color(0xFFF9A825)
	val ButtonLoginIdle = "#FFA000".color

	/* DIALOG colors */
	val SeatPayYES = Color(0xff4CAF50)
	val SeatPayNO = Color(0xFFD86464)
	val IceWhite = Color(0xFFFFFCFC)
}