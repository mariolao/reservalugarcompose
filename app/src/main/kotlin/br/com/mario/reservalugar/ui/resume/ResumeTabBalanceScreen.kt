/**
 * Tela que mostra o resumo dos valores por Carteira (Banco ou Dinheiro)
 *
 * Created on 24/01/2022
 * Lst mod on 09/05/2022
 */
package br.com.mario.reservalugar.ui.resume

import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape

import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

import br.com.mario.reservalugar.R
import br.com.mario.reservalugar.model.WalletObject
import br.com.mario.reservalugar.ui.theme.MyColors
import br.com.mario.reservalugar.ui.theme.MyColors.Holo_GreenGrass
import br.com.mario.reservalugar.ui.theme.MyColors.Holo_RedDragon
import br.com.mario.reservalugar.ui.theme.Typography
import br.com.mario.reservalugar.ui.widgets.CurrencyTextBuilder

@Composable
fun ResumeTabBalanceScreen(uiState: BalanceUiState, isExpandedScreen: Boolean) {
	when(uiState) {
		is BalanceUiState.NotLoaded -> {}
		is BalanceUiState.Loaded -> {
			if (isExpandedScreen) 
				BalanceLargeSector(uiState = uiState)
			else
				BalanceSmallSector(uiState = uiState)
		}
	}
}

@Composable
private fun BalanceSmallSector(uiState: BalanceUiState.Loaded, modifier: Modifier = Modifier) {
	val currency = CurrencyTextBuilder()
	val balance = uiState.bankInOut.receives + uiState.moneyInOut.receives -
			uiState.bankInOut.exits - uiState.moneyInOut.exits - uiState.bankInOut.chargeback - uiState.moneyInOut.chargeback
	Column(modifier.fillMaxSize().background(MyColors.BackgroundMenu)) {
		Spacer(Modifier.height(30.dp))

		Column(Modifier.fillMaxWidth().wrapContentHeight(), Arrangement.Center, Alignment.CenterHorizontally) {
			Text(stringResource(R.string.resume_balance_title), fontWeight = FontWeight.Bold, style = Typography.h3)
//			Text(String.format("R$ %.2f", balance), color = MyColors.TextNumber, fontSize = 40.sp, fontWeight = FontWeight.Bold)
			currency.TextCurrencyLabel(
				fColor = MyColors.TextNumber, fSize = 40.sp, fontWeight = FontWeight.Bold,
				text = TextFieldValue(currency.cleanValue(balance))
			)
		}

		Column(Modifier.fillMaxSize(), Arrangement.SpaceEvenly, Alignment.CenterHorizontally) {
			BalanceBox(Color.White, R.drawable.icon_wallet, uiState.moneyInOut)
			BalanceBox(Color.Yellow, R.drawable.icon_bb, uiState.bankInOut)
		}
	}
}

/** Gerencia layout quando largura maior que altura */
@Composable
private fun BalanceLargeSector(uiState: BalanceUiState.Loaded, modifier: Modifier = Modifier) {
	val currency = CurrencyTextBuilder()
	val balance = uiState.bankInOut.receives + uiState.moneyInOut.receives -
			uiState.bankInOut.exits - uiState.moneyInOut.exits - uiState.bankInOut.chargeback - uiState.moneyInOut.chargeback

	Column(modifier.fillMaxSize().background(MyColors.BackgroundMenu)) {
		Row(Modifier.fillMaxWidth().padding(vertical = 8.dp), Arrangement.Center, Alignment.CenterVertically) {
			Text(stringResource(R.string.resume_balance_title), fontWeight = FontWeight.Bold, style = Typography.h3)
			Spacer(Modifier.width(10.dp))
			currency.TextCurrencyLabel(
				fColor = MyColors.TextNumber, fSize = 30.sp, fontWeight = FontWeight.Bold,
				text = TextFieldValue(currency.cleanValue(balance))
			)
		}

		Row(Modifier.fillMaxSize(), Arrangement.SpaceAround, Alignment.CenterVertically) {
			BalanceBox(Color.White, R.drawable.icon_wallet, uiState.moneyInOut)
			BalanceBox(Color.Yellow, R.drawable.icon_bb, uiState.bankInOut)
		}
	}
}

/** Created on 24/01/2022 */
@Composable
private fun BalanceBox(color: Color, @DrawableRes idImg: Int, wallet: WalletObject) {
	Card(
		Modifier.width(300.dp).wrapContentHeight().padding(horizontal = 10.dp),
		backgroundColor = color,
		shape = RoundedCornerShape(10.dp),
		elevation = 20.dp
	) {
		Row(Modifier.wrapContentSize().padding(10.dp)) {
			Image(
				painterResource(id = idImg),
				"",
				Modifier.height(100.dp),
				Alignment.Center
			)
			Column(Modifier.fillMaxWidth(), Arrangement.SpaceEvenly, Alignment.CenterHorizontally) {
				Spacer(Modifier.height(5.dp))
				Text(String.format("R$ %.2f", (wallet.receives - wallet.exits)), color = color.contrast(), fontSize = 30.sp, fontWeight = FontWeight.Bold)
				Spacer(Modifier.height(10.dp))

				Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceEvenly) {
					Text("receita")
					Text("despesa")
				}
				Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceEvenly) {
					Text(String.format("R$ %.2f", wallet.receives), color = color.entrance())
					Text(String.format("R$ %.2f", wallet.exits + wallet.chargeback), color = color.dispense())
				}
			}
		}

	}
}

/* Return black for bright colors, white for dark colors */
private fun Color.contrast() : Color = if (luma(this) > 0.5) Color.Black else Color.White
/* Return black for bright colors, white for dark colors */
private fun Color.entrance() : Color = if (luma(this) > 0.5) Color.Green else Holo_GreenGrass
/* Return black for bright colors, white for dark colors */
private fun Color.dispense() : Color = if (luma(this) > 0.5) Color.Red else Holo_RedDragon

/** Calculate the perceptive luminance (aka luma) - human eye favors green color... */
private val luma : (Color) -> Double = { color ->
	val red = android.graphics.Color.red(color.toArgb())
	val green = android.graphics.Color.green(color.toArgb())
	val blue = android.graphics.Color.blue(color.toArgb())
	(0.299 * red + 0.587 * green + 0.114 * blue) / 255.0
}

@Preview("Tela saldo em modo RETRATO")
@Composable
private fun PreviewSmallScreen() {
	BalanceSmallSector(BalanceUiState.Loaded(
		isLoading = false,
		bankInOut = WalletObject(150.0, 180.0),
		moneyInOut = WalletObject(100.0, 80.0)
	))
}

@Preview("Tela saldo em modo DEITADO", device = Devices.PIXEL_C)
@Composable
private fun PreviewLargeScreen() {
	BalanceLargeSector(BalanceUiState.Loaded(
		isLoading = false,
		bankInOut = WalletObject(130.0, 180.0),
		moneyInOut = WalletObject(100.5, 80.0)
	))
}