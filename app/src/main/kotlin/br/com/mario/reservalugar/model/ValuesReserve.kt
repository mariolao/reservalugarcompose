package br.com.mario.reservalugar.model

import br.com.mario.reservalugar.ValuesDB.PATH_EXTRA
import br.com.mario.reservalugar.ValuesDB.PATH_PARK
import br.com.mario.reservalugar.ValuesDB.PATH_TABLE
import br.com.mario.reservalugar.ValuesDB.WAY_IN
import br.com.mario.reservalugar.ValuesDB.WAY_OUT
import br.com.mario.reservalugar.ValuesDB.WAY_REV

import br.com.mario.reservalugar.doubling
import br.com.mario.reservalugar.utils.WALLET

/**
 * Contém as representações dos objetos do nó "valores" do BD.
 *
 * @author Mário Henrique
 * @since 25/01/2022
 * Lst mod on 09/05/2022
 */

/** Representa entrada em nó "categoria". Created on 04/03/2022 */
class CategoryObject(val bk: Double = .0, val din: Double = .0) {
	constructor(map: HashMap<*, *>) : this(map.doubling(WALLET.BANK.str), map.doubling(WALLET.MONEY.str))
	val total: Double
		get() = bk + din

	fun getWalletValue(wallet: WALLET) = when(wallet) {
		WALLET.BANK -> bk
		WALLET.MONEY -> din
	}
}

/** Encapsula os valores de ganhos das categorias armazenadas no BD.   */
class CategoryBalance {
	private var tableValues = CategoryObject()
	private var extraValues = CategoryObject()
	private var parkValues = CategoryObject()

	fun setCategories(map: HashMap<*, HashMap<*, *>>) {
		tableValues = CategoryObject(map[PATH_TABLE]!!)
		extraValues = CategoryObject(map[PATH_EXTRA]!!)
		parkValues = CategoryObject(map[PATH_PARK]!!)
	}

	fun setWalletValues(tag: String, values: HashMap<String, Double>) = when(tag) {
		PATH_TABLE -> tableValues = CategoryObject(values)
		PATH_EXTRA -> extraValues = CategoryObject(values)
		else -> parkValues = CategoryObject(values)
	}

	fun getCategory(tag: String) = when(tag) {
		PATH_TABLE -> tableValues
		PATH_EXTRA -> extraValues
		else -> parkValues
	}
	fun getWalletValue(tag: String, wallet: WALLET) = with(getCategory(tag)) {
		when (wallet) {
			WALLET.BANK -> this.bk
			WALLET.MONEY -> this.din
		}
	}

	/*private val total = hashMapOf<String, Double>()
	/ * guarda valor de total por categoria * /
	private val totalCategoryMap = hashMapOf(PATH_TOTAL to .0, PATH_EXTRA to .0, PATH_PARK to .0, PATH_TABLE to .0)
	/ * valor de saldo em cada categoria por carteira * /
	private val totalWalletMap = hashMapOf(bbKey to .0, cefKey  to .0, dinKey to .0)
	/ * referente ao nó atual-saldo. Primeira camada é categoria, segunda é carteira * /
	private val totalCostsMap = hashMapOf(bbKey to .0, cefKey to .0, dinKey to .0)
	private val balanceZeroMap = hashMapOf(bbKey to .0, cefKey to .0, dinKey to .0)
	private val balanceMap = hashMapOf(
		PATH_TABLE to balanceZeroMap, PATH_EXTRA  to balanceZeroMap,
		PATH_PARK to balanceZeroMap, PATH_OUTS to balanceZeroMap
	)*/

	/*fun setEarns(map: HashMap<*, HashMap<*, *>>) {
//		listOf(PATH_TABLE, PATH_EXTRA, PATH_PARK, PATH_OUTS).forEach {
//			setCategoryMap(it, map[it]!!)
//		}
		setCategoryMap(PATH_OUTS, map[PATH_OUTS]!!)
		setCategoryMap(PATH_TABLE, map[PATH_TABLE]!!)
		setCategoryMap(PATH_EXTRA, map[PATH_EXTRA]!!)
		setCategoryMap(PATH_PARK, map[PATH_PARK]!!)
	}
	fun setCategoryMap(categoryKey: String, update: HashMap<*, *>) {
//		if (update[bbKey] == null) return
		balanceMap[categoryKey]?.let {
			it[bbKey] = update.doubling(bbKey)
			it[cefKey] = update.doubling(cefKey)
			it[dinKey] = update.doubling(dinKey)
		}
	}
	fun setSingleEarn(categoryTag: String, walletTag: String, value: Double) =
		balanceMap[categoryTag]!!.set(walletTag, value)
	fun updateTotals() {
		listOf(bbKey, cefKey, dinKey).forEach { totalWalletMap[it] = sumWallet(it) }
		listOf(PATH_TABLE, PATH_EXTRA, PATH_PARK).forEach {
			totalCategoryMap[it] = sumCategory(balanceMap[it]!!) }
		// esse total corresponde a todas as entradas (só entra dinheiro por meio das reservas)
		totalCategoryMap[PATH_TOTAL] = totalCategoryMap[PATH_TABLE]!! + totalCategoryMap[PATH_EXTRA]!! + totalCategoryMap[PATH_PARK]!!
	}

	fun getEarn(categoryKey: String, walletTag: String) = balanceMap[categoryKey]!![walletTag]!!

	fun getGainsByCategory(categoryKey: String) = totalCategoryMap[categoryKey]!!
	fun getGainsByWallet(walletTag: String): Double = totalWalletMap[walletTag]!!
	fun getExitByWallet(walletTag: String): Double = balanceMap[PATH_OUTS]!![walletTag]!!
	fun getExits() = balanceMap[PATH_OUTS]!!.map { it.value }

	private fun sumCategory(hashMap: HashMap<String, Double>) =
		(hashMap[bbKey]!! +
				hashMap[cefKey]!! + hashMap[dinKey]!!)
	private fun sumWallet(wallet: String) =
		(balanceMap[PATH_TABLE]!![wallet]!! + balanceMap[PATH_EXTRA]!![wallet]!! + balanceMap[PATH_PARK]!![wallet]!!)*/
}

/** Representa objeto do nó "caixa". Created on 06/04/2022 */
class WalletObject(val receives: Double = .0, val exits: Double = .0, val chargeback: Double = .0) {
	constructor(map: HashMap<*, *>) : this(map.doubling(WAY_IN), map.doubling(WAY_OUT), map.doubling(WAY_REV))
}

/** Created on 06/04/2022 */
class WalletBalance {
	private var bankWallet = WalletObject()
	var cefWallet = WalletObject()
	var moneyWallet = WalletObject()

	fun getWallet(tag: WALLET) = when(tag) {
		WALLET.BANK -> bankWallet
		WALLET.MONEY -> moneyWallet
	}

	fun getWalletExit(tag: String) = getWallet(WALLET.sto(tag)).exits
	fun getWalletExit(tag: WALLET) = getWallet(tag).exits
	fun setInOutValues(tag: WALLET, hashMap: HashMap<String, Double>) = when(tag) {
		WALLET.BANK -> bankWallet = WalletObject(hashMap)
		WALLET.MONEY -> moneyWallet = WalletObject(hashMap)
	}

}