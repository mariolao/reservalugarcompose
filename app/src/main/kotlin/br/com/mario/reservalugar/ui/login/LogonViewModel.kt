package br.com.mario.reservalugar.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import br.com.mario.reservalugar.model.UserObject
import br.com.mario.reservalugar.userManager

import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseUser

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

enum class AuthenticationState {
	UNAUTHENTICATED,        // Initial state, the user needs to authenticate
	AUTHENTICATED  ,        // The user has authenticated successfully, but not yet changed their password
	SECOND_ACCESS,          // User already changed the password and it's their second access
	INVALID_AUTHENTICATION  // Authentication failed
}

/**
 * Created on 18/04/2022
 * Lst mod on 19/05/2022
 */
class LogonViewModel : ViewModel() {
	private val viewModelState = MutableStateFlow(LogonViewModelState())
	val uiState = viewModelState.map { it.toUIState() }
		.stateIn(viewModelScope, SharingStarted.Eagerly, viewModelState.value.toUIState())

	init {
		//todo pré-carregamento de localCUrrentUser é necessário
		// para que a tela de login não "pisque" se usuário já logado
		// salvamento dos dados localmente pode ser uma solução (usuário atual, pelo menos)

		viewModelScope.launch {
			userManager.userMapFlow.collect { map ->
				viewModelState.update { it.copy(usersMap = map) }
			}
		}
		viewModelScope.launch {
			userManager.currentUserFlow.collect { user ->
				viewModelState.update { it.copy(userFirebase = user) }
			}
		}
		viewModelScope.launch {
			userManager.localCurrentUserFlow.collect { user ->
				val state = when {
					user.hash != null -> {
						if (user.firstAccess) AuthenticationState.AUTHENTICATED
						else AuthenticationState.SECOND_ACCESS
					}
					else -> AuthenticationState.UNAUTHENTICATED
				}
				viewModelState.update { it.copy(authenticationState = state) }
			}
		}
	}

	fun refreshUserMap() {
		viewModelScope.launch {
		}
	}

	/** Se houver usuário logado, desfaz a autenticação */
	fun logoff() {}

	/** Faz o processo de autenticação do usuário, retornando por meio de callback o resultado da operação */
	fun authenticate(email: String, password: String) {
		//todo testar se usuário existe na lista de usuários ?
		userManager.authenticate(email, password) { task ->
			viewModelState.update {
				it.copy(authenticationState = when {
					task -> {
						if (userManager.isCurrentFirstAccess)
							AuthenticationState.AUTHENTICATED
						else
							AuthenticationState.SECOND_ACCESS
					}
					else -> AuthenticationState.INVALID_AUTHENTICATION
				})
			}
		}
	}

	/** Created on 16/05/2022 */
	fun changePassword(email: String, password: String, newPassword: String) {
		val credential = EmailAuthProvider.getCredential(email, password)
		val currentUser = userManager.currentUser

		currentUser?.reauthenticate(credential)?.addOnCompleteListener {
			if (it.isSuccessful) {
				currentUser.updatePassword(newPassword).addOnCompleteListener { taskChange ->
					if (taskChange.isSuccessful) {
						//todo dizer que senha foi alterada
						viewModelState.update { state -> state.copy(authenticationState = AuthenticationState.SECOND_ACCESS) }
						userManager.setSecondAccess()
					} else {
						//todo incluir mensagem de erro
					}
				}
			} else {
				//todo crendenciamento não funcionou
			}
		}
	}
}

/**
 * Created on 17/05/2022
 * @since 1.5.5.034
 */
data class LogonViewModelState(
	val usersMap: Map<String, UserObject>? = null,
	val userFirebase: FirebaseUser? = null,
	val authenticationState: AuthenticationState = AuthenticationState.UNAUTHENTICATED,
	val isFirstAccess: Boolean = true
) {
	fun toUIState() = object : LogonUiState {
		override val usersMap: Map<String, UserObject> = this@LogonViewModelState.usersMap ?: emptyMap()
		override val userEmail: String = userFirebase?.email ?: ""
		override val isUsersLoaded: Boolean
			get() = usersMap.isNotEmpty()
		override val authState: AuthenticationState = authenticationState
		override val openChangePassword: Boolean
			get() {
				userFirebase?.let {
					if (usersMap[it.uid] != null)
						return usersMap[it.uid]!!.firstAccess
					return false
				} ?: return false
			}
		override val toLoginScreen: Boolean = (authenticationState == AuthenticationState.AUTHENTICATED).or(authenticationState == AuthenticationState.UNAUTHENTICATED)
	}
}

/**
 * Created on 17/05/2022
 * @since 1.5.5.034
 */
interface LogonUiState {
	val usersMap: Map<String, UserObject>
	val userEmail: String
	val isUsersLoaded: Boolean
	val authState: AuthenticationState
	val openChangePassword: Boolean
	val toLoginScreen: Boolean
}