package br.com.mario.reservalugar.data

import br.com.mario.reservalugar.ValuesDB
import br.com.mario.reservalugar.ValuesDB.BANK_TAG
import br.com.mario.reservalugar.ValuesDB.MAX_EXTRA
import br.com.mario.reservalugar.ValuesDB.MAX_PARK
import br.com.mario.reservalugar.ValuesDB.MAX_TABLE
import br.com.mario.reservalugar.ValuesDB.MONEY_TAG
import br.com.mario.reservalugar.ValuesDB.PRICE_EXTRA
import br.com.mario.reservalugar.ValuesDB.PRICE_PARK
import br.com.mario.reservalugar.ValuesDB.PRICE_TABLE
import br.com.mario.reservalugar.doubling

import br.com.mario.reservalugar.model.WalletObject
import br.com.mario.reservalugar.model.CategoryObject
import br.com.mario.reservalugar.model.StatusInfo
import br.com.mario.reservalugar.utils.PATHS
import br.com.mario.reservalugar.utils.WALLET

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * @since 10/04/2022
 * Lst mod on 09/05/2022
 */
class ValuesRepository {
	private var tableValues = CategoryObject()
	private var extraValues = CategoryObject()
	private var parkValues = CategoryObject()

	private val bankWallet = MutableStateFlow(WalletObject())
	private val moneyWallet = MutableStateFlow(WalletObject())

	private var priceMap = hashMapOf(PATHS.EXTRA to .0, PATHS.TABLE to .0, PATHS.PARK to .0)
	private var remainMap = hashMapOf(PATHS.EXTRA to -1L, PATHS.TABLE to -1L, PATHS.PARK to -1L)
	private var amountMap = hashMapOf(PATHS.EXTRA to 0L, PATHS.TABLE to 0L, PATHS.PARK to 0L)

	private val statusInfoExtra = MutableStateFlow(StatusInfo(priceMap[PATHS.EXTRA]!!, amountMap[PATHS.EXTRA]!!, remainMap[PATHS.EXTRA]!!, extraValues.total))
	private val statusInfoTable = MutableStateFlow(StatusInfo(priceMap[PATHS.TABLE]!!, amountMap[PATHS.TABLE]!!, remainMap[PATHS.TABLE]!!, tableValues.total))
	private val statusInfoPark = MutableStateFlow(StatusInfo(priceMap[PATHS.PARK]!!, amountMap[PATHS.PARK]!!, remainMap[PATHS.PARK]!!, parkValues.total))

	val isExtraLeft: Boolean
		get() = (remainMap[PATHS.EXTRA]!! > 0)
	val isParkLotLeft: Boolean
	 get() = remainMap[PATHS.PARK]!! > 0

	init {
		ValuesDB.loadConstants { map ->
			amountMap[PATHS.EXTRA] = map[MAX_EXTRA] as Long
			amountMap[PATHS.PARK] = map[MAX_PARK] as Long
			amountMap[PATHS.TABLE] = (map[MAX_TABLE] ?: 120L) as Long
			priceMap = hashMapOf(
				PATHS.EXTRA to map.doubling(PRICE_EXTRA),
				PATHS.TABLE to map.doubling(PRICE_TABLE),
				PATHS.PARK to map.doubling(PRICE_PARK)
			)

			if (remainMap[PATHS.EXTRA] == -1L) remainMap[PATHS.EXTRA] = amountMap[PATHS.TABLE]!!
			if (remainMap[PATHS.PARK] == -1L) remainMap[PATHS.PARK] = amountMap[PATHS.PARK]!!
			renewStatus()
		}
		ValuesDB.loadRemain { rMap ->
			remainMap = hashMapOf(
				PATHS.EXTRA to rMap[PATHS.EXTRA.str] as Long,
				PATHS.PARK to rMap[PATHS.PARK.str] as Long,
				PATHS.TABLE to try { rMap[PATHS.TABLE.str] as Long } catch (e: NullPointerException) { 120 }
			)
			renewStatus()
		}
		ValuesDB.loadInOut {
			bankWallet.value = WalletObject(it[BANK_TAG] as HashMap<*, *>)
			moneyWallet.value = WalletObject(it[MONEY_TAG] as HashMap<*, *>)
		}
		ValuesDB.loadCategories { map ->
			tableValues = CategoryObject(map[PATHS.TABLE.str] as HashMap<*, *>)
			extraValues = CategoryObject(map[PATHS.EXTRA.str] as HashMap<*, *>)
			parkValues = CategoryObject(map[PATHS.PARK.str] as HashMap<*, *>)
			renewStatus()
		}
		@Suppress("UNCHECKED_CAST")
		ValuesDB.changeCategory { onChildChanged { data, _ ->
			setCategoryValue(data.key!!, data.value as HashMap<String, Double>)
			renewStatus()
		} }
		ValuesDB.changeInOut { onChildChanged { data, _ ->
			setInOutValue(WALLET.sto(data.key!!), data.value as HashMap<String, Double>)
		}  }
		ValuesDB.changeRemain { onChildChanged { data, oldName ->
			remainMap[PATHS.sto(data.key!!)] = data.value as Long
			renewStatus()
		} }
	}

	fun observeTableStatus() : Flow<StatusInfo> = statusInfoTable
	fun observeExtraStatus() = statusInfoExtra
	fun observeParkStatus() = statusInfoPark

	fun observeWalletBank() = bankWallet
	fun observeWalletMoney() = moneyWallet

	fun setAmountTables(value: Long) {
		amountMap[PATHS.TABLE] = value
	}

	fun setCategoryValue(tag: String, values: HashMap<String, Double>) = when(PATHS.sto(tag)) {
		PATHS.EXTRA -> extraValues = CategoryObject(values)
		PATHS.TABLE -> tableValues = CategoryObject(values)
		PATHS.PARK -> parkValues = CategoryObject(values)
	}

	fun setInOutValue(tag: WALLET, values: HashMap<String, Double>) = when (tag) {
		WALLET.BANK -> bankWallet.value = WalletObject(values)
		WALLET.MONEY -> moneyWallet.value = WalletObject(values)
	}

	fun getStatusInfo(path: PATHS) = when(path) {
		PATHS.EXTRA -> ResultWrapper.Success(statusInfoExtra.value)
		PATHS.TABLE -> ResultWrapper.Success(statusInfoTable.value)
		PATHS.PARK -> if (remainMap[path]!! == -1L) ResultWrapper.Error("Não carregado disponíveis") else ResultWrapper.Success(statusInfoPark.value)
	}
	private fun renewStatus() {
		statusInfoExtra.value = StatusInfo(priceMap[PATHS.EXTRA]!!, amountMap[PATHS.EXTRA]!!, remainMap[PATHS.EXTRA]!!, extraValues.total)
		statusInfoTable.value = StatusInfo(priceMap[PATHS.TABLE]!!, amountMap[PATHS.TABLE]!!, remainMap[PATHS.TABLE]!!, tableValues.total)
		statusInfoPark.value = StatusInfo(priceMap[PATHS.PARK]!!, amountMap[PATHS.PARK]!!, remainMap[PATHS.PARK]!!, parkValues.total)
	}

	fun updateRemaining(path: PATHS, numb: Int) {
		val newValue = remainMap[path]!!.plus(numb)
		ValuesDB.updateRemain(path.str, newValue)
	}

	fun updateBalance(wallet: WALLET, path: PATHS, isPositive: Boolean) {
		val theCategory = when (path) {
			PATHS.EXTRA -> extraValues
			PATHS.PARK -> parkValues
			PATHS.TABLE -> tableValues
		}
		if (isPositive) {
			setEntrance(wallet, priceMap[path]!!)
			ValuesDB.updateEarn(path.str, wallet.str, theCategory.getWalletValue(wallet).plus(priceMap[path]!!))
		} else {
			updateReversal(wallet, priceMap[path]!!) // trata como estorno quando é nó 'caixa'
			ValuesDB.updateEarn(path.str, wallet.str, theCategory.getWalletValue(wallet).plus(-priceMap[path]!!)) // nó 'categoria' retira o valor do total
		}
	}

	private fun setEntrance(wallet: WALLET, value: Double) {
		val theWallet = if (wallet == WALLET.BANK) bankWallet else moneyWallet
		ValuesDB.updateIn(wallet.str, theWallet.value.receives + value)
	}

	/** Atualiza apenas a saída do nó "carteira" */
	internal fun updateExits(wallet: WALLET, value: Double) {
		val theWallet = if (wallet == WALLET.BANK) bankWallet else moneyWallet
		ValuesDB.updateOut(wallet.str, theWallet.value.exits + value)
	}

	/** Faz a operação de estorno. QUando o valor de uma categoria  */
	private fun updateReversal(wallet: WALLET, value: Double) {
		val theWallet = if (wallet == WALLET.BANK) bankWallet else moneyWallet
		ValuesDB.updateOutReveal(wallet.str, theWallet.value.chargeback + value)
	}

	fun getPrice(category: PATHS) = priceMap[category]!!
}