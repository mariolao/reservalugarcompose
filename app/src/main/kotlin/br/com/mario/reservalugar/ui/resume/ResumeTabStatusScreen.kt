/**
 * Created on 24/01/2022
 * Lst mod on 02/05/2022
 */
package br.com.mario.reservalugar.ui.resume

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape

import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout

import br.com.mario.reservalugar.R

import br.com.mario.reservalugar.model.StatusInfo
import br.com.mario.reservalugar.ui.theme.MyColors
import br.com.mario.reservalugar.ui.theme.Typography
import br.com.mario.reservalugar.ui.widgets.CurrencyTextBuilder

private val currency = CurrencyTextBuilder()

@Composable
fun ResumeTabStatusScreen(uiState: ResumeUiState, isExpandedScreen: Boolean, onRefresh: () -> Unit) {
	if (isExpandedScreen) {
		StatusLargeSector(uiState)
	} else
		StatusSmallSector(uiState, onRefresh)
}

/*@Composable
private fun StatusSector(
	uiState: ResumeUiState,
	onRefresh: () -> Unit = {},
	content: @Composable (uiState: ResumeUiState) -> Unit
) {
	val total = uiState.tableStatus.paydValue + uiState.extraStatus.paydValue + uiState.parkStatus.paydValue

	Column(Modifier.fillMaxSize().background(MyColors.BackgroundMenu)) {
		Column(Modifier.fillMaxWidth().wrapContentHeight(), Arrangement.Center, Alignment.CenterHorizontally) {
			Text(stringResource(R.string.resume_status_title), fontWeight = FontWeight.Bold, style = Typography.h3)
			currency.TextCurrencyLabel(
				fColor = MyColors.TextNumber, fSize = 40.sp, fontWeight = FontWeight.Bold,
				text = TextFieldValue(currency.cleanValue(total))
			)
		}

		content(uiState)
	}
}*/

@Composable
private fun StatusSmallSector(uiState: ResumeUiState, onRefresh: () -> Unit) {
	val total = uiState.tableStatus.paydValue + uiState.extraStatus.paydValue + uiState.parkStatus.paydValue

	Column(Modifier.fillMaxSize().background(MyColors.BackgroundMenu)) {
		when (uiState) {
			is ResumeUiState.NotLoaded -> {
				TextButton(onClick = onRefresh) { //todo: adicionar atualização com SwipeContent
					Text("NÃO CARREGADO",
						Modifier.fillMaxWidth().padding(vertical = 10.dp),
						fontWeight = FontWeight.Bold,
						textAlign = TextAlign.Center,
						style = Typography.h3)
				}
			}
			is ResumeUiState.Loaded -> {
				Column(Modifier.fillMaxWidth().wrapContentHeight(), Arrangement.Center, Alignment.CenterHorizontally) {
					Text(stringResource(R.string.resume_status_title), fontWeight = FontWeight.Bold, style = Typography.h3)
					currency.TextCurrencyLabel(
						fColor = MyColors.TextNumber, fSize = 40.sp, fontWeight = FontWeight.Bold,
						text = TextFieldValue(currency.cleanValue(total))
					)
				}
			}
		} // CABEÇALHO

		Column(Modifier.fillMaxSize(), Arrangement.SpaceEvenly, Alignment.CenterHorizontally) {
			BoxVertical(title = "Mesas", uiState.tableStatus)
			BoxVertical(title = "Extras", uiState.extraStatus)
			BoxVertical(title = "Estacionamento", uiState.parkStatus)
		}
	}
}

/** Mostra o leyout em modo LANDSCAPE. Created on 30/04/2022 */
@Composable
private fun StatusLargeSector(uiState: ResumeUiState) {
	val total = uiState.tableStatus.paydValue + uiState.extraStatus.paydValue + uiState.parkStatus.paydValue

	Column(Modifier.fillMaxSize().background(MyColors.BackgroundMenu)) {
		Row(Modifier.fillMaxWidth(), Arrangement.Center, Alignment.CenterVertically) {
			Text("${stringResource(R.string.resume_status_title)}:", fontWeight = FontWeight.Bold, style = Typography.h4)
			Spacer(Modifier.width(20.dp))
			currency.TextCurrencyLabel(
				fColor = MyColors.TextNumber, fSize = 40.sp, fontWeight = FontWeight.Bold,
				text = TextFieldValue(currency.cleanValue(total))
			)
		}

		Row(Modifier.fillMaxSize(), Arrangement.SpaceAround, Alignment.CenterVertically) {
			BoxHorizontal(title = "Mesas", uiState.tableStatus)
			BoxHorizontal(title = "Extras", uiState.extraStatus)
			BoxHorizontal(title = "Vagas", uiState.parkStatus)
		}
	}
}

@Composable
private fun BoxVertical(title: String, statusInfo: StatusInfo) {
	Card(
		Modifier.fillMaxWidth().padding(horizontal = 10.dp),
		shape = RoundedCornerShape(10.dp),
		border = BorderStroke(1.dp, Color.Green),
		elevation = 20.dp
	) {
		Column(Modifier.fillMaxWidth()) {
			Row(Modifier.fillMaxWidth().padding(vertical = 5.dp, horizontal = 10.dp), horizontalArrangement = Arrangement.SpaceBetween) {
				Text(title, Modifier, MyColors.TextNumber, 20.sp)
				currency.TextCurrencyLabel(
					fColor = MyColors.TextNumber, fSize = 18.sp,
					fontWeight = FontWeight.Bold,
					text = TextFieldValue(currency.cleanValue(statusInfo.paydValue))
				)
			}

			Canvas(Modifier.fillMaxWidth().height((1.5).dp)) {
				drawLine(
					Color.Green, Offset(0f, 0f), Offset(size.width, 0f),
					pathEffect = PathEffect.dashPathEffect(floatArrayOf(10f, 10f), 0f)
				)
			}

			ConstraintLayout(Modifier.fillMaxWidth().padding(top = 5.dp)) {
				val (ttTitle, ttRent, ttPayd) = createRefs()
				val (vvTitle, vvRent, vvPayd) = createRefs()

				Text("Qt. Total", Modifier.constrainAs(ttTitle) {
					top.linkTo(parent.top)
					linkTo(parent.start, ttRent.start, bias = .2f)
				})
				Text("Alugados", Modifier.constrainAs(ttRent) {
					top.linkTo(parent.top)
					start.linkTo(parent.start)
					end.linkTo(parent.end)
				})
				Text("Pagas", Modifier.constrainAs(ttPayd) {
					top.linkTo(parent.top)
					linkTo(ttRent.end, parent.end, bias = .8f)
				})

				Text("${statusInfo.capacity}", Modifier.constrainAs(vvTitle) {
					top.linkTo(ttTitle.bottom)
					linkTo(ttTitle.start, ttTitle.end)
				}, MyColors.TextNumber, fontWeight = FontWeight.Bold)
				Text("${statusInfo.rented}", Modifier.constrainAs(vvRent) {
					top.linkTo(ttRent.bottom)
					linkTo(ttRent.start, ttRent.end)
				}, MyColors.TextNumber, fontWeight = FontWeight.Bold)
				Text("${statusInfo.paid.toInt()}", Modifier.constrainAs(vvPayd) {
					top.linkTo(ttTitle.bottom)
					linkTo(ttPayd.start, ttPayd.end)
				}, MyColors.TextNumber, fontWeight = FontWeight.Bold)
			}
			Spacer(Modifier.height(10.dp))

			Canvas(Modifier.fillMaxWidth().height(1.dp)) {
				drawLine(
					Color.Black, Offset(0f, 0f), Offset(size.width, 0f),
					pathEffect = PathEffect.dashPathEffect(floatArrayOf(8f, 8f), 5f)
				)
			}
			Spacer(Modifier.height(5.dp))

			Column(Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
				Text("Total previsto")
				currency.TextCurrencyLabel(
					fColor = MyColors.TextNumber,
					fSize = 18.sp,
					fontWeight = FontWeight.Bold,
					text = TextFieldValue(currency.cleanValue(statusInfo.prev))
				)
			}
			Spacer(Modifier.height(5.dp))
		}
	}
}

/** Created on 30/04/2022 */
@Composable
private fun BoxHorizontal(title: String, statusInfo: StatusInfo) {
	Card(
		Modifier.wrapContentHeight().width(200.dp).padding(vertical = 10.dp),
		shape = RoundedCornerShape(10.dp),
		border = BorderStroke(1.dp, Color.Green),
		elevation = 20.dp
	) {
		val linePdd = 20f
		Column(Modifier.wrapContentHeight()) {
			Column(Modifier.fillMaxWidth(), Arrangement.SpaceAround, Alignment.CenterHorizontally) {
				Text(title, Modifier.padding(top = 8.dp), MyColors.TextNumber, 30.sp)
				Spacer(Modifier.height(8.dp))
				currency.TextCurrencyLabel(
					fColor = MyColors.TextNumber, fSize = 20.sp,
					fontWeight = FontWeight.Bold,
					text = TextFieldValue(currency.cleanValue(statusInfo.paydValue))
				)
				Spacer(Modifier.height(10.dp))
			} // cabeçalho de título e valor total

			Canvas(Modifier.fillMaxWidth().height((1.5).dp)) {
				drawLine(
					Color.Green, Offset(linePdd, 0f), Offset(size.width - linePdd, 0f),
					pathEffect = PathEffect.dashPathEffect(floatArrayOf(10f, 10f), 0f)
				)
			}

			ConstraintLayout(Modifier.fillMaxWidth().padding(top = 5.dp)) {
				val (ttTitle, ttRent, ttPayd) = createRefs()
				val (vvTitle, vvRent, vvPayd) = createRefs()
				val (spc01, spc02) = createRefs()

				Text("Qt. Total", Modifier.constrainAs(ttTitle) {
					top.linkTo(parent.top)
					linkTo(parent.start, parent.end, bias = .25f)
				})
				Spacer(Modifier.height(8.dp).constrainAs(spc01) {
					top.linkTo(ttTitle.bottom)
					bottom.linkTo(ttRent.top)
				})
				Text("Alugados", Modifier.constrainAs(ttRent) {
					top.linkTo(spc01.bottom)
					end.linkTo(ttTitle.end)
				})
				Spacer(Modifier.height(8.dp).constrainAs(spc02) {
					top.linkTo(ttRent.bottom)
					bottom.linkTo(ttPayd.top)
				})
				Text("Pagas", Modifier.constrainAs(ttPayd) {
					top.linkTo(spc02.bottom)
					end.linkTo(ttTitle.end)
				})

				Text("${statusInfo.capacity}", Modifier.constrainAs(vvTitle) {
					top.linkTo(parent.top)
					linkTo(parent.start, parent.end, bias = .75f)
					baseline.linkTo(ttTitle.baseline)
				}, MyColors.TextNumber, fontWeight = FontWeight.Bold)
				Text("${statusInfo.rented}", Modifier.constrainAs(vvRent) {
					linkTo(parent.start, parent.end, bias = .75f)
					baseline.linkTo(ttRent.baseline)
				}, MyColors.TextNumber, fontWeight = FontWeight.Bold)
				Text("${statusInfo.paid.toInt()}", Modifier.constrainAs(vvPayd) {
					linkTo(parent.start, parent.end, bias = .75f)
					baseline.linkTo(ttPayd.baseline)
				}, MyColors.TextNumber, fontWeight = FontWeight.Bold)
			}
			Spacer(Modifier.height(10.dp))

			Canvas(Modifier.fillMaxWidth().height(1.dp)) {
				drawLine(
					Color.Black, Offset(linePdd, 0f), Offset(size.width - linePdd, 0f),
					pathEffect = PathEffect.dashPathEffect(floatArrayOf(8f, 8f), 5f)
				)
			}
			Spacer(Modifier.height(5.dp))

			Column(Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
				Text("Previsto", Modifier.padding(bottom = 8.dp))
				currency.TextCurrencyLabel(
					fColor = MyColors.ButtonStatusOFF,
					fSize = 18.sp,
					fontWeight = FontWeight.Bold,
					text = TextFieldValue(currency.cleanValue(statusInfo.prev))
				)
			}
			Spacer(Modifier.height(5.dp))
		}

	}
}

@Preview(name = "Tela em modo vertical com itens carregados")
@Composable
fun PreviewStatus() {
	StatusSmallSector(ResumeUiState.Loaded(
		isLoading = false,
		tableStatus = StatusInfo(180.0, 30L, 3L, 2700.0),
		extraStatus = StatusInfo(180.0, 30L, 3L, 2700.0),
		parkStatus = StatusInfo(180.0, 30L, 3L, 2700.0)
	), {})
}

@Preview(name = "Caixa de detalhes do modo horizontal")
@Composable
fun PreviewStatusBox() {
	BoxHorizontal(
		title = "Extras",
		statusInfo = StatusInfo(80.5, 20L, 6L, 805.0)
	)
}

@Preview(name = "Tela em modo horizontal", device = Devices.PIXEL_C)
@Composable
fun PreviewStatusLandscape() {
	StatusLargeSector(ResumeUiState.Loaded(
		isLoading = false,
		tableStatus = StatusInfo(180.0, 120L, 3L, 2700.0),
		extraStatus = StatusInfo(80.00, 20L, 6L, 800.0),
		parkStatus = StatusInfo(10.75, 30L, 3L, 172.0)
	) )
}