package br.com.mario.reservalugar.utils

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.ui.platform.LocalContext

/**
 * To set the desired orientation when the screen appears and bring it back when it disappears
 *
 * @see https://stackoverflow.com/a/69231996/3443949
 * Added on 19/04/2022
 */

@Composable
fun LockScreenOrientation(orientation: Int) {
	val context = LocalContext.current
	DisposableEffect(Unit) {
		val activity = context.findActivity() ?: return@DisposableEffect onDispose {}
		val originalOrientation = activity.requestedOrientation
		activity.requestedOrientation = orientation
		onDispose {
			// restore original orientation when view disappears
			activity.requestedOrientation = originalOrientation
		}
	}
}

fun Context.findActivity(): Activity? = when (this) {
	is Activity -> this
	is ContextWrapper -> baseContext.findActivity()
	else -> null
}