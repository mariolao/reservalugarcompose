/**
 * @author Mário Henrique
 *
 * Created on 15/12/2021
 * Lst mod on 05/05/2022
 */
package br.com.mario.reservalugar.ui.cost

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.animation.core.tween
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState

import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.outlined.AddCircle

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateList

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension

import br.com.mario.reservalugar.R
import br.com.mario.reservalugar.compare
import br.com.mario.reservalugar.model.CostObject

import br.com.mario.reservalugar.ui.theme.MyColors
import br.com.mario.reservalugar.ui.widgets.CurrencyTextBuilder
import br.com.mario.reservalugar.ui.widgets.EmptyScreen
import br.com.mario.reservalugar.ui.widgets.MultiFabBuilder.FabState
import br.com.mario.reservalugar.ui.widgets.MultiFabBuilder.MultiFAB
import br.com.mario.reservalugar.ui.widgets.dialogs.DialogBuilder.DialogCosts
import br.com.mario.reservalugar.ui.widgets.dialogs.DialogCostContent

import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState

/**
 * Tela de controle dos gastos.
 * Mostrados em formato de lista, com total ao final
 * Created on 29/04/2022
 */
@Composable
fun CostSimpleListScreen(
	uiState: CostUiState,
	onSelectAndOpen: (Boolean, Int) -> Unit,
	onAddItem: (CostObject) -> Unit,
	onRemoveItem: (CostObject) -> Unit,
	onRefresh: () -> Unit
) {
	CostScreenWithList(
		uiState = uiState,
		onRefresh = onRefresh
	) { hasCostState ->
		Box {
			CostList(hasCostState, onRemoveItem, onSelectAndOpen)

			if (hasCostState.openAddDialog) {
				DialogCosts(
					hasCostState.selectedCost ?: CostObject(),
					onDismiss = { onSelectAndOpen(false, -1) },
					onPositiveClick = { c: CostObject, _ ->
						onSelectAndOpen(false, -1)
						onAddItem(c) //						addItem(c, init)
					}
				)
			}
		}
	}
}

/** Gerenciar tela em no modo expandido. Created on 28/04/2022 */
@Composable
fun CostListWithDetailsScreen(
	uiState: CostUiState,
	onRefresh: () -> Unit,
	onAddItem: (CostObject) -> Unit,
	onRemoveItem: (CostObject) -> Unit,
	onSelectAndOpen: (Boolean, Int) -> Unit,
	lazyListState: LazyListState
) {
	CostScreenWithList(uiState = uiState, onRefresh = onRefresh) { hasCostState ->
		Row {
			Column(Modifier.weight(.48f)) {
				CostList(
					hasCostState = hasCostState,
					onRemoveItem = onRemoveItem,
					onSelectAndOpen = onSelectAndOpen,
					state = lazyListState
				)
			}
			Divider(Modifier.fillMaxHeight().weight(.04f), Color.LightGray)
			Column(Modifier.weight(.48f)) {
				if (hasCostState.openAddDialog) {
					Crossfade(targetState = hasCostState.selectedCost) { detailCost ->
						key(detailCost?.let { it.hash!! } ?: "somenewitem") {
							DialogCostContent(
								cost = detailCost ?: CostObject(),
								onDismiss = { onSelectAndOpen(false, -1) },
								onPositiveClick = { cost, _ ->
									onSelectAndOpen(false, -1)
									onAddItem(cost)
								}
							)
						}
					}
				} //todo: adicionar um ELSE com uma tela de item não selecionado
			}
		}
	}
}

/** Encapsula os componentes responsáveis por mostrar e gerenciar a listagem. Created on 29/04/2022 */
@Composable
private fun CostScreenWithList(
	uiState: CostUiState,
	onRefresh: () -> Unit,
	hasCostContent: @Composable (uiState: CostUiState.HasItems) -> Unit
) {
	LoadingContent(
		empty = if (uiState is CostUiState.HasItems) false else uiState.isLoading,
		emptyContent = { FullScreenLoading() },
		loading = uiState.isLoading,
		onRefresh = onRefresh,
		content = {
			when (uiState) {
				is CostUiState.HasItems -> hasCostContent(uiState)
				is CostUiState.NoItems -> {
					if (uiState.errorMessages.isEmpty()) {
						// if there are no posts, and no error, let the user refresh manually
						TextButton(
							onClick = onRefresh,
							Modifier.fillMaxSize()
						) {
							Text(
								stringResource(id = R.string.tap_to_load_content),
								textAlign = TextAlign.Center
							)
						}
					} else EmptyScreen()
				}
			}
		}
	)
}

/** Mostra a listagem e gerencia os gastos. Created on 29/04/2022 */
@Composable
private fun CostList(
	hasCostState: CostUiState.HasItems,
	onRemoveItem: (CostObject) -> Unit,
	onSelectAndOpen: (Boolean, Int) -> Unit,
	state: LazyListState = rememberLazyListState()
) {
	Box(Modifier.padding(top = 5.dp)) {
		if (hasCostState.costList.isNotEmpty()) {
			ConstraintLayout(Modifier.fillMaxSize().padding(8.dp)) {
				val (headerRef, scrollRef, totalRef) = createRefs()

				Spacer(Modifier.height(5.dp))
				Row(Modifier.constrainAs(headerRef) { // representa o CABEÇALHO da lista
					top.linkTo(parent.top)
					start.linkTo(parent.start)
				}.padding(horizontal = 5.dp)
				) {
					Text("DATA", Modifier.padding(end = 60.dp), fontWeight = FontWeight.Bold)
					Text("DESCRIÇÃO", Modifier.width(150.dp), fontWeight = FontWeight.Bold)
					Text("VALOR", Modifier.padding(start = 16.dp), fontWeight = FontWeight.Bold)
				}

				val deleted = remember { mutableStateListOf<CostObject>() }
				LazyColumn(Modifier.fillMaxWidth().constrainAs(scrollRef) {
					top.linkTo(headerRef.bottom)
					bottom.linkTo(totalRef.top, 8.dp)
					start.linkTo(parent.start)
					end.linkTo(parent.end)
					height = Dimension.fillToConstraints
				}, state) {
					itemsIndexed(hasCostState.costList.sortedWith(compare({ it._time }, { it.desc }))) { i, costs ->
						AnimatedVisibility(
							visible = !deleted.contains(costs),
							enter = expandVertically(),
							exit = shrinkVertically(animationSpec = tween(1000)),
							content = { CostItem(costs, deleted, (i % 2 == 0), onRemoveItem) { onSelectAndOpen(true, i) } }
						)
					}
				}

				Row(Modifier.fillMaxWidth().constrainAs(totalRef) {
					top.linkTo(scrollRef.bottom)
					bottom.linkTo(parent.bottom, 5.dp)
					start.linkTo(parent.start)
					end.linkTo(parent.end)
				}.padding(start = 16.dp, end = 8.dp), Arrangement.SpaceBetween) {
					Text("TOTAL", Modifier, MaterialTheme.colors.primary, 20.sp, fontWeight = FontWeight.Bold)
					CurrencyTextBuilder().TextCurrencyLabel(
						fColor = MaterialTheme.colors.primary,
						fSize = 20.sp, fontWeight = FontWeight.Bold,
						text = hasCostState.totalTFV
					)
				}
			}
		} else
			EmptyScreen() //todo: solução temporária para adicionar itens quando lista vem vazia

		var addFabState by remember { mutableStateOf(FabState.COLLAPSED) }
		MultiFAB(
			Icons.Outlined.AddCircle,
			items = listOf(),
			modifier = Modifier.align(Alignment.BottomCenter).padding(bottom = 30.dp),
			toState = addFabState, stateChanged = {
				addFabState = it
				onSelectAndOpen(true, -1)
			}, onFabItemClicked = {}
		)
	}
}

@Composable
private fun CostItem(cost: CostObject, action: SnapshotStateList<CostObject>, bg: Boolean, onDelete: (CostObject) -> Unit, click: (CostObject) -> Unit) {
	Card(Modifier.height(50.dp).padding(vertical = 5.dp), backgroundColor = if (bg) Color.LightGray else Color.White, elevation = 20.dp) {
		Row(Modifier.fillMaxSize()) {
			Row(
				Modifier.fillMaxHeight().weight(.099f).padding(horizontal = 5.dp).clickable { click(cost) },
				verticalAlignment = Alignment.CenterVertically
			) {
				Text(cost.date, Modifier.padding(end = 25.dp), fontWeight = FontWeight.Bold)
				Text(cost.desc, Modifier.width(165.dp), fontWeight = FontWeight.Bold)
				Text(cost._real, Modifier.padding(end = 1.dp))
			}

			IconButton(
				onClick = {
					action.add(cost)
					onDelete(cost)
				},
				modifier = Modifier.weight(.01f),
				content = {
					Icon(imageVector = Icons.Filled.Delete, contentDescription = null, tint = MyColors.Holo_RedDragon)
				}
			)
		} // External Row

	} // Card
}

/**
 * Display an initial empty state or swipe to refresh content.
 *
 * @param empty (state) when true, display [emptyContent]
 * @param emptyContent (slot) the content to display for the empty state
 * @param loading (state) when true, display a loading spinner over [content]
 * @param onRefresh (event) event to request refresh
 * @param content (slot) the main content to show
 */
@Composable
private fun LoadingContent(
	empty: Boolean,
	emptyContent: @Composable () -> Unit,
	loading: Boolean,
	onRefresh: () -> Unit,
	content: @Composable () -> Unit
) {
	if (empty)
		emptyContent()
	else {
		SwipeRefresh(
			state = rememberSwipeRefreshState(loading),
			onRefresh = onRefresh,
			content = content,
		)
	}
}

/** Full screen circular progress indicator */
@Composable
private fun FullScreenLoading() {
	Box(modifier = Modifier.fillMaxSize().wrapContentSize(Alignment.Center)) {
		CircularProgressIndicator()
	}
}

@Preview
@Composable
private fun PreviewLoading() {
	FullScreenLoading()
}

/*
@ExperimentalAnimationApi
@Preview
@Composable
private fun PreviewCostScreen() = CostBuilder.CostScreen()*/