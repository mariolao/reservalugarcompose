/**
 * @author Mário Henrique
 * Created on 02/05/2022
 */
package br.com.mario.reservalugar.ui.resume

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import br.com.mario.reservalugar.data.ResultWrapper

import br.com.mario.reservalugar.data.ValuesRepository
import br.com.mario.reservalugar.model.StatusInfo
import br.com.mario.reservalugar.model.WalletObject
import br.com.mario.reservalugar.utils.PATHS

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/** ViewModel que gerencia a lógica de negócio da Tela Resumo. Created on 25/04/2022 */
class ResumeTabViewModel(private val valuesRepository: ValuesRepository) : ViewModel() {
	private val vmStatusState = MutableStateFlow(ResumeViewModelState(isLoading = true))
	private val vmBalanceState = MutableStateFlow(BalanceTabViewModelState(isLoading = true))

	val uiCategoryState = vmStatusState.map { it.toUIState() }
		.stateIn(viewModelScope, SharingStarted.Eagerly, vmStatusState.value.toUIState())
	val uiBalanceState = vmBalanceState.map { it.toUIState() }
		.stateIn(viewModelScope, SharingStarted.Eagerly, vmStatusState.value.toUIState())

	private val emptyInfo = StatusInfo(.0, 0, -1, .0)

	init {
		refreshInfos()

		viewModelScope.launch {
			valuesRepository.observeTableStatus().collect { info ->
				vmStatusState.update { it.copy(tableInfo = testInfo(info), isLoading = false) }
			}
		}
		viewModelScope.launch {
			valuesRepository.observeExtraStatus().collect { info ->
				vmStatusState.update { it.copy(extraInfo = testInfo(info), isLoading = false) }
			}
		}
		viewModelScope.launch {
			valuesRepository.observeParkStatus().collect { info ->
				vmStatusState.update { it.copy(parkInfo = testInfo(info), isLoading = false) }
			}
		}
		viewModelScope.launch {
			valuesRepository.observeWalletBank().collect { bankObject ->
				vmBalanceState.update { it.copy(bank = bankObject) }
			}
		}
		viewModelScope.launch {
			valuesRepository.observeWalletMoney().collect { moneyObject ->
				vmBalanceState.update { it.copy(money = moneyObject) }
			}
		}
	}
	/** Chamada para atualização forçada dos valores */
	fun refreshInfos() {
		refreshExtraInfo()
		refreshTableInfo()
		refreshParkInfo()
	}

	private fun refreshExtraInfo() {
		vmStatusState.update { it.copy(isLoading = true) }

		viewModelScope.launch {
			val result = valuesRepository.getStatusInfo(PATHS.EXTRA)

			vmStatusState.update { vmState ->
				when (result) {
					is ResultWrapper.Success -> vmState.copy(extraInfo = testInfo(result.data), isLoading = false)
					is ResultWrapper.Error -> vmState.copy(isLoading = false)
				}
			}
		}
	}
	private fun refreshTableInfo() {
		vmStatusState.update { it.copy(isLoading = true) }

		viewModelScope.launch {
			val result = valuesRepository.getStatusInfo(PATHS.TABLE)

			vmStatusState.update { vmState ->
				when (result) {
					is ResultWrapper.Success -> vmState.copy(tableInfo = testInfo(result.data), isLoading = false)
					is ResultWrapper.Error -> vmState.copy(isLoading = false)
				}
			}
		}
	}
	private fun refreshParkInfo() {
		vmStatusState.update { it.copy(isLoading = true) }

		viewModelScope.launch {
			val result = valuesRepository.getStatusInfo(PATHS.PARK)

			vmStatusState.update { vmState ->
				when (result) {
					is ResultWrapper.Success -> vmState.copy(parkInfo = testInfo(result.data), isLoading = false)
					is ResultWrapper.Error -> vmState.copy(isLoading = false)
				}
			}
		}
	}

	private fun testInfo(info: StatusInfo?) : StatusInfo? =
		if (info == emptyInfo) null
		else info

	companion object {
		fun provideFactory(valuesRepository: ValuesRepository): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
			override fun <T : ViewModel> create(modelClass: Class<T>) = ResumeTabViewModel(valuesRepository) as T
		}
	}
}

private data class ResumeViewModelState(
	val isLoading: Boolean = false,
	val extraInfo: StatusInfo? = null,
	val tableInfo: StatusInfo? = null,
	val parkInfo: StatusInfo? = null
) {
	fun toUIState() : ResumeUiState =
		if (tableInfo == null || extraInfo == null || parkInfo == null) {
			ResumeUiState.NotLoaded(
				isLoading = false,
			)
		} else
			ResumeUiState.Loaded(
				extraStatus = extraInfo,
				tableStatus = tableInfo,
				parkStatus = parkInfo,
				isLoading = false
			)
}
/** Created on 27/04/2022 */
private data class BalanceTabViewModelState(
	val isLoading: Boolean = false,
	val bank: WalletObject? = null,
	val money: WalletObject? = null
) {
	fun toUIState() : BalanceUiState =
		if (bank == null || money == null) {
			BalanceUiState.NotLoaded(isLoading = false)
		} else {
			BalanceUiState.Loaded(
				bankInOut = bank,
				moneyInOut = money,
				isLoading = isLoading
			)
		}
}

sealed interface ResumeUiState {
	val isLoading: Boolean
	val tableStatus: StatusInfo
	val extraStatus: StatusInfo
	val parkStatus: StatusInfo

	data class NotLoaded(
		override val isLoading: Boolean,
		override val tableStatus: StatusInfo = StatusInfo(.0, 0, 0, .0),
		override val extraStatus: StatusInfo = StatusInfo(.0, 0, 0, .0),
		override val parkStatus: StatusInfo = StatusInfo(.0, 0, 0, .0)
	) : ResumeUiState

	data class Loaded(
		override val isLoading: Boolean,
		override val tableStatus: StatusInfo,
		override val extraStatus: StatusInfo,
		override val parkStatus: StatusInfo
	) : ResumeUiState
}
/** Created on 27/04/2022 */
sealed interface BalanceUiState {
	val isLoading: Boolean
	val bankInOut: WalletObject
	val moneyInOut: WalletObject

	data class NotLoaded(
		override val isLoading: Boolean,
		override val bankInOut: WalletObject = WalletObject(),
		override val moneyInOut: WalletObject = WalletObject()
	) : BalanceUiState

	data class Loaded(
		override val isLoading: Boolean,
		override val bankInOut: WalletObject,
		override val moneyInOut: WalletObject
	) : BalanceUiState
}