package br.com.mario.reservalugar.ui.widgets

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import br.com.mario.reservalugar.R

/** Created on 14/02/2022 */
@Composable
fun EmptyScreen() {
	Column(Modifier.fillMaxSize(), Arrangement.Center, Alignment.CenterHorizontally) {
		Column(Modifier.wrapContentSize(), horizontalAlignment = Alignment.CenterHorizontally) {
			Image(painterResource(R.drawable.icon_empty_256), null, Modifier.padding(bottom = 20.dp))
			Text("Não há items ainda", color = Color.Blue, fontSize = 25.sp, fontWeight = FontWeight.Bold)
		}
	}
}