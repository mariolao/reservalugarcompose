package br.com.mario.reservalugar.nav

/** Lst mod on 11/05/2022 */
sealed class Routes(val route: String) {
	object MenuRoute : Routes("menu")
	object TablesRoute : Routes("places")
	object CostsRoute : Routes("costs")
	object GainsRoute : Routes("gains")
	object ResumeRoute : Routes("resume")
	object LogonRoute : Routes("logon")
	object SettingRoute: Routes("setting")
}