/**
 * @author Mário Henrique
 * Created on 19/04/2022
 * Lst mod on 27/04/2022
 */
package br.com.mario.reservalugar.ui.gains

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import br.com.mario.reservalugar.model.TableObject
import br.com.mario.reservalugar.utils.PATHS
import br.com.mario.reservalugar.utils.WALLET

import kotlin.reflect.KFunction0
import kotlin.reflect.KFunction4
import kotlin.reflect.KFunction5

/** Created on 20/04/2022 */
@Composable
fun GainRoute(gainVM: GainViewModel, isExpanded: Boolean) {
//	LockScreenOrientation(orientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR)
	val uiState by gainVM.uiState.collectAsState()

	GainRoute(
		uiState = uiState,
		isExpandedScreen = isExpanded,
		onPayElement = gainVM::payment,
		onSelectElement = gainVM::selectItem,
		onHideDialogs = gainVM::hideAllDialogs
	)
}

/**
 * @param uiState controla os dados que são mostrados na tela
 * @param isExpandedScreen diz quando a tela está em modo expandido
 */
@Composable
fun GainRoute(
	uiState: GainsUiState,
	isExpandedScreen: Boolean,
	onPayElement: KFunction4<TableObject, PATHS, WALLET, Boolean, Unit>,
	onSelectElement: KFunction5<String, PATHS, String, WALLET?, Boolean, Unit>,
	onHideDialogs: KFunction0<Unit>,
) {
	when (getScreenType(isExpandedScreen, uiState)) {
		GainScreenType.OneList -> {
			GainTabBasedScreen(
				uiState = uiState,
				onPayClick = onPayElement,
				onSelectedElement = onSelectElement,
				onHideDialogs
			)
		}
		GainScreenType.DoubleList -> {
			GainDoubleListedScreen(
				uiState = uiState,
				onPayClick = onPayElement,
				onSelectItem = onSelectElement,
				onHideDialogs = onHideDialogs
			)
		}
	}
}

/**
 * Represents the types based on the screen available space
 */
private enum class GainScreenType {
	DoubleList, OneList
}

private fun getScreenType(isExpandedScreen: Boolean, uiState: GainsUiState) : GainScreenType = when(isExpandedScreen) {
	false -> GainScreenType.OneList
	true -> GainScreenType.DoubleList
}