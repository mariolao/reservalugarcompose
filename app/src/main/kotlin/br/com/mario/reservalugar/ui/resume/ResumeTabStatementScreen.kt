/**
 * Aba da Tela Resumo [ResumeScreen] responsável por mostra o extrato,
 * um histórico das operações.
 *
 * Created on 24/01/2022
 * Lst mod on 09/05/2022
 */
package br.com.mario.reservalugar.ui.resume

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed

import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

import br.com.mario.reservalugar.model.CostReport
import br.com.mario.reservalugar.model.GainReport
import br.com.mario.reservalugar.model.ReportObject
import br.com.mario.reservalugar.model.TableReport

import br.com.mario.reservalugar.monetary
import br.com.mario.reservalugar.ui.widgets.EmptyScreen

/** Rota de entrada para construção do layout da Aba Extrato */
@Composable
fun ResumeTabStatementRoute(statementVM: StatementViewModel, isExpandedScreen: Boolean, onRefresh: () -> Unit) {
	val uiState: StatementUiState by statementVM.uiState.collectAsState()
	ResumeTabStatementScreen(uiState, isExpandedScreen = isExpandedScreen, onRefresh = onRefresh)
}

@Composable
private fun ResumeTabStatementScreen(uiState: StatementUiState, isExpandedScreen: Boolean, onRefresh: () -> Unit) {
	Box(Modifier.fillMaxSize()) {
		when (uiState) {
			is StatementUiState.NoItems -> EmptyScreen()
			is StatementUiState.HasItems -> {
				if (isExpandedScreen) {
					ExpandedStatementBody(uiState)
				}
				else
					SmallStatementBody(uiState)
			}
		}
	}
}

@Composable
private fun SmallStatementBody(uiState: StatementUiState.HasItems) {
	Column(Modifier.fillMaxSize()) {
		Row(Modifier.height(15.dp).fillMaxWidth().padding(horizontal = 5.dp)) {
			Text("DATA", Modifier.weight(.195f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(.01f), Color.Black)

			Text("OPER", Modifier.weight(.23f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(.01f), Color.Black)

			Text("DESC", Modifier.weight(.29f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(.01f), Color.Black)

			Text("CART", Modifier.weight(.13f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(.01f), Color.Black)

			Text("VALOR", Modifier.weight(.115f), textAlign = TextAlign.Center)
		} // CABEÇALHO

		LazyColumn(Modifier.fillMaxWidth()) {
			itemsIndexed(uiState.reportList/*.sortedWith(compare({ it.dateL }))*/) { idx, item ->
				StatementItem(item = item, bg = (idx % 2 == 0)) {}
			}
		}
	}
}

/**
 * Build an expanded screen, i.e., it shows more items in the Row
 * @since 07/05/2022
 */
@Composable
private fun ExpandedStatementBody(uiState: StatementUiState.HasItems) {
	Column(Modifier.fillMaxSize()) {
		Row(Modifier.height(15.dp).fillMaxWidth().padding(horizontal = 5.dp)) {
			Text("DATA", Modifier.weight(115f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(10f), Color.Black)

			Text("VENDEDOR", Modifier.weight(134f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(10f), Color.Black)

			Text("COMPRADOR", Modifier.weight(134f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(10f), Color.Black)

			Text("OPERACAO", Modifier.weight(154f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(10f), Color.Black)

			Text("DESCRICAO", Modifier.weight(154f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(10f), Color.Black)

			Text("CARTEIRA", Modifier.weight(139f), textAlign = TextAlign.Center)
			Divider(Modifier.fillMaxHeight().weight(10f), Color.Black)

			Text("VALOR", Modifier.weight(110f), textAlign = TextAlign.Center)
		}

		LazyColumn(Modifier.fillMaxWidth()) {
			itemsIndexed(uiState.reportList) { idx, item ->
				ExpandedStatementItem(item = item, bg = (idx % 2 == 0))
			}
		}
	}
}

@Composable
private fun StatementItem(item: ReportObject, bg: Boolean, click: (ReportObject) -> Unit) { //todo 'click' é para abrir uma janela com detalhamento na tela pequena
	Card(Modifier.height(50.dp).padding(vertical = 5.dp), backgroundColor = if (bg) Color.LightGray else Color.White, elevation = 20.dp) {
		Row(Modifier.fillMaxSize().padding(horizontal = 5.dp), verticalAlignment =  Alignment.CenterVertically) {
			Text(item.dateShort, Modifier.weight(.205f), textAlign = TextAlign.Center)
			Text(item.op, Modifier.weight(.24f), textAlign = TextAlign.Center)
			Text(item.target, Modifier.weight(.30f), textAlign = TextAlign.Center)

			when(item) {
				is CostReport -> {
					Text(item.wallet, Modifier.weight(.14f), textAlign = TextAlign.Center, maxLines = 1, overflow = TextOverflow.Ellipsis)
					Text("${item.value}", Modifier.weight(.13f), textAlign = TextAlign.Center)
				}
				is TableReport -> Spacer(Modifier.weight(.255f))
				is GainReport -> {
					Text(item.wallet, Modifier.weight(.14f), textAlign = TextAlign.Center, maxLines = 1, overflow = TextOverflow.Ellipsis)
					Text("${item.value}", Modifier.weight(.13f), textAlign = TextAlign.Center)
				}
			}
		}
	}
}

/**
 * Build the expanded column item for the data.
 * @since 07/05/2022
 */
@Composable
private fun ExpandedStatementItem(item: ReportObject, bg: Boolean) {
	Card(Modifier.height(50.dp).padding(vertical = 5.dp), backgroundColor = if (bg) Color.LightGray else Color.White, elevation = 20.dp) {
		Row(Modifier.fillMaxSize().padding(end = 5.dp), verticalAlignment = Alignment.CenterVertically) {
			Text(item.date, Modifier.weight(120f), textAlign = TextAlign.Center)
			Text(item.seller, Modifier.weight(144f), textAlign = TextAlign.Center)
			when (item) {
				is TableReport -> Text(item.buyer, Modifier.weight(144f), textAlign = TextAlign.Center)
				is GainReport -> Text(item.buyer, Modifier.weight(144f), textAlign = TextAlign.Center)
				else -> Spacer(Modifier.weight(144f))
			}
			Text(item.op, Modifier.weight(164f), textAlign = TextAlign.Center)
			Text(item.target, Modifier.weight(164f), textAlign = TextAlign.Center)

			when(item) {
				is CostReport -> {
					Text(item.wallet, Modifier.weight(149f), textAlign = TextAlign.Center)
					Text(item.value.monetary(), Modifier.weight(115f), textAlign = TextAlign.Center)
				}
				is TableReport -> Spacer(Modifier.weight(264f))
				is GainReport -> {
					Text(item.wallet, Modifier.weight(149f), textAlign = TextAlign.Center)
					Text(item.value.monetary(), Modifier.weight(115f), textAlign = TextAlign.Center)
				}
			}
		}
	}
}

@Preview("Tela Extrato no modo curto")
@Composable
private fun PreviewStatementBody() {
	SmallStatementBody(StatementUiState.HasItems(
		isLoading = false,
		reportList = listOf(
			TableReport("19/03/2022", "reserva", "mesa 9", "Adalberto", "atual"),
			CostReport("20/03/2022", "gasto", "seguranças", "alguém", 550.0, "banco"),
			GainReport("03/05/2022", "reserva", "mesa 23", "Roberto D'Ávila","Raimudinha", 150.0, "banco")
		)
	))
}