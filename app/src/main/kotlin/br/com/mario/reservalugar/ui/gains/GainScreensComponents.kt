/**
 * Contém as funções de componentes que gerenciam em formato de listas as compras
 * de mesas, extras e vagas de estacionamento.
 *
 * Created on 21/04/2022
 * Lst mod on 09/05/2022
 */
package br.com.mario.reservalugar.ui.gains

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items

import androidx.compose.material.Checkbox
import androidx.compose.material.Divider
import androidx.compose.material.Text

import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.PathEffect
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

import br.com.mario.reservalugar.compare
import br.com.mario.reservalugar.model.TableObject

import br.com.mario.reservalugar.ui.theme.MyColors
import br.com.mario.reservalugar.ui.widgets.dialogs.DialogBuilder
import br.com.mario.reservalugar.ui.widgets.dialogs.OnDismiss
import br.com.mario.reservalugar.utils.PATHS
import br.com.mario.reservalugar.utils.WALLET
import br.com.mario.reservalugar.walletRes

@Composable
internal fun TableReservePaymentMiniList(
	uiState: GainsUiState.Loaded,
	onSelectedElement: (itemId: String, target: PATHS, owner: String, wallet: WALLET?, check: Boolean) -> Unit,
	onPayClick: (TableObject, PATHS, WALLET, Boolean) -> Unit,
	onHideDialogs: OnDismiss,
	modifier: Modifier = Modifier.fillMaxSize()
) {
	val mapOfCheckTable = HashMap<String, MutableState<Boolean>>()
	val mapOfCheckExtra = HashMap<String, MutableState<Boolean>>()

	ScrollComponent(uiState,
		uiState.tableReservationMiniList.sortedWith(compare({ it.dateStamp }, { it.buyer })),
		onPayClick, onHideDialogs,
		checkExtraStatus = mapOfCheckExtra, checkTableStatus = mapOfCheckTable,
		modifier = modifier,
		headerLayout = {
			Text("DATA", Modifier.weight(1.5f), MyColors.TextNumber, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center)
			Divider(Modifier.wrapContentHeight().weight(.1f), Color.Transparent)

			Text("NOME", Modifier.weight(3.5f), MyColors.TextNumber, fontWeight = FontWeight.Bold, textAlign = TextAlign.Start)
			Divider(Modifier.wrapContentHeight().weight(.1f), Color.Transparent)

			Text("MESA", Modifier.weight(1.6f), MyColors.TextNumber, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center)

			Text("PAGO", Modifier.weight(1.6f), MyColors.TextNumber, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center)
			Text("EXTRA", Modifier.weight(1.6f), MyColors.TextNumber, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center)
		},
		lineLayout = { sub -> // linha de cada item:
			mapOfCheckExtra[sub.id] = remember { mutableStateOf(sub.extra?.paid ?: false) }
			mapOfCheckTable[sub.id] = remember { mutableStateOf(sub.paid) }

			Text(sub.dateStr.substringBeforeLast('/'), Modifier.weight(1.5f), fontWeight = FontWeight.Bold, textAlign = TextAlign.Center)
			Divider(Modifier.wrapContentHeight().weight(.1f), Color.Transparent)

			Text(sub.buyer, Modifier.weight(3.5f), fontWeight = FontWeight.Bold)
			Divider(Modifier.wrapContentHeight().weight(.1f), Color.Transparent)

			Text(sub.id, Modifier.weight(1.6f), colorSwitcher = sub.paid, TextAlign.Center)
			Checkbox(mapOfCheckTable[sub.id]!!.value, {
				onSelectedElement(
					sub.id, // identificador da mesa
					PATHS.TABLE, // categoria
					sub.buyer,   // comprador
					if (sub.walletStr == "") null
					else WALLET.sto(sub.walletStr),  // wallet
					it // check
				)
			}, Modifier.weight(1.6f)) // pagar mesa
			Checkbox(mapOfCheckExtra[sub.id]!!.value, { status ->
				onSelectedElement(
					sub.id,
					PATHS.EXTRA,
					sub.buyer,
					sub.extra?.let { WALLET.sto(it.wallet) },
					status
				)
			}, Modifier.weight(1.6f), enabled = sub.extra != null) // pagar extra
		})
}

@Composable
private fun Text(text: String, modifier: Modifier, colorSwitcher: Boolean, align: TextAlign) {
	Text(text, modifier, if (colorSwitcher) MyColors.SeatPayYES else MyColors.SeatPayNO, fontWeight = FontWeight.Bold, textAlign = align)
}

@Composable
internal fun ParkReservePaymentMiniList(
	uiState: GainsUiState.Loaded,
	onSelectedElement: (String, PATHS, String, WALLET?, Boolean) -> Unit,
	onPayClick: (TableObject, PATHS, WALLET, Boolean) -> Unit,
	onHideDialogs: () -> Unit,
	modifier: Modifier = Modifier.fillMaxSize()
) {
	val mapOfCheckPark = HashMap<String, MutableState<Boolean>>()

	ScrollComponent(uiState,
		uiState.parkReservationMiniList.sortedWith(compare({ it.name })),
		onPayClick, onHideDialogs,
		modifier = modifier,
		checkParkStatus = mapOfCheckPark,
		headerLayout = {
			Text("NOME", Modifier.weight(3.5f).padding(start = 20.dp), MyColors.TextNumber, fontWeight = FontWeight.Bold)
			Text("PAGO", Modifier.weight(1.6f), MyColors.TextNumber, fontWeight = FontWeight.Bold, textAlign = TextAlign.Center)
			Spacer(Modifier.weight(4.9f, true))
		},
		lineLayout = { item ->
			mapOfCheckPark[item.name] = remember { mutableStateOf(item.parking!!.paid) }

			Text(item.name, Modifier.weight(3.5f).padding(start = 20.dp), fontWeight = FontWeight.Bold)
			Checkbox(
				checked = mapOfCheckPark[item.name]!!.value,
				onCheckedChange = { status ->
					onSelectedElement(
						"0", // mesa alvo neutra
						PATHS.PARK,
						item.name, // vagas são pagas dentro do nó "reserva_comprador"
						item.parking?.let { WALLET.sto(it.wallet) },
						status
					)
				},
				Modifier.weight(1.6f)
			)
			Spacer(Modifier.weight(4.9f, true))
		}
	)
}

@Composable
private fun <T> ScrollComponent(
	uiState: GainsUiState.Loaded,
	elements: List<T>,
	onPayClick: (TableObject, PATHS, WALLET, Boolean) -> Unit,
	onHideDialogs: OnDismiss,
	checkExtraStatus: Map<String, MutableState<Boolean>> = HashMap(),
	checkParkStatus: Map<String, MutableState<Boolean>> = HashMap(),
	checkTableStatus: Map<String, MutableState<Boolean>> = HashMap(),
	headerLayout: @Composable RowScope.() -> Unit,
	lineLayout: @Composable RowScope.(T) -> Unit,
	modifier: Modifier
) {
	Box(modifier) {
		Column {
			Row(Modifier.fillMaxWidth().padding(top = 8.dp), content = headerLayout)
			LazyColumn(Modifier.fillMaxSize()) {
				items(elements, itemContent = {
					Row(Modifier.fillMaxWidth().height(30.dp).padding(top = 8.dp), verticalAlignment = Alignment.CenterVertically, content = { lineLayout(it) })
					Spacer(Modifier.height(5.dp))
					Canvas(Modifier.fillMaxWidth().height((1.5).dp)) {
						drawLine(
							Color.Black, Offset(0f, 0f), Offset(size.width, 0f),
							pathEffect = PathEffect.dashPathEffect(floatArrayOf(10f, 10f), 0f)
						)
					}
				})
			}
		}

		if (uiState.openChooseDial) {
			DialogBuilder.DialogChooseWallet(onHideDialogs) { wallet ->
				val table = uiState.selectedTable ?: TableObject(buyer = uiState.targetOwner)
				onPayClick(table, uiState.targetPath, wallet, true)
				when(uiState.targetPath) {
					PATHS.EXTRA -> checkExtraStatus[table.id]!!.value = true
					PATHS.PARK -> checkParkStatus[table.buyer]!!.value = true
					PATHS.TABLE -> checkTableStatus[table.id]!!.value = true
				}
				onHideDialogs()
			}
		}

		if (uiState.openRefundDial) {
			val targetWallet = uiState.targetWallet!!
			DialogBuilder.DialogRefundWallet(walletRes[targetWallet.ordinal], onDismiss = onHideDialogs ) {
				val table = uiState.selectedTable ?: TableObject(buyer = uiState.targetOwner)
				onPayClick(table, uiState.targetPath, targetWallet, false)
				when(uiState.targetPath) {
					PATHS.EXTRA -> checkExtraStatus[table.id]!!.value = false
					PATHS.PARK -> checkParkStatus[table.buyer]!!.value = false
					PATHS.TABLE -> checkTableStatus[table.id]!!.value = false
				}
				onHideDialogs()
			}
		}
	}
}