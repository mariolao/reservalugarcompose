package br.com.mario.reservalugar.ui.resume

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

/**
 * Gerencia o estado da tela de Resumo, que controla as guias,
 * selecionando a tela correspondente e perpetuando a escolha
 *
 * @author Mário Henrique
 * @since 07/05/2022
 */
class ResumeViewModel : ViewModel() {
	private val _tabSelected = MutableStateFlow(1)
	val tabIndex : StateFlow<Int>
		get() = _tabSelected

	fun setIndex(index: Int) { _tabSelected.value = index }

	companion object {
		fun provideFactory() = object : ViewModelProvider.Factory {
			override fun <T : ViewModel> create(modelClass: Class<T>) = ResumeViewModel() as T
		}
	}
}