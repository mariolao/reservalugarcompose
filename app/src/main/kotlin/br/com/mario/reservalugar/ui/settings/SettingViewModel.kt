/**
 * ViewModel da tela de Configurações, resposável por outras operações e alterar dados da aplicação
 *
 * @author Mário Henrique
 * Created on 10/05/2022
 * Lst mod on 30/06/2022
 */
package br.com.mario.reservalugar.ui.settings

import android.content.Context
import android.content.Intent
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import br.com.mario.reservalugar.R
import br.com.mario.reservalugar.TITLE_SHARE_PDF
import br.com.mario.reservalugar.data.BuyerRepository
import br.com.mario.reservalugar.data.ResultWrapper
import br.com.mario.reservalugar.data.TableRepository
import br.com.mario.reservalugar.model.BuyerObject
import br.com.mario.reservalugar.model.TableObject
import br.com.mario.reservalugar.model.UserObject
import br.com.mario.reservalugar.userManager

import com.google.firebase.auth.FirebaseUser

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update

import kotlinx.coroutines.launch
import java.io.File

/* Created on 10/05/2022 */
class SettingViewModel(buyerRepo: BuyerRepository, tableRepo: TableRepository, private val settingDS: SettingDataStore) : ViewModel() {
	private val _testBDSwitcher = MutableStateFlow(false)
	private val viewModelState = MutableStateFlow(SettingViewModelState())

	val uiState = viewModelState.stateIn(viewModelScope, SharingStarted.Eagerly, viewModelState.value)

	val testBD : ReturnBD
		get() = ReturnBD(_testBDSwitcher) { _testBDSwitcher.value = it }

	init {
		rescueLocalCurrentUser()

		viewModelScope.launch {
			settingDS.testBDFlagFlow.collect { flag ->
				viewModelState.update { it.copy(testDBFlag = flag) }
			}
		}
		viewModelScope.launch {
			buyerRepo.observeBuyerReserves().collect { map ->
				viewModelState.update { it.copy(buyerReservationMap = map) }
			}
		}
		viewModelScope.launch {
			tableRepo.observeTableReserves().collect { list ->
				viewModelState.update { it.copy(reservationList = list) }
			}
		}
		viewModelScope.launch {
			userManager.userMapFlow.collect { map ->
				viewModelState.update { it.copy(currentUser = userManager.currentUser, userMap = map) }
			}
		}
		viewModelScope.launch {
			userManager.localCurrentUserFlow.collect { user ->
				viewModelState.update { it.copy() }
			}
		}
	}

	fun rescueLocalCurrentUser() {
		viewModelScope.launch {
			when (userManager.localCurrentUser) {
				is ResultWrapper.Success -> {
					viewModelState.update { it.copy() }
				}
				is ResultWrapper.Error -> {}
			}
		}
	}

	fun confirmFlagChange(block: () -> Unit) {
		viewModelScope.launch {
			val flag = viewModelState.value.testDBFlag
			settingDS.setFlag(flag)
			delay(1000)
			block()
		}
	}
	/** @since Beta 1.5.6.12 */
	fun shareFile(shareFile: File, ctx: Context) {
		val contentUri = FileProvider.getUriForFile(ctx, ctx.getString(R.string.authority), shareFile)
		Intent().apply {
			action = Intent.ACTION_SEND
			setDataAndType(contentUri, ctx.contentResolver.getType(contentUri))
			putExtra(Intent.EXTRA_STREAM, contentUri)
			type = "application/pdf"

			ctx.startActivity(Intent.createChooser(this, TITLE_SHARE_PDF))
		}
	}

	fun switchBDFlag(value: Boolean, open: Boolean) =
		viewModelState.update { it.copy(testDBFlag = value, openAlertChangeFlag = open) }
}

data class ReturnBD(val state: StateFlow<Boolean>, val update: (Boolean) -> Unit) {
	operator fun invoke() = state
}

/* Created on 11/05/2022 */
data class SettingViewModelState(
	val currentUser: FirebaseUser? = null,
	val userMap: Map<String, UserObject> = emptyMap(),
	val testDBFlag: Boolean = false,
	val openAlertChangeFlag: Boolean = false,
	val reservationList: List<TableObject>? = null,
	val buyerReservationMap: Map<String, BuyerObject>? = null
) {
	val parkingList: List<BuyerObject>
		get() = buyerReservationMap?.run { this.values.toList().filter { it.hasParkLot } } ?: emptyList()
	val tableReservationList: List<TableObject>
		get() = reservationList?.run { filter { it.hasOwner } } ?: emptyList()
	val localUser: UserObject
		get() = userMap[currentUser!!.uid]!!
}