package br.com.mario.reservalugar.ui.tablemap

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.animateZoomBy
import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.gestures.detectTransformGestures
import androidx.compose.foundation.gestures.rememberTransformableState
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.verticalScroll

import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.EventSeat

import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

import androidx.constraintlayout.compose.ConstraintLayout

import br.com.mario.reservalugar.R
import br.com.mario.reservalugar.TODAY
import br.com.mario.reservalugar.leftZero

import br.com.mario.reservalugar.ui.theme.MyColors.ButtonStatusOFF
import br.com.mario.reservalugar.ui.theme.MyColors.ButtonStatusON
import br.com.mario.reservalugar.ui.theme.MyColors.SeatPayNO
import br.com.mario.reservalugar.ui.theme.MyColors.SeatPayYES

import br.com.mario.reservalugar.ui.widgets.MultiFabBuilder.FabOptions
import br.com.mario.reservalugar.ui.widgets.MultiFabBuilder.FabState.COLLAPSED
import br.com.mario.reservalugar.ui.widgets.MultiFabBuilder.MultiFAB
import br.com.mario.reservalugar.ui.widgets.MultiFabBuilder.MultiFabItem
import br.com.mario.reservalugar.ui.widgets.dialogs.AlertExclusion
import br.com.mario.reservalugar.ui.widgets.dialogs.AlertReserveInfo
import br.com.mario.reservalugar.ui.widgets.dialogs.DialogBuilder.DialogChangeOwnership
import br.com.mario.reservalugar.ui.widgets.dialogs.DialogBuilder.DialogReserveOne
import br.com.mario.reservalugar.ui.widgets.dialogs.DialogBuilder.DialogReserveVarious
import br.com.mario.reservalugar.ui.widgets.dialogs.DialogBuilder.DialogTableDetails

import br.com.mario.reservalugar.utils.LockScreenOrientation
import br.com.mario.reservalugar.utils.ScreenshotBox
import br.com.mario.reservalugar.utils.rememberScreenshotState

import kotlinx.coroutines.launch

/**
 * Mostra a estrutura de mesas na área, permitindo fazer a reserva "geograficamente"
 *
 * Created on 27/10/2021
 * Lst mod on 30/06/2022
 */
@SuppressLint("StaticFieldLeak")

@Composable
fun TableScreen(bookingVM: BookingViewModel) {
	LockScreenOrientation(orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) // não permite modo horizontal

	val uiState by bookingVM.uiState.collectAsState()
	/*tableButtonState = when(uiState) { // gerenciar estado de cada mesa (testando)
		is TableUiState.Loaded -> { uiState.tableList }
		is TableUiState.NotLoaded -> { emptyList() }
	}.associate { table ->
		key(table.id.toInt()) {
			table.id.toInt() to remember { mutableStateOf(table.hasOwner) } // tem dono ou não
		}
	}*/

	TableScreenContent(bookingVM, uiState)
}

@Composable
private fun TableScreenContent(bookingVM: BookingViewModel, uiState: TableUiState) {
	val screenshotState = rememberScreenshotState()

	Box(Modifier.fillMaxSize()) {
		var scale by remember { mutableStateOf(1f) }
		var offX by remember { mutableStateOf(0F) }
		var offY by remember { mutableStateOf(0F) }
		val scope = rememberCoroutineScope()
		val state = rememberTransformableState { zoomChange, offsetChange, _ ->
			scale *= zoomChange
			offX += offsetChange.x
			offY += offsetChange.y
		}
		val baseSize = 15.dp
		val areaMargin = baseSize * 2

		ScreenshotBox(screenshotState = screenshotState) {
			ConstraintLayout(
				Modifier
					.wrapContentSize().align(Alignment.TopStart).background(Color.White)
					.padding(10.dp)
					.horizontalScroll(rememberScrollState(0))
					.verticalScroll(rememberScrollState(0))
					.border((1.7).dp, Color.Black, CutCornerShape(2.dp))
					.padding(areaMargin)
					.graphicsLayer { scaleX = scale; scaleY = scale; translationX = offX; translationY = offY }
					.pointerInput(Unit) {
						detectTapGestures(
							onDoubleTap = {
								if (scale < .98f || scale > 1.1f)
									scope.launch { state.animateZoomBy(2f) }
								else
									scope.launch { state.animateZoomBy(1 / 2f) }
							}
						)
					} // tap gestures
					.pointerInput(Unit) {
						detectTransformGestures { _, _, zoom, _ ->
							scale = when {
								scale < .5f -> .5f
								scale > 1.5f -> 1.5f
								else -> scale * zoom
							}
						}
					} // transform gesture
					.pointerInput(Unit) {
						detectDragGestures { change, dragAmount ->
							change.consumeAllChanges()
							offX = (offX + dragAmount.x).coerceIn(-500F, 0F)
							offY = (offY + dragAmount.y).coerceIn(-1550f, 0f) // mapa muito grande verticalmente
						}
					} // drag gestures
			) {
				val (stage, danceFloor, cmp_01, cmp_02) = createRefs()
				val area01Wdt = 95.dp

				Box(
					Modifier.size((baseSize * 22.66f), (baseSize * 6)).border(1.dp, Color.Red).constrainAs(stage) {
						top.linkTo(parent.top, 2.dp) // apenas para aparecer a linha no topo
						start.linkTo(parent.start, (areaMargin + area01Wdt))
					},
					contentAlignment = Alignment.Center,
					content = { Text("PALCO", fontSize = 40.sp) }
				) // palco

				Box(
					Modifier.size((baseSize * 22.66f), (baseSize * 16)).border(1.dp, Color.Black)
						.constrainAs(danceFloor) {
							top.linkTo(stage.bottom)
							start.linkTo(parent.start, (areaMargin + area01Wdt))
						},
					contentAlignment = Alignment.Center,
					content = { Text("ÁREA DE\n DANÇA", fontSize = 40.sp) }
				) // área de dança

				Area01Content(Modifier.constrainAs(cmp_01) { // size: 95w / 495h
					top.linkTo(danceFloor.bottom, 3.dp)
					start.linkTo(parent.start, areaMargin)
					end.linkTo(danceFloor.start, areaMargin * 2)
				}, uiState, bookingVM::selectTable)

				Area02Content(Modifier.constrainAs(cmp_02) { // size: 340w /
					top.linkTo(danceFloor.bottom, 3.dp)
					start.linkTo(danceFloor.start)
				}, uiState, bookingVM::selectTable)
			}
		}

		if (uiState is TableUiState.Loaded) {
			if (uiState.openDialogOne) {
				DialogReserveOne(
					uiState.selectedTable?.id!!.toInt(),
					onDismiss = { bookingVM.closeAllDialogs() },
					onPositiveClick = { owner ->
						bookingVM.addTableReserve(owner.trim(), uiState.selectedTable.id, TODAY)
						bookingVM.closeAllDialogs()
					}
				)
			}
			if (uiState.openDialogVarious) {//todo finalizar
				DialogReserveVarious(
					onDismiss = bookingVM::showMultipleReservation,
					onPositiveClick = { inputTables, owner ->
						bookingVM.showMultipleReservation()
						bookingVM.addTablesReserve(inputTables.replace("\\s".toRegex(), ""), owner.trim(), TODAY)
					})
			}
			if (uiState.openDetail) {
				DialogTableDetails(
					result = uiState.selectedTable!! to uiState.selectedBuyer!!,
					onBookParkLot = bookingVM::manageParkReserve,
					onBookExtra = bookingVM::manageExtraReserve,
					onChangeOwner = bookingVM::showChangeOwner,
					onDismiss = bookingVM::closeAllDialogs,
					onUnbookSeat = { table, buyer ->
						bookingVM.prepareToRemoveTableReservation(table, buyer)
						bookingVM.closeAllDialogs()
					})
			}
			if (uiState.openDialogChangeOwner) {
				DialogChangeOwnership(
					pair = uiState.selectedTable!! to uiState.selectedBuyer!!,
					onDismiss = bookingVM::closeAllDialogs,
					onChangeOwner = bookingVM::changeOwner
				)
			}
			if (uiState.openTablesReservationWarning) {
				AlertReserveInfo(
					title = "Reserva não concluída",
					message = uiState.msgBody,
					onDismiss = bookingVM::closeAllAlerts,
					onPositive = {
						bookingVM.closeAllAlerts()
						bookingVM.openAlert(false)
					})
			}
			if (uiState.openTableReservationExclusion) {
				AlertExclusion(
					title = "Exclusão de reserva",
					msg = uiState.msgBody,
					onDismiss = bookingVM::closeAllAlerts,
					onPositive = {
						bookingVM.closeAllAlerts()
						bookingVM.removeTableReserve(uiState.selectedTable!!, uiState.selectedBuyer!!)
					})
			}
		}

		val ctx = LocalContext.current
		var toState by remember { mutableStateOf(COLLAPSED) }
		MultiFAB(
			Icons.Filled.Add,
			listOf(
				MultiFabItem(FabOptions.MULTIBOOK, Icons.Filled.EventSeat, "reservar várias mesas"),
				MultiFabItem(FabOptions.SCREENSHOT, ImageVector.vectorResource(R.drawable.ic_wallpaper_24dp), "captura de tela")
			),
			modifier = Modifier.padding(bottom = 15.dp, end = 15.dp).align(Alignment.BottomEnd),
			toState = toState, stateChanged = { toState = it }) {
			when (it.identifier) {
				FabOptions.MULTIBOOK -> bookingVM.showMultipleReservation(true)
				FabOptions.SCREENSHOT -> {
					scale = .5f
					offX = -200f
					offY = -750f
					bookingVM.makeScreenshot(ctx, screenshotState)
				}
			}
		}
	}
}

/** área logo ao lado do palco (mesas 109 a 120) */
@Composable
private fun Area01Content(modifier: Modifier, uiState: TableUiState, onButtonTapped: (Boolean, Int) -> Unit) {
	Column(modifier) {
		for (cl in 109..119 step 2) {
			Row(Modifier.padding(bottom = 5.dp)) {
				ButtonTable("$cl", uiState, Modifier.padding(end = 5.dp), onButtonTapped = onButtonTapped)
				ButtonTable("${cl + 1}", uiState, onButtonTapped = onButtonTapped)
			}
		}
	}
} // Area01

/** área mais à direita (mesas 001 a 108) */
@Composable
private fun Area02Content(modifier: Modifier, uiState: TableUiState, onButtonTapped: (Boolean, Int) -> Unit) {
	Column(modifier) {
		for (cl in 0..17) { // 18 colunas de mesas
			Row(Modifier.padding(bottom = 5.dp)) {
				ButtonTable(((6 * cl) + 1).leftZero(2), uiState, onButtonTapped = onButtonTapped)
				ButtonTable(((6 * cl) + 2).leftZero(2), uiState, Modifier.padding(horizontal = 5.dp), onButtonTapped = onButtonTapped)
				ButtonTable(((6 * cl) + 3).leftZero(2), uiState, onButtonTapped = onButtonTapped)

				Spacer(Modifier.width(50.dp))

				ButtonTable(((6 * cl) + 4).leftZero(2), uiState, onButtonTapped = onButtonTapped)
				ButtonTable(((6 * cl) + 5).leftZero(2), uiState, Modifier.padding(horizontal = 5.dp), onButtonTapped = onButtonTapped)
				ButtonTable(((6 * cl) + 6).leftZero(2), uiState, onButtonTapped = onButtonTapped)
			}
		}
	}
} // Area02

@Composable
private fun ButtonTable(
	label: String,
	uiState: TableUiState,
	modifier: Modifier = Modifier,
	sz: Dp = 45.dp,
	onButtonTapped: (hasOwner: Boolean, tableIdx: Int) -> Unit,
	outContent: (@Composable RowScope.() -> Unit)? = null,
) {
	val tableIdx = label.toInt()
	val presence = uiState.tableList.getOrNull(tableIdx)?.hasOwner ?: false
	Button(
		modifier = modifier.size(sz),
		onClick = { onButtonTapped(presence, tableIdx) },
		colors = ButtonDefaults.buttonColors(if (presence) ButtonStatusON else ButtonStatusOFF),
		border = BorderStroke((1.5).dp,
			if (uiState.tableList[tableIdx].paid) SeatPayYES else SeatPayNO
		),
		contentPadding = PaddingValues(2.dp), // para tirar a restrição de espaço do texto
		content = outContent ?: { Text(label, fontSize = if (tableIdx > 99) 10.sp else TextUnit.Unspecified) }
	)
}

@Preview
@Composable
private fun PreviewArea01() {
	Area01Content(modifier = Modifier,
		uiState = TableUiState.NotLoaded(isLoading = false, errorMessages = listOf("error")),
		onButtonTapped = { _, _ -> })
}