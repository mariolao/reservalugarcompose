package br.com.mario.reservalugar.data

import br.com.mario.reservalugar.CostsDB
import br.com.mario.reservalugar.model.CostObject

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * @author Mário Henrique
 * Created on 25/04/2022
 * Lst mod on 05/05/2022
 */
class CostRepository {
	private val costs = MutableStateFlow<List<CostObject>>(emptyList())
	private val costMap = HashMap<String, CostObject>()

	init {
		@Suppress("UNCHECKED_CAST")
		(CostsDB.load { data ->
			try {
				(data as HashMap<String, *>).forEach { (key, cost) -> costMap[key] = CostObject.fromMap(key, cost) }
				updateList()

			} catch (e: ClassCastException) {
				if (data is ArrayList<*>) data.forEach { i -> throw Exception("ArrayList item: $i") }
			}
		})
		CostsDB.change {
			onChildAdded { data, oldName ->
				costMap[data.key!!] = CostObject.fromMap(data.key!!, data.value as HashMap<*, *>)
				updateList()
			}
			onChildRemoved { costMap.remove(it!!.key); updateList() }
		}
	}

	fun observeCostList(): Flow<List<CostObject>> = costs

	private fun updateList() {
		costs.value = costMap.values.toList()
	}

	fun addCost(cost: CostObject) = CostsDB.addItem(cost)
	fun delCost(costId: String) = CostsDB.delItem(costId)

	fun getCostList() : ResultWrapper<List<CostObject>> = //fixme: adicionar carregamento de erro do BD
		/*if (costs.value.isNotEmpty())*/ ResultWrapper.Success(costs.value)
//		else ResultWrapper.Error("Lista não carregada")
}