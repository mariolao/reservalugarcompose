package br.com.mario.reservalugar.data

/**
 * Baseado no resultado criado no app JetNews da Google, com pequenas adaptações
 * Created on 09/04/2022
 */
sealed class ResultWrapper<out R>(val data: R? = null, val message: String? = null) {
	class Success<out T>(data: T) : ResultWrapper<T>(data)
	class Error<out T>(message: String, data: T? = null) : ResultWrapper<T>(data, message)
}

fun <T> ResultWrapper<T>.successOr(fallback: T) : T = (this as? ResultWrapper.Success<T>)?.data ?: fallback