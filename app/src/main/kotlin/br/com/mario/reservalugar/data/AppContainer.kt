package br.com.mario.reservalugar.data

import android.app.Application

/**
 * Gerencia Injeção de Dependência dos Repositórios a nível de Aplicação
 *
 * Created on 11/04/2022
 * Lst mod on 02/05/2022
 */

interface AppContainer {
	val costRepository: CostRepository
	val buyerRepository: BuyerRepository
	val reportRepository: ReportRepository
	val tableRepository: TableRepository
	val valuesRepository: ValuesRepository
	val app: Application
}

class AppConteinerImpl(val application: Application) : AppContainer {
	override val buyerRepository: BuyerRepository by lazy {
		BuyerRepository()
	}
	override val costRepository: CostRepository by lazy { CostRepository() }
	override val reportRepository by lazy { ReportRepository() }
	override val tableRepository: TableRepository by lazy {
		TableRepository()
	}
	override val valuesRepository: ValuesRepository by lazy {
		ValuesRepository()
	}
	override val app: Application = application
}