package br.com.mario.reservalugar

import android.content.Context
import android.content.Intent

import androidx.compose.ui.graphics.Color
import androidx.core.graphics.toColorInt

import br.com.mario.reservalugar.data.UserFirebaseManager

import com.pdfjet.Cell

import org.joda.time.DateTimeZone
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

/**
 * Arquivo que contém funções e recursos auxiliares ao funcionamento da aplicação
 *
 * @author Mário Henrique
 * Created on 23/04/2018
 * Lst Mod on 28/06/2022
 */

// GLOBAL CONSTANTS
const val TITLE_SHARE_IMG = "Compartilhar mapa atual"
const val IMG_FOLDER = "images"
const val IMG_FILE = "mapa_mesas.png"
/** WORK WITH PDF Jet */
const val TITLE_SHARE_PDF = "Compartilhar arquivo"
const val PDF_FILE_01 = "relatorio_mesas.pdf"
const val PDF_FILE_02 = "relatorio_vagas.pdf"
const val MAX_ROWS = 60

/** Pega o valor inteiro do recurso de carteira correspondente */
val walletRes = listOf(R.drawable.icon_bb, R.drawable.icon_wallet)

val TODAY: String = (LocalDate(DateTimeZone.forID("America/Fortaleza")).toString(DateTimeFormat.forPattern("dd/MM/yyyy")))

val userManager: UserFirebaseManager by lazy { UserFirebaseManager() }

// EXTENSIONS FUNCTIONS
internal fun Double.monetary(digits: Int = 2) = "R$ %2.${digits}f".format(this)
//internal fun Float.monetary(digits: Int) = "R$ %2.${digits}f".format(this)
internal fun Int.leftZero(space: Int): String = ("%0${space}d".format(this))

fun String.shrink(limit: Int) = if (length > limit) substring(0 until limit) else this
val String.color: Color get() =  Color(this.toColorInt())

/** Created on 07/03/2022 - convertendo com teste de tipo */
internal fun HashMap<*, *>.doubling(key: String): Double = this[key].let { if (it is Double) it else (it as Long).toDouble() }

/**
 * Força o reset completo da aplicação
 * @since 11/05/2022
 * @see https://stackoverflow.com/a/46848226/3443949
 */
fun Context.relaunchApp() {
	var resetAppIntent = this.packageManager.getLaunchIntentForPackage(this.packageName)
	resetAppIntent = resetAppIntent?.run {
		Intent.makeRestartActivityTask(component)
	}
	this.startActivity(resetAppIntent)
	Runtime.getRuntime().exit(0)
}

// GLOBAL FUNCTIONS
// http://kotlination.com/kotlin/kotlin-sort-list-of-objects-with-comparator-example
fun <T> compare(vararg selectors: (T) -> Comparable<*>?): Comparator<T> =
	(Comparator { a, b -> compareValuesBy(a, b, *selectors) })

@Suppress("FunctionName")
fun Table(data: ArrayList<List<Cell>>) = com.pdfjet.Table().apply { setData(data, com.pdfjet.Table.DATA_HAS_1_HEADER_ROWS) }