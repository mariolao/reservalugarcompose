package br.com.mario.reservalugar.ui

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost

@Composable
fun ReservaLugarNavGraph(
	isExpandedScreen: Boolean,
	modifier: Modifier = Modifier,
	navController: NavHostController,
	startDestination: String,
) {
	NavHost(navController = navController, startDestination = startDestination, modifier = modifier) {

	}
}