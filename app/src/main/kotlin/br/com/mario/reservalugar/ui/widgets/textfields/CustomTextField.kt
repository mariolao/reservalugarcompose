package br.com.mario.reservalugar.ui.widgets.textfields

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardOptions

import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged

import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

import br.com.mario.reservalugar.shrink
import br.com.mario.reservalugar.ui.theme.MyColors

import kotlin.reflect.KFunction1

/**
 * Created on 18/12/2021
 * Lst mod on 12/05/2022
 */

data class InputWrapper(val value: String = "", val errorId: Int?) {
	constructor(_value: String) : this(_value, null)
	constructor(_value: String, _errorFunc: KFunction1<String, Int?>) : this(_value, _errorFunc(_value))
}

/*@Composable
fun TextFieldError(input: InputWrapper, placeholder: String, type: KeyboardType, visual: VisualTransformation,
                   format: (String) -> String,callback: NotBlankCallback, onValueChange: OnValueChange, *//*onImeKeyAction: OnImeKeyAction*//*
) {
	Column {
		TextField( // Trocar por BasicTextField
			value = input.value,
			onValueChange = {
				it.shrink(8)
				onValueChange(it)
				callback(format(it), it.isNotBlank())
			},
			placeholder = { Text(placeholder, color = Color.Gray, fontSize = 16.sp) },
			isError = input.errorId != null, // apenas muda o aspecto do TextField
			visualTransformation = visual,
			keyboardOptions = KeyboardOptions(keyboardType = type),
//				keyboardActions = KeyboardActions(onAny = { onImeKeyAction() })
		)
		if (input.errorId != null)
			Text(stringResource(id = input.errorId), color = Color.Red, fontSize = 12.sp, letterSpacing = (1.5).sp)
	}
}*/

@Composable
internal fun TextFieldErrorBasic(modifier: Modifier,
                                 input: InputWrapper, placeholder: String, bgColor: Color, type: KeyboardType, visual: VisualTransformation,
                                 limit: Int = Int.MAX_VALUE, onValueChange: OnValueChange
) {
	Column(modifier) {
		val isError = input.errorId != null

		Box(Modifier.height(35.dp) ) {
			var isFocused by remember { mutableStateOf(false) }

			BasicTextField(
				modifier = Modifier.fillMaxSize().onFocusChanged {
					if (isFocused != it.isFocused) isFocused = it.isFocused
				},
				value = input.value, onValueChange = {
					val shrink = it.shrink(limit)
					onValueChange(shrink)
//					callback(format(shrink), (shrink.isNotBlank() && !isError))
				},
				singleLine = true,
				textStyle = TextStyle(fontSize = 18.sp, textAlign = TextAlign.Center),
				visualTransformation = visual,
				keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Words, keyboardType = type),
				decorationBox = { innerTxt ->
					Box(Modifier.fillMaxWidth().background(bgColor), Alignment.Center) {
						if (input.value.isEmpty())
							Text(placeholder, color = Color.Gray, fontSize = 16.sp)
						innerTxt()
					}
				}
			)

			Divider( // linha que mostra status do foco ou se há erro
				Modifier.height(1.dp).align(Alignment.BottomCenter),
				color = if (isError) MyColors.Holo_RedDragon else if (isFocused) MyColors.Purple500 else Color.Gray
			)
		}
		if (isError)
			Text(stringResource(id = input.errorId!!), color = Color.Red, fontSize = 12.sp, letterSpacing = (1.5).sp)
	}
}

/**
 * Modified TextField to make some configurations more refined
 *
 * @since 18/12/2021
 */
@Composable
fun MyTextField(
	placeholder: String, type: KeyboardType,
	bgColor: Color,
	modifier: Modifier = Modifier,
	textInit: String = "", textColor: Color = Color.Unspecified,
	limit: Int = Int.MAX_VALUE,
	visual: VisualTransformation = VisualTransformation.None,
	callback: (str: String, set: Boolean) -> Unit,
) {
	var text by remember { mutableStateOf(textInit) }
	var isTextNotBlank by remember { mutableStateOf(textInit.isNotBlank()) }
	Box(modifier.height(35.dp)) {

		var isFocused by remember { mutableStateOf(false) }
		BasicTextField(
			modifier = Modifier.fillMaxSize().onFocusChanged {
				if (isFocused != it.isFocused) isFocused = it.isFocused
			},
			value = text, onValueChange = {
				text = (it.shrink(limit))/*, TextRange(it.text.length)*/
				isTextNotBlank = it.isNotBlank()
				callback(it, isTextNotBlank)
			},
			singleLine = true,
			textStyle = TextStyle(color = textColor, fontSize = 18.sp, textAlign = TextAlign.Center),
			keyboardOptions = KeyboardOptions(capitalization = KeyboardCapitalization.Words, keyboardType = type),
			visualTransformation = visual,
			decorationBox = { innerTxt ->
				Box(
					Modifier.fillMaxWidth().background(bgColor),
					contentAlignment = Alignment.Center
				) {
					if (text.isEmpty())
						Text(placeholder, color = Color.Gray, fontSize = 16.sp)
					innerTxt()
				}
			}
		)
		Divider(
			Modifier.height(1.dp).align(Alignment.BottomCenter),
			color = if (isFocused) MyColors.Purple500 else Color.Gray
		)
	}
}

typealias OnValueChange = (value: String) -> Unit
typealias OnImeKeyAction = () -> Unit