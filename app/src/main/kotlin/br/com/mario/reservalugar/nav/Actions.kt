package br.com.mario.reservalugar.nav

import androidx.navigation.NavHostController

class Actions(navHost: NavHostController?) {
	val goToSeats: () -> Unit = { navHost?.navigate(Routes.TablesRoute.route) }
	val goToCosts = navHost?.navigate(Routes.CostsRoute.route)
}