package br.com.mario.reservalugar.ui.widgets.textfields

import br.com.mario.reservalugar.R

/** Create on 18/12/2021 */
object InputValidator {
	fun getDateErrorIdOrNull(input: String): Int? = when(input.length) {
		2 -> if (input.toInt() > 31) R.string.error_day else null
		3 -> if (input[2].digitToInt() > 1) R.string.error_month_first_digit else null
		4 -> if (input.substring(2..3).toInt() > 12) R.string.error_month_second_digit else null
		8 -> if (input.substring(4..7).toInt() !in 2020..2022) R.string.error_year_range else null
		else -> null
	}

	fun getValueErrorOrNull(input: String): Int? = if (input.length > 7) R.string.error_length else null
}