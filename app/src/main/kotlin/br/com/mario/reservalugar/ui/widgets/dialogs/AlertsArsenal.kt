/**
 * Created on 14/12/2021
 * Lst mod on 17/05/2022
 */
package br.com.mario.reservalugar.ui.widgets.dialogs

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions

import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons.Filled
import androidx.compose.material.icons.filled.Create
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.material.icons.filled.VisibilityOff
import androidx.compose.material.icons.filled.Warning

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties

import br.com.mario.reservalugar.ui.theme.MyColors.Holo_Bluedark
import br.com.mario.reservalugar.ui.theme.MyColors.Holo_RedDragon
import br.com.mario.reservalugar.ui.theme.Typography
import br.com.mario.reservalugar.ui.widgets.dialogs.AlertConfig.ChangePass
import br.com.mario.reservalugar.ui.widgets.dialogs.AlertConfig.Danger
import br.com.mario.reservalugar.ui.widgets.dialogs.AlertConfig.Info

sealed class AlertConfig(val icon: ImageVector, val colorBg: Color, val colorFg: Color) {
	object Info: AlertConfig(Filled.Info, Color.DarkGray, Color.White)
	object Danger: AlertConfig(Filled.Warning, Holo_RedDragon, Color.White)
	object ChangePass: AlertConfig(Filled.Create, Holo_Bluedark, Color.White)
}

@Composable
fun AlertReserveInfo(
	title: String,
	message: String,
	info: AlertConfig = Info,
	onDismiss: () -> Unit,
	onPositive: () -> Unit
) {
	Dialog(onDismissRequest = onDismiss) {
		AlertSkeleton(title, message, info) {
			Button(onClick = onPositive,
				Modifier.size(115.dp, 40.dp),
				colors = ButtonDefaults.buttonColors(backgroundColor = info.colorFg),
				shape = RoundedCornerShape(8.dp),
				content = { Text("OK", color = info.colorBg) }
			)
		}
	}
}

@Composable
fun AlertExclusion(
	title: String,
	msg: String,
	delete: AlertConfig = Danger,
	onDismiss: () -> Unit,
	onPositive: () -> Unit
) {
	Dialog(onDismissRequest = onDismiss) {
		AlertSkeleton(title, msg, delete) {
			Row(Modifier.fillMaxWidth(), Arrangement.SpaceEvenly) {
				Button(onClick = onDismiss,
					Modifier.size(115.dp, 40.dp),
					colors = ButtonDefaults.buttonColors(backgroundColor = delete.colorBg),
					shape = RoundedCornerShape(8.dp),
					content = { Text("NÃO", color = delete.colorFg) }
				)
				Button(onClick = onPositive,
					Modifier.size(115.dp, 40.dp),
					colors = ButtonDefaults.buttonColors(backgroundColor = delete.colorFg),
					shape = RoundedCornerShape(8.dp),
					content = { Text("SIM", color = delete.colorBg) }
				)
			}
		}
	}
}

@Composable
fun AlertResetApplication(
	title: String = "Reinicialização necessária",
	message: String = "Para que a opção seja efetivada a aplicação terá que ser reiniciada.\n Prosseguir?",
	info: AlertConfig = Danger,
	onDismiss: () -> Unit,
	onPositive: () -> Unit
) {
	Dialog(onDismissRequest = onDismiss) {
		AlertSkeleton(title, message, info) {
			Row(Modifier.fillMaxWidth(), Arrangement.SpaceEvenly) {
				Button(onClick = onDismiss,
					Modifier.size(115.dp, 40.dp),
					colors = ButtonDefaults.buttonColors(backgroundColor = info.colorBg),
					shape = RoundedCornerShape(8.dp),
					content = { Text("CANCELA", color = info.colorFg) }
				)
				Button(onClick = onPositive,
					colors = ButtonDefaults.buttonColors(backgroundColor = info.colorFg),
					shape = RoundedCornerShape(8.dp),
					content = { Text("CONFIRMA", color = info.colorBg) }
				)
			}
		}
	}
}

/**
 * Created on 16/05/2022
 * Lst mod on 17/05/2022
 * @since 1.5.5.032
 */
@Composable
fun AlertResetPassword(
	alertConfig: AlertConfig = ChangePass,
	onDismiss: () -> Unit,
	onExecute: (oldPass: String, newPass: String) -> Unit
) {
	Dialog(onDismiss, DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false)) {
		var oldPassword by remember { mutableStateOf("") }
		var newPassword by remember { mutableStateOf("") }

		AlertSkeleton(
			title = "Primeiro Acesso",
			body = { 
					 Column(horizontalAlignment = Alignment.CenterHorizontally) {
						 var oldPasswordVisible by remember { mutableStateOf(false) }
						 TextField(
							 value = oldPassword,
							 onValueChange = { oldPassword = it },
							 modifier = Modifier.padding(top = 5.dp),
							 label = { Text("Senha atual") },
							 singleLine = true,
							 placeholder = { Text("digite senha atual") },
							 visualTransformation = if (oldPasswordVisible) VisualTransformation.None else PasswordVisualTransformation(),
							 keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
							 trailingIcon = {
								 val image = if (oldPasswordVisible)
									 Filled.Visibility
								 else Filled.VisibilityOff

								 // Please provide localized description for accessibility services
								 val description = if (oldPasswordVisible) "Esconder senha" else "Mostrar senha"
								 IconButton(onClick = {oldPasswordVisible = !oldPasswordVisible}){
									 Icon(imageVector  = image, description)
								 }
							 }
						 )

						 var newPasswordVisible by remember { mutableStateOf(false) }
						 TextField(
							 value = newPassword,
							 onValueChange = { newPassword = it },
							 modifier = Modifier.padding(top = 10.dp),
							 label = { Text("Nova senha") },
							 singleLine = true,
							 placeholder = { Text("digite uma nova senha") },
							 visualTransformation = if (newPasswordVisible) VisualTransformation.None else PasswordVisualTransformation(),
							 keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
							 trailingIcon = {
								 val image = if (newPasswordVisible)
									 Filled.Visibility
								 else Filled.VisibilityOff

								 // Please provide localized description for accessibility services
								 val description = if (newPasswordVisible) "Esconder senha" else "Mostrar senha"
								 IconButton(onClick = { newPasswordVisible = !newPasswordVisible }) {
									 Icon(imageVector = image, description)
								 }
							 }
						 )

						 var repeatPassword by remember { mutableStateOf("") }
						 var repeatPasswordVisible by remember { mutableStateOf(false) }
						 TextField( // todo adicinoar alerta de senhas diferentes
							 value = repeatPassword,
							 onValueChange = { repeatPassword = it },
							 modifier = Modifier.padding(top = 10.dp),
							 label = { Text("Confirme senha") },
							 singleLine = true,
							 placeholder = { Text("digite novamente a senha") },
							 visualTransformation = if (repeatPasswordVisible) VisualTransformation.None else PasswordVisualTransformation(),
							 keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Password),
							 trailingIcon = {
								 val image = if (repeatPasswordVisible)
									 Filled.Visibility
								 else Filled.VisibilityOff

								 // Please provide localized description for accessibility services
								 val description = if (repeatPasswordVisible) "Esconder senha" else "Mostrar senha"
								 IconButton(onClick = {repeatPasswordVisible = !repeatPasswordVisible}){
									 Icon(imageVector  = image, description)
								 }
							 }
						 )
					 }
			},
			config = alertConfig,
			buttons = {
				Spacer(Modifier.height(10.dp))
				Button(onClick = { onExecute(oldPassword, newPassword) },
					Modifier.size(115.dp, 40.dp),
					colors = ButtonDefaults.buttonColors(backgroundColor = alertConfig.colorFg),
					shape = RoundedCornerShape(8.dp),
					content = { Text("OK", color = alertConfig.colorBg) }
				)
			}
		)
	}
}

/**
 * Estrutura base para criar o layout do Alerta.
 * @since 14/12/2021
 */
@Composable
fun AlertSkeleton(title: String, message: String, config: AlertConfig, buttons: @Composable ColumnScope.() -> Unit) {
	Card(Modifier.width(300.dp), RoundedCornerShape(10.dp), config.colorBg) {
		Column(Modifier.padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally) {
			// cabeçalho do alerta
			Row(Modifier.wrapContentWidth(), Arrangement.SpaceEvenly, Alignment.CenterVertically) {
				Icon(config.icon, contentDescription = "", tint = Color.White)
				Spacer(Modifier.width(10.dp))
				Text(title, style = Typography.h6, color = Color.White)
			}

			Text(message, Modifier.padding(vertical = 20.dp), config.colorFg, textAlign = TextAlign.Center)

			buttons()
		}
	}
}

@Composable
fun AlertSkeleton(title: String, body: @Composable () -> Unit, config: AlertConfig, buttons: @Composable ColumnScope.() -> Unit) {
	Card(shape = RoundedCornerShape(10.dp), backgroundColor = config.colorBg) {
		Column(Modifier.padding(10.dp), horizontalAlignment = Alignment.CenterHorizontally) {
			// cabeçalho do alerta
			Row(Modifier.wrapContentWidth(), Arrangement.SpaceEvenly, Alignment.CenterVertically) {
				Icon(config.icon, contentDescription = "", tint = Color.White)
				Spacer(Modifier.width(10.dp))
				Text(title, style = Typography.h6, color = Color.White)
			}

			body()
			buttons()
		}
	}
}

@Preview
@Composable
fun PreviewAlertInfo() =
	AlertSkeleton(title = "Título", message = "Corpo do alerta", config = Info) {
		Text("área do botão")
	}

@Preview(name = "Alerta Vermelho")
@Composable
private fun PreviewExclusion() {
	AlertSkeleton(title = "Exclusão de reserva", message = "mensagem de alerta", Danger) {}
}