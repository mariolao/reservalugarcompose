package br.com.mario.reservalugar.ui

object ReservaLugarDestinations {
	const val TABLE_ROUTE = "tables"
	const val GAINS_ROUTE = "gains"
	const val COSTS_ROUTE = "costs"
	const val REPORT_ROUTE = "reports"
}