package br.com.mario.reservalugar.data

import br.com.mario.reservalugar.PaymentDB
import br.com.mario.reservalugar.ReserveDB
import br.com.mario.reservalugar.model.BuyerObject
import br.com.mario.reservalugar.model.ParkingModel

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import java.util.HashMap

/**
 *  Created on 11/04/2022
 *  Lst mod on 29/06/2022
 */
class BuyerRepository {
	private val donos = MutableStateFlow<Map<String, BuyerObject>>(hashMapOf())

	init {
		ReserveDB.loadBuyerReserves {
			@Suppress("UNCHECKED_CAST")
			val map = it.mapValues { (_, item) -> BuyerObject.fromMap(item as Any) }
			donos.value = map
		}
		ReserveDB.changeBuyerReserve {
			onChildAdded { data, _ -> setOfflineBuyer(data.key!!, data.value!!) }
			onChildChanged { data, _ -> setOfflineBuyer(data.key!!, data.value!!) } // por conta do nó de Estacionamento
		}
	}

	fun observeBuyerReserves(): Flow<Map<String, BuyerObject>> = donos

	fun includeOwner(buyerReserve: BuyerObject) {
		ReserveDB.setBuyerReserve(buyerReserve.name, buyerReserve)
	}
	fun excludeOwner(buyer: BuyerObject) = ReserveDB.setBuyerReserve(buyer.name, null)
	fun getBuyerReserves() =
		if (donos.value.isNotEmpty()) ResultWrapper.Success(donos.value)
		else ResultWrapper.Error("Mapeamento não carregado ou não existe")

	fun getBuyerReserve(ownerName: String) = donos.value.get(ownerName)

	fun attachParkingToBuyer(name: String, date: String) =
		ReserveDB.setParkReserve(name, ParkingModel(date))
	fun detachParkingToBuyer(name: String) =
		ReserveDB.setParkReserve(name, null)
	fun manageParkingPayment(buyerTag: String, walletStr: String, paid: Boolean) =
		PaymentDB.setParkStatus(buyerTag, ParkingModel.toPayMap(walletStr, paid))

	fun setOfflineBuyer(key: String, value: Any) {
		val temp = donos.value.toMutableMap()
		if (value is HashMap<*, *>) temp[key] = BuyerObject.fromMap(value)
		else temp[key] = (value as BuyerObject)
		donos.value = temp
	}
}