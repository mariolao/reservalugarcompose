/**
 * Classe para encapsular o objeto Gastos
 *
 * @author Mário Henrique
 * Created on 01/06/2020
 * Lst mod on 29/04/2022
 */
package br.com.mario.reservalugar.model

import br.com.mario.reservalugar.doubling

import com.google.firebase.database.Exclude
import com.google.firebase.database.PropertyName

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

private const val DATE_TAG = "data"
private const val DESC_TAG = "descricao"
private const val WALLET_TAG = "carteira"
private const val VAL_TAG = "valor"

@Suppress("PropertyName")
data class CostObject(
	@get:PropertyName(DATE_TAG) val date: String = "", @get:PropertyName(DESC_TAG) val desc: String = "",
	@get:PropertyName(VAL_TAG) var value: Double = .0, @get:PropertyName(WALLET_TAG) val wallet: String = "",
	@get:Exclude var hash: String? = null
) {
	override fun equals(other: Any?) = if (other is CostObject)
		((date == other.date).and(desc == other.desc).and(value == other.value))
	else false

	@get:Exclude
	val _time: DateTime
		get() = (DateTimeFormat.forPattern("dd/MM/yyyy").parseDateTime(date))

	@get:Exclude
	val _real: String
		get() = String.format("R$ %.2f", value)

	@get:Exclude
	val cleanDate: String
		get() = date.replace("/", "")

	companion object {
		fun fromMap(key: String, item: Any) = (item as HashMap<*, *>).run {
			CostObject(this[DATE_TAG].toString(), this[DESC_TAG].toString(), this.doubling(VAL_TAG), this[WALLET_TAG].toString(), key)
		}
	}
}