/**
 * @author Mário Henrique
 * Created on 02/05/2022
 */
package br.com.mario.reservalugar.ui.resume

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope

import br.com.mario.reservalugar.data.ReportRepository
import br.com.mario.reservalugar.data.ResultWrapper
import br.com.mario.reservalugar.data.ValuesRepository
import br.com.mario.reservalugar.model.ReportObject

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update

import kotlinx.coroutines.launch

class StatementViewModel(private val values: ValuesRepository, private val report: ReportRepository) : ViewModel() {
	private val viewModelState = MutableStateFlow(StatementViewModelState(isLoading = true))

	val uiState = viewModelState.map { it.toUiState() }
		.stateIn(viewModelScope, SharingStarted.Eagerly, viewModelState.value.toUiState())

	init {
		refreshReportList()

		viewModelScope.launch {
			report.observeReportList().collect { list ->
				viewModelState.update { it.copy(reportList = list, isLoading = false) }
			}
		}
	}

	fun refreshReportList() {
		viewModelState.update { it.copy(isLoading = true) }

		viewModelScope.launch {
			val result = report.getList()

			viewModelState.update {
				when (result) {
					is ResultWrapper.Success -> it.copy(reportList = result.data, isLoading = false)
					is ResultWrapper.Error -> it.copy(isLoading = false)
				}
			}
		}
	}

	companion object {
		fun provideFactory(report: ReportRepository, values: ValuesRepository) = object : ViewModelProvider.Factory {
			override fun <T : ViewModel> create(modelClass: Class<T>) = StatementViewModel(values, report) as T
		}
	}
}

data class StatementViewModelState(
	val isLoading: Boolean = false,
	val reportList: List<ReportObject>? = null
) {
	fun toUiState() : StatementUiState =
		if (reportList == null)
			StatementUiState.NoItems(isLoading = false)
	else
		StatementUiState.HasItems(
			isLoading = false,
			reportList = reportList
		)
}

sealed interface StatementUiState {
	val isLoading: Boolean
	val reportList: List<ReportObject>

	data class NoItems(
		override val isLoading: Boolean,
		override val reportList: List<ReportObject> = emptyList(),
	) : StatementUiState

	data class HasItems(
		override val isLoading: Boolean,
		override val reportList: List<ReportObject>
	) : StatementUiState
}