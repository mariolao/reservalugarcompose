/**
 * @author Mário Henrique
 * Created on 24/04/2022
 */
package br.com.mario.reservalugar.ui.cost

import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue

import br.com.mario.reservalugar.model.CostObject

import kotlin.reflect.KFunction0
import kotlin.reflect.KFunction1
import kotlin.reflect.KFunction2

@Composable
fun CostRoute(costVM: CostViewModel, isExpanded: Boolean) {
	val uiState by costVM.uiState.collectAsState()

	CostRoute(
		uiState = uiState,
		isExpandedScreen = isExpanded,
		onSelectAndOpen = costVM::showDialog,
		onAddItem = costVM::addItem,
		onRemoveItem = costVM::deleteItem,
		onRefresh = costVM::refreshCosts
	)
}

@Composable
private fun CostRoute(
	uiState: CostUiState,
	isExpandedScreen: Boolean,
	onSelectAndOpen: KFunction2<Boolean, Int, Unit>,
	onAddItem: KFunction1<CostObject, Unit>,
	onRemoveItem: KFunction1<CostObject, Unit>,
	onRefresh: KFunction0<Unit>,
) {
	val costLazyListState = rememberLazyListState()

	if (!isExpandedScreen) {
		CostSimpleListScreen(uiState, onSelectAndOpen, onAddItem, onRemoveItem, onRefresh)
	} else
		CostListWithDetailsScreen(uiState, onRefresh, onAddItem, onRemoveItem, onSelectAndOpen, costLazyListState)
}