/**
 * Arquivo contendo o controle de funções para acesso ao BD do Firebase
 *
 * @author Mário Henrique
 * Created on 23/04/2020
 * Lst Mod on 19/05/2022
 */
@file:Suppress("UNCHECKED_CAST")

package br.com.mario.reservalugar

import br.com.mario.reservalugar.model.BuyerObject
import br.com.mario.reservalugar.model.ExtraModel
import br.com.mario.reservalugar.model.ParkingModel
import br.com.mario.reservalugar.model.ReportObject
import br.com.mario.reservalugar.model.TableObject

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

var TEST = false
private const val TEST_BD_TAG = "zBD_TESTE_DBz"

private val currentUser: FirebaseUser?
	get() = firebaseAuth.currentUser
private val firebaseAuth: FirebaseAuth by lazy { FirebaseAuth.getInstance() }

private val firebaseDB: FirebaseDatabase by lazy { Firebase.database.apply { setPersistenceEnabled(true) } }

private val firebaseRef: DatabaseReference
	by lazy { if (TEST) firebaseDB.reference.child(TEST_BD_TAG) else firebaseDB.reference }

private val reportRef by lazy { firebaseRef.child("relatorio_operacoes") }
private val spendsRef by lazy { firebaseRef.child("controle_gastos") }
private val placesRef by lazy { firebaseRef.child("reserva_lugar") }
private val buyersRef by lazy { firebaseRef.child("reserva_comprador") }
private val valuesRef by lazy { firebaseRef.child("valores") }
// DataBaseReference s de segunda camada:
private val generalReportRef by lazy { reportRef.child("relatorio_geral") }
private val userReportRef by lazy { reportRef.child("relatorio_usuario") }
// utilização com gerenciamento de usuário/vendedor
private val sellerRef by lazy { firebaseRef.child("usuarios") }

internal val generateKey get() = firebaseRef.push().key!!

object ReserveDB {
	/* monitora reservas das mesas, assim como das extras */
	fun loadTableReserves(f: (ArrayList<*>) -> Unit) = placesRef.onSingleDataChange { f(it.value as ArrayList<*>) }
	fun changeTableReserve(g: __ChildEventListener.() -> Unit) = placesRef.childEventListener { g(this) }
	fun setTableReserve(table: String, value: TableObject) = placesRef.child(table).setValue(value)
	fun setTablesReserve(children: MutableMap<String, Any>) = placesRef.updateChildren(children)
	fun setExtraReserve(table: String, value: ExtraModel?) = placesRef.child("${table}/extra").setValue(value)
	/* monitora reservas dos compradores, assim como de estacionamento */
	fun loadBuyerReserves(f: (HashMap<String, *>) -> Unit) =
		buyersRef.onSingleDataChange { it.value?.let { v -> f(v as HashMap<String, *>) } }
	fun changeBuyerReserve(g: __ChildEventListener.() -> Unit) = buyersRef.childEventListener(g)
	fun setBuyerReserve(key: String, value: BuyerObject?) = buyersRef.child(key).setValue(value)
	fun setParkReserve(buyer: String, value: ParkingModel?) = buyersRef.child("${buyer}/vaga").setValue(value)
}

object PaymentDB {
	fun setTableStatus(table: String, children: HashMap<String, Any>) = placesRef.child(table).updateChildren(children)
	fun setExtraStatus(table: String, children: HashMap<String, Any>) =
		placesRef.child("${table}/extra").updateChildren(children)
	fun setParkStatus(buyer: String, children: HashMap<String, Any>) =
		buyersRef.child("${buyer}/vaga").updateChildren(children)
}

object CostsDB {//todo fazer com que carregue com possibilidade de erro
	fun load(f: (Any) -> Unit) = spendsRef.onSingleDataChange { it.value?.let { v -> f.invoke(v) } }
	fun change(g: __ChildEventListener.() -> Unit) = spendsRef.childEventListener { g.invoke(this) }

	fun addItem(value: Any) = spendsRef.child(generateKey).setValue(value)
//	fun modItem(item: CostObject) = spendsRef.child(item.hash!!).setValue(item)
	fun delItem(key: String) = spendsRef.child(key).removeValue()
}

object ValuesDB {
	private const val GAIN_INITIAL = "constantes"
	const val PATH_EXTRA = "extra"
	const val PATH_TOTAL = "total"
	const val PATH_TABLE = "mesa"
	const val PATH_PARK = "estacionamento"
	const val BANK_TAG = "banco"
	const val MONEY_TAG = "dinheiro"
	const val PATH_OUTS = "saidas"
	const val PRICE_EXTRA = "preco-extra"
	const val PRICE_PARK = "preco-vaga"
	const val PRICE_TABLE = "preco-mesa"
	const val MAX_TABLE = "total-mesas"
	const val MAX_EXTRA = "total-extras"
	const val MAX_PARK = "total-vagas"
	val WAY_IN = "entrada"
	val WAY_OUT = "saida"
	val WAY_REV = "estorno"

	private val remainRef = valuesRef.child("disponibilidade")
	private val categoryRef = valuesRef.child("categoria")
	private val walletRef = valuesRef.child("caixa")

	/** carrega os dados de controle, como quantidade máxima disponível para reservas e preços dos itens  */
	fun loadConstants(f: (HashMap<*,*>) -> Unit) =
		valuesRef.child(GAIN_INITIAL).onSingleDataChange{ f(it.value as HashMap<*, *>) }

	fun loadCategories(f: (HashMap<*, *>) -> Unit) =
		categoryRef.onSingleDataChange { f(it.value as HashMap<*, *>) }
	fun changeCategory(g: __ChildEventListener.() -> Unit) =
		categoryRef.childEventListener { g(this) }
	fun eventBalanceExits(h: (exit: HashMap<String, *>) -> Unit) =
		categoryRef.child(PATH_OUTS).onValueDataChange { h(it.value as HashMap<String, *>) }

	fun loadInOut(f: (HashMap<*, *>) -> Unit) =
		walletRef.onSingleDataChange { f(it.value as HashMap<*, *>) }
	fun loadInOutBank(f: (HashMap<*, *>) -> Unit) =
		walletRef.child(BANK_TAG).onSingleDataChange { f(it.value as HashMap<*, *>) }
	fun loadInOutDIN(f: (HashMap<*, *>) -> Unit) =
		walletRef.child(MONEY_TAG).onSingleDataChange { f(it.value as HashMap<*, *>) }
	fun changeInOut(g: __ChildEventListener.() -> Unit) =
		walletRef.childEventListener { g(this) }

	fun loadRemain(f: (HashMap<*, *>) -> Unit) = remainRef.onSingleDataChange { f(it.value as HashMap<*, *>) }
	fun changeRemain(g: __ChildEventListener.() -> Unit) = remainRef.childEventListener { g(this) }
	fun updateRemain(key: String, value: Long) = remainRef.child(key).setValue(value)

	/** atualiza o saldo por categoria, e depois o distribuídos nas carteiras */
	fun updateEarn(catKey: String, wallet: String, value: Any) = categoryRef.child(catKey).child(wallet).setValue(value)
	/*fun updateInOut(key: String, way: String, value: Double) =
		valuesRef.child("$inOutPath$key").child(way).setValue(value)*/
	fun updateIn(key: String, value: Double) = walletRef.child(key).child(WAY_IN).setValue(value)
	fun updateOut(key: String, value: Double) = walletRef.child(key).child(WAY_OUT).setValue(value)
	fun updateOutReveal(key: String, value: Double) = walletRef.child(key).child(WAY_REV).setValue(value)
}

object ReportDB {
	private val key: String get() = generalReportRef.push().key!!

	fun loadGeneral(f: (HashMap<String, *>) -> Unit) =
		generalReportRef.onSingleDataChange { it.value?.let { v -> f.invoke(v as HashMap<String, *>) } }
	fun changeGeneral(g: __ChildEventListener.() -> Unit) = generalReportRef.childEventListener { g.invoke(this) }

	fun report(value: ReportObject) { generalReportRef.child(key).setValue(value) }
}

/** Created on 09/07/2020 */
object UserDB {
	const val ACCESS_TAG = "primeiro_acesso"
//		val reportKey get() = greatReportRef.push().key!!
	fun loadUsers(f: (HashMap<String, *>) -> Unit) = sellerRef.onSingleDataChange { f(it.value as HashMap<String, *>) }
	fun changeUsers(g: __ChildEventListener.() -> Unit) = sellerRef.childEventListener { g(this) }
	fun loadAccess(f: (Boolean) -> Unit) =
		sellerRef.child(currentUser!!.uid).child(ACCESS_TAG).onSingleDataChange { f.invoke(it.value as Boolean) }

	fun loadCurrentUser(hash: String, f: (HashMap<*, *>) -> Unit) = sellerRef.child(hash).onSingleDataChange { f(it.value as  HashMap<*, *>) }

	fun currentUser(f: (FirebaseUser?) -> Unit) = f(currentUser)
	fun setAccess(uid: String, access: Boolean) = sellerRef.child(uid).child(ACCESS_TAG).setValue(access)

//		fun report(key: String, value: Report) = greatReportRef.child(key).setValue(value)
}