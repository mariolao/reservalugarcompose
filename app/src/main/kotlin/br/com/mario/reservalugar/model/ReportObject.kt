package br.com.mario.reservalugar.model

import br.com.mario.reservalugar.doubling
import com.google.firebase.database.Exclude
import com.google.firebase.database.PropertyName
import org.joda.time.format.DateTimeFormat

private const val DATE_TAG = "data"
private const val OP_TAG = "operacao"
private const val DESC_TAG = "objeto"
private const val BUY_TAG = "comprador"
private const val SEL_TAG = "usuario"
private const val WALLET_TAG = "carteira"
private const val VAL_TAG = "valor"

/**
 * Classe de modelo de Relatório Geral (BD: relatorios -> relatorio_geral)
 *
 * Created on 02/01/2022
 * Lst mod on 03/05/2022
 */
sealed class ReportObject(
	@get:PropertyName(DATE_TAG) val date: String,
	@get:PropertyName(OP_TAG) val op: String,
	@get:PropertyName(DESC_TAG) val target: String,
	@get:PropertyName(SEL_TAG) val seller: String,
) {
	val dateL: Long
		@PropertyName("data_ord") get() = DateTimeFormat.forPattern("dd/MM/yy").parseDateTime(date).millis

	val dateShort: String
		@Exclude get() = DateTimeFormat.forPattern("dd/MM/yy").print(dateL)

	/*@get:Exclude val dateTime: DateTime
		get() = DateTime(dateL)*/

	companion object {
		fun fromMap(item: Any) = (item as HashMap<*, String>).run {
			if (this.containsKey(BUY_TAG)) {
				if (this.containsKey(WALLET_TAG))
					GainReport(this[DATE_TAG]!!, this[OP_TAG]!!, this[DESC_TAG]!!, this[BUY_TAG]!!, this[SEL_TAG]!!, this.doubling(
						VAL_TAG), this[WALLET_TAG]!!)
				else
					TableReport(this[DATE_TAG]!!, this[OP_TAG]!!, this[DESC_TAG]!!, this[BUY_TAG]!!, this[SEL_TAG]!!)
			} else
				CostReport(this[DATE_TAG]!!, this[OP_TAG]!!, this[DESC_TAG]!!, this[SEL_TAG]!!, this.doubling(VAL_TAG), this[WALLET_TAG]!!)
//			ReportObject(this["comprador"]!!, this["data"]!!, this["operacao"]!!, this["objeto"]!!, this["usuario"]!!, this["carteira"]!!)
		}
	}
}

class CostReport(
	date: String, op: String, desc: String, seller: String,
	@get:PropertyName(VAL_TAG) val value: Double, @get:PropertyName(WALLET_TAG) val wallet: String
) : ReportObject(date, op, desc, seller)

class TableReport(
	date: String, op: String, desc: String,
	@get:PropertyName(BUY_TAG) val buyer: String, seller: String,
) : ReportObject(date, op, desc, seller)

class GainReport(
	date: String, op: String, desc: String,
	@get:PropertyName(BUY_TAG) val buyer: String, seller: String,
	@get:PropertyName(VAL_TAG) val value: Double, @get:PropertyName(WALLET_TAG) val wallet: String
) : ReportObject(date, op, desc, seller)