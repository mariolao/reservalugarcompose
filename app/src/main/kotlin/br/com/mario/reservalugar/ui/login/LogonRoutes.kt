/**
 * @author Mário Henrique
 * Created on 12/05/2022
 * Lst mod on 27/06/2022
 */
package br.com.mario.reservalugar.ui.login

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size

import androidx.compose.material.Icon
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.Text
import androidx.compose.material.TextField

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue

import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

import androidx.constraintlayout.compose.ConstraintLayout

import br.com.mario.reservalugar.R
import br.com.mario.reservalugar.ui.theme.MyColors
import br.com.mario.reservalugar.ui.widgets.dialogs.AlertResetPassword

import com.simform.ssjetpackcomposeprogressbuttonlibrary.SSButtonState
import com.simform.ssjetpackcomposeprogressbuttonlibrary.SSButtonType
import com.simform.ssjetpackcomposeprogressbuttonlibrary.SSJetPackComposeProgressButton
import kotlinx.coroutines.delay

/** Ponto de entrada para tela de login */
@Composable
fun LogonRoute(loginVM: LogonViewModel, onSuccessful: () -> Unit) {

//	val authenticationState by loginVM.authenticationState.collectAsState()
	val uiState by loginVM.uiState.collectAsState()
	LogonScreen(
		uiState = uiState,
		onAuthenticate = loginVM::authenticate,
		onChangePassword = loginVM::changePassword,
		onSuccessful = onSuccessful
	)
}

@Composable
fun LogonScreen(
	uiState: LogonUiState,
	onAuthenticate: (String, String) -> Unit,
	onChangePassword: (String, String, String) -> Unit,
	onSuccessful: () -> Unit,
) {
	var loginBtState by remember { mutableStateOf(SSButtonState.IDLE) }
	var userMail by remember { mutableStateOf(uiState.userEmail) }

	when (uiState.authState) {
		AuthenticationState.AUTHENTICATED -> {
			LaunchedEffect(key1 = true) {
				loginBtState = SSButtonState.SUCCESS
			}
			val (visible, setVisibility) = remember { mutableStateOf(uiState.openChangePassword) }
			if (visible) { //todo verificar necessidade de estar aqui neste bloco
				AlertResetPassword(onDismiss = { setVisibility(false) }, onExecute = { nP, cnP ->
					onChangePassword(userMail, nP, cnP)
					setVisibility(false)
				})
			}
		}
		AuthenticationState.SECOND_ACCESS -> {
			LaunchedEffect(key1 = true) {
				loginBtState = SSButtonState.SUCCESS
				delay(1000)
				onSuccessful() // altera CONDITIONAL_TAG p/ Verdadeiro e faz popBackStack()
			}
		}
		AuthenticationState.INVALID_AUTHENTICATION -> { loginBtState = SSButtonState.FAILIURE }
		else -> {}
	}

	loginBtState = if (uiState.isUsersLoaded) SSButtonState.IDLE else SSButtonState.LOADING // usuários não carregados -> botão carregando

	ConstraintLayout(Modifier.fillMaxSize()
		.background(brush = Brush.linearGradient(
			colors = listOf(Color(0xff23729a), Color(0xff42959a), Color(0xff2d9a59)),
			start = Offset.Zero,
			end = Offset.Infinite)
		)
	) {
		val (ivLogo, userTt, passTt, btLoad) = createRefs()

		Image(
			painterResource(R.mipmap.ic_launcher_round),
			"logo da aplicação",
			Modifier.size(200.dp).padding(top = 50.dp).constrainAs(ivLogo) {
				top.linkTo(parent.top)
				linkTo(parent.start, parent.end)
			}
		)

		TextField(
			value = userMail,
			onValueChange = { userMail = it },
			Modifier.constrainAs(userTt) {
				top.linkTo(ivLogo.bottom, 40.dp)
				linkTo(parent.start, parent.end)
			},
			textStyle = LocalTextStyle.current.copy(color = Color.White),
			placeholder = { Text("nome de usuário", color = Color.White) },
			leadingIcon = { Icon(painterResource(R.drawable.ic_action_user), null ) }
		)
/*MyTextField( //todo falta configurar leadingIcon
			placeholder = "email de usuário",
			type = KeyboardType.Email,
			bgColor = Color.Transparent,
			textColor = Color.White,
			modifier = Modifier.constrainAs(userTt) {
				top.linkTo(ivLogo.bottom, 30.dp)
				linkTo(parent.start, parent.end)
			}
		) { str, b -> }*/

		var userPass by remember { mutableStateOf("") }
		TextField( //todo adicionar filtro p/ senha
			value = userPass,
			onValueChange = { userPass = it },
			Modifier.constrainAs(passTt) {
				top.linkTo(userTt.bottom, 25.dp)
				linkTo(parent.start, parent.end)
			},
			placeholder = { Text("coloque sua senha", color = Color.White) },
			leadingIcon = { Icon(painterResource(R.drawable.ic_action_lock), null) }
		)

		Box(Modifier.constrainAs(btLoad) {
			top.linkTo(passTt.bottom, 70.dp)
			linkTo(parent.start, parent.end)
		}) {

			SSJetPackComposeProgressButton(
				type = SSButtonType.CIRCLE,
				width = 200.dp,
				height = 50.dp,
				onClick = {
					if (loginBtState == SSButtonState.FAILIURE)
						loginBtState = SSButtonState.IDLE
					onAuthenticate(userMail.trim(), userPass)
				},
				assetColor = MyColors.ButtonLoginIdle,
				buttonState = loginBtState,
				blinkingIcon = true,
				cornerRadius = 50,
				enabled = uiState.isUsersLoaded,
				failureIconPainter = painterResource(R.drawable.ic_loading_error),
				text = "Acessar",
				fontSize = 18.sp
			)
		}
	}
}