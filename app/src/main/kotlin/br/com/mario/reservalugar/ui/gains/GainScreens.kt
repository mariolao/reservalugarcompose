package br.com.mario.reservalugar.ui.gains

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.material.Tab
import androidx.compose.material.TabRow
import androidx.compose.material.Text

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

import br.com.mario.reservalugar.model.TableObject
import br.com.mario.reservalugar.ui.theme.MyColors.Holo_GreenGrass
import br.com.mario.reservalugar.ui.theme.MyColors.IceWhite
import br.com.mario.reservalugar.ui.widgets.EmptyScreen
import br.com.mario.reservalugar.utils.PATHS
import br.com.mario.reservalugar.utils.WALLET

import kotlin.reflect.KFunction0
import kotlin.reflect.KFunction4
import kotlin.reflect.KFunction5

/**
 * Gerencia tela contendo abas, mostra os relatórios de reservas (Mesas, Extras e Vagas)
 *
 * Created on 22/12/2021
 * Lst mod on 09/05/2022
 */

@Composable
fun GainTabBasedScreen(
	uiState: GainsUiState,
	onPayClick: KFunction4<TableObject, PATHS, WALLET, Boolean, Unit>,
	onSelectedElement: KFunction5<String, PATHS, String, WALLET?, Boolean, Unit>,
	onHideDialogs: () -> Unit
) {
	var tabIndex by remember { mutableStateOf(0) }

	Column { //todo aplicar swipe: https://www.rockandnull.com/jetpack-compose-swipe-pager/
		TabRow(selectedTabIndex = tabIndex, backgroundColor = Holo_GreenGrass) {
			Tab(selected = tabIndex == 0, onClick = { tabIndex = 0 }, text = { Text("MESAS") })
			Tab(selected = tabIndex == 1, onClick = { tabIndex = 1 }, text = { Text("VAGAS") })
		}

		when (uiState) {
			is GainsUiState.NotLoaded -> EmptyScreen(/*adicionar mensagem por aqui, erros também*/)
			is GainsUiState.Loaded -> {
				when (tabIndex) {
					0 -> TableReservePaymentMiniList(uiState, onSelectedElement, onPayClick, onHideDialogs)
					1 -> ParkReservePaymentMiniList(uiState, onSelectedElement, onPayClick, onHideDialogs)
				}
			}
		}

	}
}

@Composable
fun GainDoubleListedScreen(
	uiState: GainsUiState,
	onPayClick: KFunction4<TableObject, PATHS, WALLET, Boolean, Unit>,
	onSelectItem: KFunction5<String, PATHS, String, WALLET?, Boolean, Unit>,
	onHideDialogs: KFunction0<Unit>
) {
	Row {
		if (uiState is GainsUiState.Loaded) {
			Column(Modifier.weight(.52f)) {
				Text("MESAS",
					Modifier.fillMaxWidth().background(Holo_GreenGrass).padding(vertical = 5.dp),
					color = IceWhite,
					fontWeight = FontWeight.Bold,
					textAlign = TextAlign.Center)
				TableReservePaymentMiniList(uiState, onSelectItem, onPayClick, onHideDialogs)
			}
			Divider(Modifier.fillMaxHeight().weight(.04f), Color.LightGray)
			Column(Modifier.weight(.44f)) {
				Text("VAGAS",
					Modifier.fillMaxWidth().background(Holo_GreenGrass).padding(vertical = 5.dp),
					color = IceWhite,
					fontWeight = FontWeight.Bold,
					textAlign = TextAlign.Center)
				ParkReservePaymentMiniList(uiState, onSelectItem, onPayClick, onHideDialogs)
			}
		} else {
			EmptyScreen()
			EmptyScreen()
		}
	}
}