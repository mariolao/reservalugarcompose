/**
 * Contém as funções de controle da tela Ganhos
 *
 * @author Mário Henrique
 * Created on 19/04/2022
 * Lst mod on 03/05/2022
 */
package br.com.mario.reservalugar.ui.gains

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope

import br.com.mario.reservalugar.data.BuyerRepository
import br.com.mario.reservalugar.data.ReportRepository
import br.com.mario.reservalugar.data.ResultWrapper
import br.com.mario.reservalugar.data.TableRepository
import br.com.mario.reservalugar.data.ValuesRepository

import br.com.mario.reservalugar.model.BuyerObject
import br.com.mario.reservalugar.model.TableObject
import br.com.mario.reservalugar.utils.PATHS
import br.com.mario.reservalugar.utils.WALLET

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class GainViewModel(
	private val buyerRepository: BuyerRepository, private val reportRepository: ReportRepository,
	private val tableRepository: TableRepository, private val valuesRepository: ValuesRepository
) : ViewModel() {
	private val viewModelState = MutableStateFlow(GainsViewModelState(isLoading = true))

	val uiState = viewModelState.map { it.toUIState() }.stateIn(viewModelScope, SharingStarted.Eagerly, viewModelState.value.toUIState())

	init {
		refreshBuyerState()
		refreshTableState()

		viewModelScope.launch {
			buyerRepository.observeBuyerReserves().collect { buyerMap ->
				viewModelState.update { it.copy(buyerReservationMap = buyerMap) }
			}
		}
		viewModelScope.launch {
			tableRepository.observeTableReserves().collect { tableList ->
				viewModelState.update { it.copy(tableReservationList = tableList) }
			}
		}
	}

	private fun refreshTableState() {
		viewModelState.update { it.copy(isLoading = true) }

		viewModelScope.launch {
			val result = tableRepository.getTableReserves()

			viewModelState.update {
				when(result) {
					is ResultWrapper.Success -> it.copy(tableReservationList = result.data, isLoading = false)
					is ResultWrapper.Error -> {
						val errorMessages = it.errorMessages + "Some new error"
						it.copy(errorMessages = errorMessages, isLoading = false)
					}
				}
			}
		}
	}
	private fun refreshBuyerState() {
		viewModelState.update { it.copy(isLoading = true) }

		viewModelScope.launch {
			val result = buyerRepository.getBuyerReserves()

			viewModelState.update {
				when(result) {
					is ResultWrapper.Success -> it.copy(buyerReservationMap = result.data, isLoading = false)
					is ResultWrapper.Error -> it.copy(errorMessages = it.errorMessages + result.message!!, isLoading = false)
				}
			}
		}
	}

	fun selectItem(itemId: String, pathCategory: PATHS, owner: String, wallet: WALLET? = null, check: Boolean) {
		viewModelState.update { state ->
			state.copy(
				selectedTableId = itemId.toInt(),
				targetCategory = pathCategory,
				theOwner = owner,
				theWallet = wallet,
				openChooseDial = check,
				openRefundDial = !check
			)
		}
	}

	fun payment(table: TableObject, category: PATHS, wallet: WALLET, status: Boolean) {
		val message = if (status) "efetuado: " else "estorno: "
		val walletStatus = if (status) wallet.str else ""
		when(category) {
			PATHS.EXTRA -> {
				tableRepository.manageExtraPayment(table.id, walletStatus, status)
				reportRepository.addPayment(
					table.buyer, "${message}\nextra de ${table.id}", "atual",
					valuesRepository.getPrice(category), wallet.str)
			}
			PATHS.PARK -> {
				buyerRepository.manageParkingPayment(table.buyer, walletStatus, status)
				reportRepository.addPayment(
					table.buyer, "${message}estacionamento", "atual",
					valuesRepository.getPrice(category), wallet.str)
			}
			PATHS.TABLE -> {
				tableRepository.manageTablePayment(table.id, walletStatus, status) // altera status de pagamento em Reserva de Mesas
				reportRepository.addPayment(
					table.buyer, "$message${table.id}", "atual",
					valuesRepository.getPrice(category), wallet.str)
			}
		}
		valuesRepository.updateBalance(wallet, category, status)
	}

	fun hideAllDialogs() =
		viewModelState.update { it.copy(openChooseDial = false, openRefundDial = false) }

	companion object {
		fun provideFactory(
			buyer: BuyerRepository, report: ReportRepository,
			table: TableRepository, values: ValuesRepository,
		) = object : ViewModelProvider.Factory {
			override fun <T : ViewModel> create(modelClass: Class<T>): T = GainViewModel(buyer, report, table, values) as T
		}
	}
}

private data class GainsViewModelState(
	val tableReservationList: List<TableObject>? = null,
	val buyerReservationMap: Map<String, BuyerObject>? = null,
	val selectedTableId: Int = 0,
	val targetCategory: PATHS = PATHS.TABLE,
	val theOwner: String = "",
	val theWallet: WALLET? = null,
	val openChooseDial: Boolean = false,
	val openRefundDial: Boolean = false,
	val isLoading: Boolean = false,
	val errorMessages: List<String> = emptyList(),
) {
	fun toUIState() : GainsUiState =
		if (tableReservationList != null && buyerReservationMap != null) {
			GainsUiState.Loaded(
				selectedTable = if (selectedTableId == 0) null else tableReservationList[selectedTableId],
				tableReservationMiniList = tableReservationList.filter { it.hasOwner },
				parkReservationMiniList = buyerReservationMap.values.toList().filter { it.hasParkLot },
				openChooseDial = openChooseDial,
				openRefundDial = openRefundDial,
				targetPath = targetCategory,
				targetOwner = theOwner,
				targetWallet = theWallet,
				isLoading = false,
				errorMessages = errorMessages
			)
		} else {
			GainsUiState.NotLoaded(
				isLoading = false,
				errorMessages = errorMessages
			)
		}
}

sealed interface GainsUiState {
	val isLoading:Boolean
	val tableReservationMiniList: List<TableObject>
	val parkReservationMiniList: List<BuyerObject>
	val errorMessages: List<String>

	data class NotLoaded(
		override val tableReservationMiniList: List<TableObject> = emptyList(),
		override val parkReservationMiniList: List<BuyerObject> = emptyList(),
		override val isLoading: Boolean,
		override val errorMessages: List<String>
	) : GainsUiState

	data class Loaded(
		val selectedTable: TableObject?,
		val openRefundDial: Boolean = false,
		val openChooseDial: Boolean = false,
		val targetPath: PATHS,
		val targetOwner: String,
		val targetWallet: WALLET? = null,
		override val isLoading: Boolean,
		override val errorMessages: List<String>,
		override val tableReservationMiniList: List<TableObject>,
		override val parkReservationMiniList: List<BuyerObject>
	) : GainsUiState
}