package br.com.mario.reservalugar.ui.settings

import android.content.Context

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.preferencesDataStore

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

/**
 * DataStore da aplicação.
 * Atualmente salva apenas o Banco de Dados que está será usado
 *
 * @author Mário Henrique
 * @since 11/05/2022
 */
class SettingDataStore(private val context: Context) {
	companion object {
		private val Context.dataStore: DataStore<Preferences> by preferencesDataStore("settings")
		val TEST_DATABASE_FLAG = booleanPreferencesKey("test_database_flag")
	}

	val testBDFlagFlow: Flow<Boolean> = context.dataStore.data.map { preferences ->
		preferences[TEST_DATABASE_FLAG] ?: false
	}

	suspend fun setFlag(flag: Boolean) {
		context.dataStore.edit { preferences ->
			preferences[TEST_DATABASE_FLAG] = flag
		}
	}
}