/**
 * Contém o ViewModel para a tela de Controle dos Gastos
 *
 * @author Mário Henrique
 * Created on 29/04/2022
 * Lst mod on 05/05/2022
 */
package br.com.mario.reservalugar.ui.cost

import androidx.compose.ui.text.input.TextFieldValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope

import br.com.mario.reservalugar.data.CostRepository
import br.com.mario.reservalugar.data.ReportRepository
import br.com.mario.reservalugar.data.ResultWrapper
import br.com.mario.reservalugar.data.ValuesRepository
import br.com.mario.reservalugar.model.CostObject
import br.com.mario.reservalugar.monetary
import br.com.mario.reservalugar.utils.WALLET

import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

/** Created on 24/04/2022 */
class CostViewModel(
	private val costRepository: CostRepository,
	private val valuesRepository: ValuesRepository,
	private val reportRepository: ReportRepository
) : ViewModel() {
	private val viewModelState = MutableStateFlow(CostViewModelState(isLoading = true))

	val uiState = viewModelState.map { it.toUIState() }
		.stateIn(viewModelScope, SharingStarted.Eagerly, viewModelState.value.toUIState())

	init {
		refreshCosts()

		viewModelScope.launch {
			costRepository.observeCostList().collect { list ->
				viewModelState.update { it.copy(costList = list.ifEmpty { null }) }
			}
		}
	}

	/** Atualiza manualmente a lista de gastos */
	fun refreshCosts() {
		viewModelState.update { it.copy(isLoading = true) }

		viewModelScope.launch {
			val result = costRepository.getCostList()
			viewModelState.update {
				when(result) {
					is ResultWrapper.Success -> {
						val transform = (result.data as List<CostObject>)
						it.copy(costList = transform, isLoading = false) }
					is ResultWrapper.Error -> {
						val error = it.errorMessages + result.message!!
						it.copy(costList = null, errorMessages = error, isLoading = false)
					}
				}
			}
		}
	}

	fun showDialog(open: Boolean, costId: Int) =
		viewModelState.update { it.copy(openDialog = open, selectedCostId = costId) }

	fun addItem(cost: CostObject) {
		costRepository.addCost(cost)
		valuesRepository.updateExits(WALLET.sto(cost.wallet), cost.value)
		reportRepository.addCost("adicionado", cost.desc, "atual", cost.value, cost.wallet)
	}
	fun deleteItem(cost: CostObject) {
		costRepository.delCost(cost.hash!!)
		valuesRepository.updateExits(WALLET.sto(cost.wallet), -cost.value)
		reportRepository.addCost("excluído", cost.desc, "atual", cost.value, cost.wallet)
	}

	companion object {
		fun provideFactory(cost: CostRepository, report: ReportRepository, values: ValuesRepository) =
			object : ViewModelProvider.Factory {
				override fun <T : ViewModel> create(modelClass: Class<T>): T = CostViewModel(cost, values, report) as T
			}
	}
}
/** Created on 24/04/2022 */
private data class CostViewModelState(
	val costList: List<CostObject>? = null,
	val selectedCostId: Int = -1,
	val openDialog: Boolean = false,
	val isLoading: Boolean = false,
	val errorMessages: List<String> = emptyList()
) {
	fun toUIState() =
		if (costList == null) {
			CostUiState.NoItems(
				isLoading = isLoading,
				errorMessages = errorMessages
			)
		} else
			CostUiState.HasItems(
				openAddDialog = openDialog,
				selectedCost = if (selectedCostId == -1) null else costList[selectedCostId],
				totalTFV = countTotal(costList),
				isLoading = isLoading,
				costList = costList,
				errorMessages = errorMessages
			)

	private fun countTotal(list: List<CostObject>) = TextFieldValue(
		(list.fold(.0) { acc, cs -> acc.plus(cs.value) }).monetary(2)
	)
}

sealed interface CostUiState {
	val isLoading: Boolean
	val errorMessages: List<String>

	data class NoItems(
		override val isLoading: Boolean = false,
		override val errorMessages: List<String>
	) : CostUiState

	data class HasItems(
		val openAddDialog: Boolean,
		val selectedCost: CostObject?,
		val costList: List<CostObject>,
		val totalTFV: TextFieldValue,
		override val isLoading: Boolean,
		override val errorMessages: List<String>
	) : CostUiState
}