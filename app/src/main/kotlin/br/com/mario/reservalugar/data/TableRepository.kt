package br.com.mario.reservalugar.data

import android.util.Log

import br.com.mario.reservalugar.PaymentDB
import br.com.mario.reservalugar.ReserveDB
import br.com.mario.reservalugar.model.ExtraModel
import br.com.mario.reservalugar.model.TableObject

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

/**
 * Created on 09/04/2022
 * Lst mod on 22/04/2022
 */
class TableRepository {
	private val lugares = MutableStateFlow<List<TableObject>>(ArrayList()) // reserva_lugar

	init {
		ReserveDB.loadTableReserves {
			lugares.value = it.mapIndexed { idx, item -> TableObject.fromMap(item, idx.toString()) } as ArrayList
			Log.i("OK", "carregamento completo")
		}
		ReserveDB.changeTableReserve {
			onChildChanged { data, _ ->
				setOfflineTable(data.key.toString().toInt(), TableObject.fromMap(data.value, data.key!!))
//				lugares.value[data.key.toString().toInt()] = ReservaLugar.fromMap(data.value, data.key!!)
			}
		}
	}

	fun observeTableReserves(): Flow<List<TableObject>> = lugares
	fun getTableReserves() : ResultWrapper<List<TableObject>> = ResultWrapper.Success(lugares.value)

	fun reserveTable(table: String, key: String, date: String) {
		ReserveDB.setTableReserve(table, TableObject(key, date))
	}
	fun reserveTable(table: Map<String, TableObject>) {
		ReserveDB.setTablesReserve(table as MutableMap<String, Any>)
	}
	fun wipeTable(tableKey: String) = ReserveDB.setTableReserve(tableKey, TableObject()) // adiciona objeto vazio

	fun attachExtraToTable(tableStr: String, date: String) =
		ReserveDB.setExtraReserve(tableStr, ExtraModel(date))
	fun detachExtraToTable(tableStr: String) =
		ReserveDB.setExtraReserve(tableStr, null)

	fun manageTablePayment(tableTag: String, walletStr: String, paid: Boolean) {
		PaymentDB.setTableStatus(tableTag, TableObject.toPayMap(walletStr, paid))
	}

	fun manageExtraPayment(tableTag: String, walletStr: String, paid: Boolean) {
		PaymentDB.setExtraStatus(tableTag, ExtraModel.toPayMap(walletStr, paid))
	}

	private fun setOfflineTable(idx: Int, reserve: TableObject) {
		val temp = lugares.value.toMutableList()
		temp[idx] = reserve
		lugares.value = temp
	}
}