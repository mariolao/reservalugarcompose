package br.mariodeveloper.superspinner

import android.annotation.SuppressLint
import androidx.annotation.ArrayRes

import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.tween
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width

import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue

import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringArrayResource
import androidx.compose.ui.unit.dp

/**
 * Repositório de Spinner que lista Imagens
 *
 * Created on 08/02/2022
 * Lst mod on 14/02/2022
 */

@SuppressLint("UnusedTransitionTargetStateParameter")
@Composable
fun Spinner(modifier: Modifier, current: Int, onSelected: (Int) -> Unit , elements: List<Painter>, elementsTitle: List<String>? = null, title: String? = null) {
	if (elementsTitle != null) {
		if (elementsTitle.size != elements.size) throw IllegalArgumentException("Tamanho de listas diferente!")
	}
	var expanded by remember { mutableStateOf(false) }
//	var currentItem by remember { mutableStateOf(currentElement) }
	val transitionState = remember { MutableTransitionState(expanded).apply { targetState = !expanded } }

	val transition = updateTransition(targetState = transitionState, label = "")
	val arrowRotation by transition.animateFloat({ tween(durationMillis = 300)}, label = "rotation") {
		if (expanded) 180f else 0f
	}

	Row(modifier.clickable { expanded = !expanded }.padding(start = 5.dp),
		Arrangement.Center, Alignment.CenterVertically) {
		Image(painter = elements[current], null, Modifier.size(36.dp).border((0.5.dp), Color.Black))
//		Image(painter = current, contentDescription = "", Modifier.size(36.dp).border((0.5.dp), color = Color.Black))
		Icon(Icons.Filled.ArrowDropDown, "Spinner", Modifier.size(36.dp).rotate(arrowRotation))

		DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
			elements.forEachIndexed { idx, item ->
				DropdownMenuItem(
					onClick = {
						expanded = false
						onSelected(idx)
					}
				) {
					Image(painter = item, contentDescription = "element", Modifier.size(36.dp))
					if (elementsTitle != null) {
						Spacer(Modifier.width(5.dp))
						Text(elementsTitle[idx])
					}
				}
			}
		}
	}
}

@SuppressLint("UnusedTransitionTargetStateParameter")
@Composable
fun Spinner(modifier: Modifier, current: Int, onSelected: (Int) -> Unit, @ArrayRes elements: Int, @ArrayRes elementsTitle: Int?) {
	val titles = if (elementsTitle != null) stringArrayResource(elementsTitle) else null
	val elementsId = LocalContext.current.resources.obtainTypedArray(elements)//integerArrayResource(elements)
	val elementList = listOf(
		painterResource(elementsId.getResourceId(0, 0)),
		painterResource(elementsId.getResourceId(1, 0)),
		painterResource(elementsId.getResourceId(2, 0))
	)
	if (titles?.size != elementList.size) throw IllegalArgumentException("Tamanho de listas diferente!")

	var expanded by remember { mutableStateOf(false) }
	var currentItem by remember { mutableStateOf(elementList[current]) }
	val transitionState = remember { MutableTransitionState(expanded).apply { targetState = !expanded } }

	val transition = updateTransition(targetState = transitionState, label = "")
	val arrowRotation by transition.animateFloat({ tween(durationMillis = 300)}, label = "rotation") {
		if (expanded) 180f else 0f
	}

	Row(modifier.clickable { expanded = !expanded }.padding(start = 5.dp), Arrangement.Center, Alignment.CenterVertically) {
		Image(painter = elementList[current], contentDescription = "", Modifier.size(36.dp).border((0.5.dp), color = Color.Black))
		Icon(Icons.Filled.ArrowDropDown, "Spinner", Modifier.size(36.dp).rotate(arrowRotation))

		DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
			elementList.forEachIndexed { idx, item ->
				DropdownMenuItem(
					onClick = {
						expanded = false
						onSelected(idx)
					}
				) {
					Image(painter = item, contentDescription = "element", Modifier.size(36.dp))
					if (titles != null)
						Text(titles[idx])
				}
			}
		}
	}
}