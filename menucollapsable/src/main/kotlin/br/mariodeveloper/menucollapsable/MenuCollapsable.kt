package br.mariodeveloper.menucollapsable

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewmodel.compose.viewModel

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost

import br.mariodeveloper.menucollapsable.utils.CONDITIONAL_TAG
import br.mariodeveloper.menucollapsable.utils.WindowInfo

/**
 * Created on 08/01/2022
 * Lst mod on 14/05/2022
 */

@Composable
fun MenuCollapsable(
	controller: NavHostController,
	windowInfo: WindowInfo,
	menusOptions: List<MenuItemModel>,
	modifier: Modifier = Modifier.fillMaxSize(),
	timeOffset: Int = 800,
	timeSize: Int = 800,
	hasSettings: Boolean = false,
	settingRoute: String? = null,
	conditionalAction: () -> Unit = {},
	conditionalPredicate: (() -> Boolean)? = null,
	conditionalSavedStateHandle: SavedStateHandle? = null,
	conditionalActionForCancelation: () -> Unit = {},
	composables: NavGraphBuilder.() -> Unit
) {
	if (conditionalPredicate != null && conditionalSavedStateHandle != null) {
		val conditionalResult by conditionalSavedStateHandle.getStateFlow(CONDITIONAL_TAG, false).collectAsState()

		if (conditionalPredicate()) { // se condição para desvio é verdadeira
			when (conditionalResult) {
				false/*null, ConditionalStates.CONDITIONAL_NOTDONE*/ -> LaunchedEffect(Unit) {
					conditionalAction()
				}
				true/*ConditionalStates.CONDITIONAL_COMPLETED*/ ->
					MenuCollapsable(controller, windowInfo, menusOptions, modifier, timeOffset, timeSize, hasSettings, settingRoute, composables)
				/*ConditionalStates.CONDITIONAL_CANCELLED -> LaunchedEffect(Unit) {
					conditionalActionForCancelation()
				}*/
			}
		} else {
			MenuCollapsable(controller, windowInfo, menusOptions, modifier, timeOffset, timeSize, hasSettings, settingRoute, composables)
		}
	} else
		MenuCollapsable(controller, windowInfo, menusOptions, modifier, timeOffset, timeSize, hasSettings, settingRoute, composables)
}

@Composable
private fun MenuCollapsable(
	controller: NavHostController,
	windowInfo: WindowInfo,
	menusOptions: List<MenuItemModel>,
	modifier: Modifier,
	timeOffset: Int,
	timeSize: Int,
	hasSettings: Boolean,
	settingRoute: String?,
	composables: NavGraphBuilder.() -> Unit
) {
	val menuVM: MenuViewModel = viewModel()

	val qtdMenus = menusOptions.size
	val reduceFactor = 3

	MenuCollapse(modifier = modifier) {
		when (windowInfo.screenWidthInfo) {
			is WindowInfo.WindowType.Compact -> {
				Column(Modifier.fillMaxSize()) {
					// space reserved for collapsed option of the menu
					Spacer(Modifier.height(windowInfo.screenHeight / (reduceFactor * qtdMenus)))
					NavHost(navController = controller, startDestination = "menu") {
						composables()
					}
				}
				// camada superior; o menu cobre a área em que as telas serão carregadas
				MenuSurface(menuVM, menus = menusOptions, goes = controller, timeOffset, timeSize, hasSettings, settingRoute)
			}
			is WindowInfo.WindowType.Medium,
			is WindowInfo.WindowType.Expanded -> {
				Row (Modifier.fillMaxSize()) {
					// space reserved for collapsed option of the menu
					Spacer(Modifier.width(windowInfo.screenWidth / (reduceFactor * qtdMenus)))
					NavHost(navController = controller, startDestination = "menu") {
						composables()
					}
				}

				// camada superior; o menu cobre a área em que as telas serão carregadas
				LandscapeMenuSurface(menuVM, menus = menusOptions, goes = controller, timeOffset, timeSize, hasSettings, settingRoute)
			}
		}
	}
}

@Composable
private fun MenuCollapse(modifier: Modifier, content: @Composable BoxScope.() -> Unit) {
	Box(modifier) {
		content()
	}
}