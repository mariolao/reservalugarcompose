package br.mariodeveloper.menucollapsable.utils

import java.io.Serializable

/**
 * Controla os estados da navegação condicional
 *
 * @since 13/05/2022
 */
sealed class ConditionalStates : Serializable {
	object CONDITIONAL_NOTDONE: ConditionalStates()
	object CONDITIONAL_COMPLETED: ConditionalStates()
	object CONDITIONAL_CANCELLED: ConditionalStates()
}

const val CONDITIONAL_TAG = "conditional_successful"